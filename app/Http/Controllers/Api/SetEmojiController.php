<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;
use App\Models\NewsBanner;

class SetEmojiController extends Controller
{
    public function export(){
        return News::all();
    }

    public function import(){
        // $AllBanner = NewsBanner::query();
        $data = file_get_contents(public_path('json/emoji.json'),'r');
        $newsData = json_decode($data);

        // dd($newsData);
        foreach($newsData as $key => $data){
            $Banners = NewsBanner::where('news_id',$data->id)->get();
            $News = News::create([
                // 'id'=>$data->id,
                'category_id'=>$data->category_id,
                'title'=>$data->title,
                'content'=>$data->content,
                'date'=>$data->date,
                'share_comment_url'=>$data->share_comment_url,
                'img'=>$data->img,
                'sort'=>$data->sort,
                'state'=>$data->state,
                'delete'=>$data->delete,
            ]);
            foreach($Banners as $Banner){
                // dump($Banner);
                    $Banner->update([
                    'news_id'=>$News->id
                ]);
            }
            
        }
        // dd('123');
    }
}
