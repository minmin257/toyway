<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\HomeBanner;
use App\Models\InnerBanner;
use App\Models\Link;
use App\Models\News;
use App\Models\NewsBanner;
use App\Models\Product;
use App\Models\Video;

class SetUrlController extends Controller
{
    public function setUrl(){
        //HomeBanner
        foreach(HomeBanner::all() as $HemeBanner){
            $HemeBanner->update([
                'img' => Str::replace('https://toyway.jhong-demo.com.tw', 'https://i-love-toyway.com.tw', $HemeBanner->img),
            ]);
        }
        //InnerBanner
        foreach(InnerBanner::all() as $InnerBanner){
            $InnerBanner->update([
                'img' => Str::replace('https://toyway.jhong-demo.com.tw', 'https://i-love-toyway.com.tw', $InnerBanner->img),
            ]);
        }
        //Link
        foreach(Link::all() as $Link){
            $Link->update([
                'img' => Str::replace('https://toyway.jhong-demo.com.tw', 'https://i-love-toyway.com.tw', $Link->img),
            ]);
        }
        //Link
        foreach(News::all() as $News){
            $News->update([
                'img' => Str::replace('https://toyway.jhong-demo.com.tw', 'https://i-love-toyway.com.tw', $News->img),
                'content' => Str::replace('https://toyway.jhong-demo.com.tw', 'https://i-love-toyway.com.tw', $News->content),
                'share_comment_url' => Str::replace('https://toyway.jhong-demo.com.tw', 'https://i-love-toyway.com.tw', $News->share_comment_url),
            ]);
        }
        //NewsBanner
        foreach(NewsBanner::all() as $NewsBanner){
            $NewsBanner->update([
                'img' => Str::replace('https://toyway.jhong-demo.com.tw', 'https://i-love-toyway.com.tw', $NewsBanner->img),
            ]);
        }
        //Product
        foreach(Product::all() as $Product){
            $Product->update([
                'img' => Str::replace('https://toyway.jhong-demo.com.tw', 'https://i-love-toyway.com.tw', $Product->img),
                'content' => Str::replace('https://toyway.jhong-demo.com.tw', 'https://i-love-toyway.com.tw', $Product->content),
                'share_comment_url' => Str::replace('https://toyway.jhong-demo.com.tw', 'https://i-love-toyway.com.tw', $Product->share_comment_url),
            ]);
        }
        //Video
        foreach(Video::all() as $Video){
            $Video->update([
                'img' => Str::replace('https://toyway.jhong-demo.com.tw', 'https://i-love-toyway.com.tw', $Video->img),
                'content' => Str::replace('https://toyway.jhong-demo.com.tw', 'https://i-love-toyway.com.tw', $Video->content),
                'share_comment_url' => Str::replace('https://toyway.jhong-demo.com.tw', 'https://i-love-toyway.com.tw', $Video->share_comment_url),
            ]);
        }
    }
}
