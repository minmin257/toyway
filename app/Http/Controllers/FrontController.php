<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Repositories\VideoRepository;
use App\Repositories\NewsRepository;
use App\Repositories\ProductRepository;
use App\Repositories\BannerRepository;
use App\Repositories\PageRepository;
use App\Repositories\SubjectRepository;
use App\Repositories\LinkRepository;

class FrontController extends Controller
{
    protected $Video, $News, $Product, $Banner, $About, $Subject, $Link;

    public function __construct(VideoRepository $Video, NewsRepository $News, ProductRepository $Product, BannerRepository $Banner, PageRepository $Page, SubjectRepository $Subject, LinkRepository $Link){
        $this->Video = $Video;
        $this->News = $News;
        $this->Product = $Product;
        $this->Banner = $Banner;
        $this->Page = $Page;
        $this->Subject = $Subject;
        $this->Link = $Link;
    }

    public function index(){
        $Banners = $this->Banner->readHomeBanner()->where('state',1)->get();
        $Products = $this->Product->readProduct()->where('state',1)->where('homepage',1)->take(5)->get();
        $News = $this->News->readNewsWithoutCategory()->where('state',1)->take(5)->get();
        $Video = $this->Video->read()->where('state',1)->first();
        return view('front.homepage.index',compact('Products','News','Video','Banners'));
    }

    //about.other
    public function page(Request $request){
        $Subject = $this->Subject->read()->where('url',$request->url)->first() ?? abort(404);
        $Pages = $this->Page->read()->get();
        return view('front.page.index',compact('Pages','Subject'));
    }

    public function news(Request $request){
        $Subject = $this->Subject->read()->where('url',$request->url)->first() ?? abort(404);
        if($request->has('id')){
            $News = $this->News->readNews()->where('state',1)->where('id',$request->id)->first() ?? abort(404);
            
            //計算上下頁
            $category_id = null;
            switch($request->url){
                case 'news':
                    $category_id = 1;
                    break;
                case 'newProduct':
                    $category_id = 2;
                    break;
                default:
                    $category_id = 3;
            }
            $NewsList = $this->News->readNews()->where('category_id',$category_id)->where('state',1)->pluck('id')->toArray();
            $current_key = array_search($News->id,$NewsList);
            if(count($NewsList) == 1){
                $NewsLast = $NewsList[$current_key];
                $NewsNext = $NewsList[$current_key];
            }
            else{
                if($current_key == 0){ //第一筆
                    $NewsLast = end($NewsList);
                    $NewsNext = $NewsList[$current_key+1];
                }
                elseif($current_key == array_key_last($NewsList)){ //最後一筆
                    $NewsLast = $NewsList[$current_key-1];
                    $NewsNext = reset($NewsList);
                }
                else{
                    $NewsLast = $NewsList[$current_key-1];
                    $NewsNext = $NewsList[$current_key+1];
                }
            }
            return view('front.news.inner',compact('News','Subject','NewsLast','NewsNext'));
        }
        else{
            switch($request->url){
                case 'news':
                    $News = $this->News->readNews()->where('category_id',1)->where('state',1)->paginate(10);
                    break;
                case 'newProduct':
                    $News = $this->News->readNews()->where('category_id',2)->where('state',1)->paginate(10);
                    break;
                default:
                    $News = $this->News->readNews()->where('category_id',3)->where('state',1)->paginate(10);
            }
            return view('front.news.list',compact('News','Subject'));
        }
    }

    public function product(Request $request){
        $CurrentCategory = null;
        $CurrentBrand = null;
        $Subject = $this->Subject->read()->where('url','product')->first() ?? abort(404);
        $ProductCategories = $this->Product->readCategories()->get();
        if(!is_null($request->category)){
            $CurrentCategory = $this->Product->readCategories()->findOrFail($request->category);
            if(!is_null($request->brand)){
                $CurrentBrand = $this->Product->readBrands()->findOrFail($request->brand);
                $Products = $this->Product->readProduct()->where('state',1)->where('brand_id',$CurrentBrand->id)->paginate(50);
                $product_id = $this->Product->readProduct()->where('state',1)->where('brand_id',$CurrentBrand->id)->pluck('id');
                $ProductHasSelects = $this->Product->readProductHasSelect()->whereIn('product_id',$product_id)->pluck('product_select_id')->toArray();
                $ProductSelects = array_unique($ProductHasSelects);
                $Selects = $this->Product->readProductSelect()->whereIn('id',$ProductSelects)->get();
                return view('front.product.list',compact('ProductCategories','Subject','CurrentCategory','CurrentBrand','Products','Selects'));
            }
            $brand_ids = $CurrentCategory->brands->pluck('id');
            $Products = $this->Product->readProduct()->where('state',1)->whereIn('brand_id',$brand_ids)->paginate(50);
            return view('front.product.list',compact('ProductCategories','Subject','CurrentCategory','CurrentBrand','Products'));
        }
        $brand_ids = $ProductCategories->first()->brands->pluck('id');
        $Products = $this->Product->readProduct()->where('state',1)->whereIn('brand_id',$brand_ids)->paginate(50);
        return view('front.product.list',compact('ProductCategories','Subject','CurrentCategory','CurrentBrand','Products'));
    }

    public function product_fliter(Request $request){
        $Products = $this->Product->filter($request);
        return view('front.product.hole',compact('Products'));
    }

    public function product_inner(Request $request){
        $Subject = $this->Subject->read()->where('url','product')->first() ?? abort(404);
        $Product = $this->Product->readProduct()->where('state',1)->where('id',$request->product)->first() ?? abort(404);
        $ProductCategories = $this->Product->readCategories()->get();
        $CurrentCategory = $Product->brand->category;
        $CurrentBrand = $request->brand;

        //計算上下頁
        if(!is_null($request->brand)){
            $ProductList = $this->Product->readProduct()->where('state',1)->where('brand_id',$request->brand)->pluck('id')->toArray();
        }
        else{
            $ProductList = null;
            foreach($CurrentCategory->brands as $brand){
                if(is_null($ProductList)){
                    $ProductList = $this->Product->readProduct()->where('state',1)->where('brand_id',$brand->id)->get();
                }
                else{
                    $ProductList = $ProductList->concat($this->Product->readProduct()->where('state',1)->where('brand_id',$brand->id)->get());
                }
            }
            $ProductList = $ProductList->pluck('id')->toArray();
        }
        $current_key = array_search($Product->id,$ProductList);
        if(count($ProductList) == 1){
            $ProductLast = $ProductList[$current_key];
            $ProductNext = $ProductList[$current_key];
        }
        else{
            if($current_key == 0){ //第一筆
                $ProductLast = end($ProductList);
                $ProductNext = $ProductList[$current_key+1];
            }
            elseif($current_key == array_key_last($ProductList)){ //最後一筆
                $ProductLast = $ProductList[$current_key-1];
                $ProductNext = reset($ProductList);
            }
            else{
                $ProductLast = $ProductList[$current_key-1];
                $ProductNext = $ProductList[$current_key+1];
            }
        }
        
        return view('front.product.inner',compact('Subject','Product','ProductCategories','CurrentCategory','CurrentBrand','ProductNext','ProductLast'));
    }

    public function special(Request $request){
        $Subject = $this->Subject->read()->where('url','special')->first() ?? abort(404);
        if(!is_null($request->video)){
            $Video = $this->Video->read()->findOrFail($request->video) ?? abort(404);
            return view('front.video.inner',compact('Video','Subject'));
        }
        $MainVideo = $this->Video->read()->where('state',1)->first();
        $Videos = $this->Video->read()->where('state',1)->where('id','!=',$MainVideo->id)->paginate(6);
        return view('front.video.list',compact('MainVideo','Videos','Subject'));
    }

    public function link(Request $request){
        $Subject = $this->Subject->read()->where('url','link')->first() ?? abort(404);
        $Links = $this->Link->read()->where('state',1)->get();
        $LinkCoontent = $this->Page->read()->where('subject_id',$Subject->id)->first() ?? abort(404);
        return view('front.link.index',compact('Subject','Links','LinkCoontent'));
    }

    public function search(Request $request){
        // dd($request->keyword);
        $keyword = $request->keyword;
        $Subject = $this->Subject->read()->where('url','search')->first();
        // $Products = $this->Product->readProduct()->where('state',1)->where('name', 'like', '%'.$request->keyword.'%')->orWhere('content', 'like', '%'.$request->keyword.'%')->orWhere('no', 'like', '%'.$request->keyword.'%')->get();
        $Products = $this->Product->readProduct()->where('state',1)
            ->where(function ($query) use ($request) {
                $query->where('name', 'like', '%'.$request->keyword.'%')
                ->orWhere('content', 'like', '%'.$request->keyword.'%')
                ->orWhere('no', 'like', '%'.$request->keyword.'%');
            })->get();
        
        // $News = $this->News->readNews()->where('state',1)->where('title', 'like', '%'.$request->keyword.'%')->orWhere('content', 'like', '%'.$request->keyword.'%')->get();
        $News = $this->News->readNews()->where('state',1)
            ->where(function ($query) use ($request) {
                $query->where('title', 'like', '%'.$request->keyword.'%')
                ->orWhere('content', 'like', '%'.$request->keyword.'%');
            })->get();

        $Video = $this->Video->read()->where('state',1)
            ->where(function ($query) use ($request) {
                $query->where('title', 'like', '%'.$request->keyword.'%')
                ->orWhere('content', 'like', '%'.$request->keyword.'%');
            })->get();
        
        $collections = $Products->concat($News)->concat($Video);
        // dd($collections);
        $Results = $this->paginate($collections->sortByDesc('created_at')->slice(0, 100));
        $total = count($collections->slice(0, 100));

        if($request->has('select_option')){
            if($request->select_option == 'time'){
                // dd($Results);
                $Results = $this->paginate($collections->sortByDesc('created_at')->slice(0, 100));
            }
            else{
                $collections2 = $Products->concat($News)->concat($Video);
                $id_array = [];
                for($i=0; $i<$total; $i++){
                    $max = 0; //關鍵字最多
                    $max_id = null;
                    foreach($collections2 as $key => $collection){
                        $count_str = 0;
                        if(isset($collection->title)){
                            if($request->keyword != null){
                                $count_str = $count_str + substr_count($collection->title,$request->keyword);
                                $count_str = $count_str + substr_count($collection->content,$request->keyword);
                                }
                        }
                        else{
                            if($request->keyword != null){
                                $count_str = $count_str + substr_count($collection->name,$request->keyword);
                                $count_str = $count_str + substr_count($collection->content,$request->keyword);
                            }
                        }
                        
                        if($max == 0){
                            $max_id = $key;
                        }
                        if($count_str > $max){
                            $max = $count_str;
                            $max_id = $key;
                        }
                    }
                    $id_array[$i] = $max_id;
                    $collections2->pull($max_id);
                }

                $final_array = [];
                
                foreach($id_array as $key => $id){
                    foreach($collections as $colkey => $collection){
                        if($colkey == $id){
                            $final_array[$key] = $collection;
                        }
                    }
                }
                // dd($Results);
                $Results = $this->paginate(collect($final_array)->slice(0, 100));
            }
        }
        
        return view('front.search.index',compact('Subject','Results','keyword','total'));
    }

    //分頁
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
