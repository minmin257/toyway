<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Repositories\PremissionRepository;
use App\Http\Requests\backend\UpdateProfileRequest;
use App\Http\Requests\backend\CreateUserRequest;

class AuthorityController extends Controller
{
    protected $User, $Premission;

    public function __construct(UserRepository $User, PremissionRepository $Premission){
        $this->middleware('HasPremission:權限管理', ['only' => ['management']]);
        $this->User = $User;
        $this->Premission = $Premission;
    }

    public function profile(){
    	return view('backend.authority.profile');
    }

    public function profile_update(UpdateProfileRequest $request){
        $this->User->update($request);
    }

    public function management(){
        $Users = $this->User->read()->where('id','!=',1)->get();
        return view('backend.authority.management',compact('Users'));
    }

    public function management_update(Request $request){
        $this->User->update_state($request);
        return back()->with('update_message','');
    }

    public function management_edit(Request $request){
        $User = $this->User->read()->where('id',$request->id)->first();
        return view('backend.authority.edit',compact('User'));
    }

    public function premission(Request $request){
        $User = $this->User->read()->findOrFail($request->id);
        $Premissions = $this->Premission->read()->get();
        return view('backend.authority.premission',compact('User','Premissions'));
    }

    public function premission_update(Request $request){
        $this->User->premission_delete($request);
        $this->User->premission_create($request);
        return back()->with('update_message','');
    }

    public function user_create(CreateUserRequest $request){
        $this->User->create($request);
    }

    public function user_delete(Request $request){
        $this->User->delete($request);
    }
}
