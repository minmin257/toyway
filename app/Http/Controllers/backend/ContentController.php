<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\BannerRepository;
use App\Repositories\ProductRepository;
use App\Repositories\NewsRepository;
use App\Repositories\SubjectRepository;
use App\Repositories\PageRepository;
use App\Repositories\VideoRepository;
use App\Repositories\LinkRepository;
use App\Http\Requests\backend\UpdateBannerRequest;
use App\Http\Requests\backend\CreateBannerRequest;
use App\Http\Requests\backend\UpdateNewsCategoryRequest;
use App\Http\Requests\backend\CreateNewsRequest;
use App\Http\Requests\backend\UpdateNewsRequest;
use App\Http\Requests\backend\CreateNewsBannerRequest;
use App\Http\Requests\backend\UpdateNewsBannerRequest;
use App\Http\Requests\backend\UpdateSubjectRequest;
use App\Http\Requests\backend\UpdateProductCategoryRequest;
use App\Http\Requests\backend\UpdateProductBrandRequest;
use App\Http\Requests\backend\CreateProductBrandRequest;
use App\Http\Requests\backend\CreateProductRequest;
use App\Http\Requests\backend\UpdateProductRequest;
use App\Http\Requests\backend\CreateVideoRequest;
use App\Http\Requests\backend\UpdateVideoRequest;
use App\Http\Requests\backend\UpdateProductSelectRequest;
use App\Http\Requests\backend\CreateProductSelectRequest;
use App\Http\Requests\backend\UpdateLinkRequest;
use App\Http\Requests\backend\CreateLinkRequest;
use App\Http\Requests\backend\CreateProductImgRequest;

class ContentController extends Controller
{
    protected $Banner, $Product, $News, $Subject, $Page, $Video, $Link;

    public function __construct(BannerRepository $Banner, ProductRepository $Product, NewsRepository $News, SubjectRepository $Subject, PageRepository $Page,  VideoRepository $Video, LinkRepository $Link){
        $this->Banner = $Banner;
        $this->Product = $Product;
        $this->News = $News;
        $this->Subject = $Subject;
        $this->Page = $Page;
        $this->Video = $Video;
        $this->Link = $Link;
    }
    //homebanner
    public function homebanner(){
        $Banners = $this->Banner->readHomeBanner()->get();
        return view('backend.content.homebanner',compact('Banners'));
    }

    public function homebanner_update(UpdateBannerRequest $request){
        $this->Banner->updateHomeBanner($request);
    }

    public function homebanner_create(CreateBannerRequest $request){
        $this->Banner->createHomeBanner($request);
    }

    public function homebanner_delete(Request $request){
        $this->Banner->deleteHomeBanner($request);
    }
    //homeproduct
    public function homeproduct(){
        $ProductCategories = $this->Product->readCategories()->get();
        $Products = $this->Product->readProduct()->where('state',1)->where('homepage',0)->get();
        $HomepageProducts = $this->Product->readProduct()->where('homepage',1)->get();
        return view('backend.content.homepageproduct',compact('ProductCategories','Products','HomepageProducts'));
    }

    public function homeproduct_fliter(Request $request){
        $ProductCategory = $this->Product->readCategories()->where('id',$request->id)->first();
        $ProductBrands = $ProductCategory->brands;
        return $ProductBrands;
    }

    public function homeproduct_fliter_product(Request $request){ //取得對應產品
        $ProductBrand = $this->Product->readBrands()->where('state',1)->where('id',$request->id)->first();
        $Products = $ProductBrand->product->where('state',1)->where('homepage',0);
        return view('backend.content.homepagehole',compact('Products'));
    }

    public function homeproduct_update(Request $request){
        $this->Product->homeproductUpdate($request);
    }

    public function homeproduct_delete(Request $request){
        $this->Product->homeproductDelete($request);
    }
    //innerbanner
    public function innerbanner(){
        $Banners = $this->Banner->readInnerBanner()->get();
        return view('backend.content.innerbanner',compact('Banners'));
    }

    public function innerbanner_update(UpdateBannerRequest $request){
        $this->Banner->updateInnerBanner($request);
    }
    //page
    public function subject(){
        $Subjects =  $this->Subject->read()->where('id','!=',10)->get();
        return view('backend.content.subject',compact('Subjects'));
    }
    
    public function subject_update(UpdateSubjectRequest $request){
        $this->Subject->update($request);
    }
    
    public function pages(Request $request){
        $Pages = $this->Page->read()->get();
        foreach($Pages as $Page){
            if($Page->subject->url == $request->name){
                return view('backend.content.pages',compact('Page'));
            }
        }
    }

    public function pages_update(Request $request){
        $this->Page->update($request);
    }

    //products
    public function product_categories(){
        $ProductCategories = $this->Product->readCategories()->get();
        return view('backend.content.productCategory',compact('ProductCategories'));
    }

    public function product_categories_update(UpdateProductCategoryRequest $request){
        $this->Product->updateCategories($request);
    }

    public function product_categories_edit(Request $request){
        $ProductCategory = $this->Product->readCategories()->where('id',$request->id)->first();
        return view('backend.content.productCategoryEdit',compact('ProductCategory'));
    }

    public function product_brands(Request $request){
        $ProductBrands = $this->Product->readBrands()->where('category_id',$request->id)->get();
        return view('backend.content.productBrand',compact('ProductBrands'));
    }

    public function product_brands_update(UpdateProductBrandRequest $request){
        $this->Product->updateBrands($request);
    }

    public function product_brands_create(CreateProductBrandRequest $request){
        $this->Product->createBrands($request);
    }

    public function product_brands_delete(Request $request){
        $this->Product->deleteBrands($request);
    }

    public function product_selects(){
        $ProductSelects = $this->Product->readProductSelect()->get();
        return view('backend.content.productSelect',compact('ProductSelects'));
    }

    public function product_selects_update(UpdateProductSelectRequest $request){
        $this->Product->updateProductSelect($request);
    }

    public function product_selects_create(CreateProductSelectRequest $request){
        $this->Product->createProductSelect($request);
    }

    public function product_selects_delete(Request $request){
        $this->Product->deleteProductSelect($request);
    }

    public function product_selects_lists(Request $request){
        $select_id = $request->id;
        $ProductCategories = $this->Product->readCategories()->get();
        $Products_id = $this->Product->readProductHasSelect()->where('product_select_id',$request->id)->pluck('product_id');
        $HasProducts = $this->Product->readProduct()->whereIn('id',$Products_id)->get();
        $Products = $this->Product->readProduct()->whereNotIn('id',$Products_id)->get();
        return view('backend.content.productSelectList',compact('HasProducts','Products','ProductCategories','select_id'));
    }

    public function select_fliter_product(Request $request){
        $Products_id = $this->Product->readProductHasSelect()->where('product_select_id',$request->select_id)->pluck('product_id');
        $ProductBrand = $this->Product->readBrands()->where('state',1)->where('id',$request->id)->first();
        $Products = $ProductBrand->product->where('state',1)->whereNotIn('id',$Products_id);
        return view('backend.content.productSelectHole',compact('Products'));
    }

    public function product_fliter_product(Request $request){
        $Products_id = $this->Product->readProductHasSelect()->where('product_select_id',$request->select_id)->pluck('product_id');
        $ProductBrand = $this->Product->readBrands()->where('state',1)->where('id',$request->id)->first();
        $Products = $ProductBrand->product->where('state',1)->whereNotIn('id',$Products_id);
        return view('backend.content.productListHole',compact('Products'));
    }

    public function product_selects_lists_update(Request $request){
        $this->Product->updateProductHasSelect($request);
    }

    public function product_selects_lists_delete(Request $request){
        $this->Product->deleteProductHasSelect($request);
    }

    public function product_lists(Request $request){
        $check = 0;
        $ProductCategories = $this->Product->readCategories()->get();
        // $ProductBrand = $this->Product->readBrands()->where('state',1)->get();
        if(!is_null($request->id)){
            $check = 1;
            $Products = $this->Product->readProduct()->where('brand_id',$request->id)->get();
        }
        else{
            $Products = $this->Product->readProduct()->get();
        }
        return view('backend.content.productList',compact('Products','check','ProductCategories'));
    }

    public function product_create(CreateProductRequest $request){
        $this->Product->createProduct($request);
    }

    public function product_edit(Request $request){
        $Product = $this->Product->readProduct($request)->where('id',$request->id)->first();
        $ProductCategories = $this->Product->readCategories()->get();
        return view('backend.content.productEdit',compact('Product','ProductCategories'));
    }

    public function product_img(Request $request){
        $Product = $this->Product->readProduct($request)->where('id',$request->id)->first();
        $Imgs = $Product->imgs;
        return view('backend.content.productImg',compact('Product','Imgs'));
    }

    public function product_img_delete(Request $request){
        $this->Product->deleteProductImg($request);
    }

    public function product_img_update(Request $request){
        $this->Product->updateProductImg($request);
    }

    public function product_img_create(CreateProductImgRequest $request){
        $this->Product->createProductImg($request);
    }
    
    public function product_update(UpdateProductRequest $request){
        $this->Product->updateProduct($request);
    }

    public function product_delete(Request $request){
        $this->Product->deleteProduct($request);
    }
    //videos
    public function videos(){
        $Videos = $this->Video->read()->get();
        return view('backend.content.videos',compact('Videos'));
    }

    public function videos_create(CreateVideoRequest $request){
        $this->Video->create($request);
    }

    public function videos_edit(Request $request){
        $Video = $this->Video->read()->where('id',$request->id)->first();
        return view('backend.content.videosEdit',compact('Video'));
    }

    public function videos_update(UpdateVideoRequest $request){
        $this->Video->update($request);
    }

    public function videos_delete(Request $request){
        $this->Video->delete($request);
    }

    //links
    public function links(){
        $Links = $this->Link->read()->get();
        return view('backend.content.links',compact('Links'));
    }

    public function links_edit(Request $request){
        $Link = $this->Link->read()->where('id',$request->id)->first();
        return view('backend.content.linksEdit',compact('Link'));
    }

    public function links_update(UpdateLinkRequest $request){
        $this->Link->update($request);
    }

    public function links_create(CreateLinkRequest $request){
        $this->Link->create($request);
    }

    public function links_delete(Request $request){
        $this->Link->delete($request);
    }

    //news
    public function news_categories(){
        $NewsCategories = $this->News->readCategory()->get();
        return view('backend.content.newsCategory',compact('NewsCategories'));
    }

    public function news_categories_update(UpdateNewsCategoryRequest $request){
        $this->News->updateCategory($request);
    }

    public function news_lists(Request $request){
        $check = 0;
        $NewsCategories = $this->News->readCategory()->get();
        if(!is_null($request->id)){
            $check = 1;
            $News = $this->News->readNewsWithoutCategory()->where('category_id',$request->id)->get();
        }
        else{
            $News = $this->News->readNewsWithoutCategory()->get();
        }
        return view('backend.content.newsList',compact('News','NewsCategories','check'));
    }

    public function news_edit(Request $request){
        $NewsCategories = $this->News->readCategory()->get();
        $News = $this->News->readNews()->where('id',$request->id)->first();
        return view('backend.content.newsEdit',compact('News','NewsCategories'));
    }

    public function news_delete(Request $request){
        $this->News->deleteNews($request);
    }

    public function news_create(CreateNewsRequest $request){
        $this->News->createNews($request);
    }

    public function news_update(UpdateNewsRequest $request){
        $this->News->updateNews($request);
    }
    //newsbanner
    public function news_banner(Request $request){
        $News = $this->News->readNews()->where('id',$request->id)->first();
        $Banners = $News->banners->where('delete',0)->sortByDesc('sort');
        return view('backend.content.newsBanner',compact('Banners','News'));
    }

    public function news_banner_create(CreateNewsBannerRequest $request){
        $this->News->createNewsBanner($request);
    }

    public function news_banner_update(UpdateNewsBannerRequest $request){
        $this->News->updateNewsBanner($request);
    }

    public function news_banner_delete(Request $request){
        $this->News->deleteNewsBanner($request);
    }

    
}
