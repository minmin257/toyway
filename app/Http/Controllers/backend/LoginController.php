<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\backend\LoginRequest;

class LoginController extends Controller
{
    public function __construct() //中介
	{
		$this->middleware('guest:web', ['except'=>'logout']);
	}
	
    public function index()
    {
    	return view('backend.login');
    }

    public function login(LoginRequest $request)
    {
    	$credentials = [
    		'account'=>$request->account,
    		'password'=>$request->password,
    	];

    	if(!Auth()->attempt($credentials))
    	{
    		return back()->withErrors([
    			'message'=>'帳號或密碼有誤，請重新確認輸入'
    		]);
    	}
    	else
    	{
    		return redirect()->route('backend.index');
    	}
    }

    public function logout()
    {
        Auth()->logout();
        return redirect()->route('backend');
    }
}
