<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SystemRepository;
use App\Repositories\FooterRepository;
use App\Http\Requests\backend\UpdateSystemRequest;

class SystemController extends Controller
{
    protected $System, $Footer;

    public function __construct(SystemRepository $System, FooterRepository $Footer){
        $this->System = $System;
        $this->Footer = $Footer;
    }  

    public function system(){
        $System = $this->System->read();
        return view('backend.system.system',compact('System'));
    }

    public function system_update(UpdateSystemRequest $request){
        $this->System->update($request);
    }

    public function footer(){
        $Footer = $this->Footer->read();
        return view('backend.system.footer',compact('Footer'));
    }

    public function footer_update(Request $request){
        $this->Footer->update($request);
    }
}
