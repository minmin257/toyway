<?php
namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;
use Closure;
use App\Repositories\SystemRepository;

class CheckForMaintenanceMode extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array
     */
    protected $System;

    public function __construct(SystemRepository $SystemRepository){
        $this->System = $SystemRepository;
    }

    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array
     */
    public function handle($request, Closure $next){
        $System = $this->System->read()->first();
    	if($System->maintain){		
            abort(503,$System->message);
            // switch (app()->getLocale()) {
            //     case 'zh_TW':
            //         abort(503,$System->message);
            //         break;
            //     default:
            //         abort(503,$System->en_message);
            //         break;
            // }
    	}
    	return $next($request);
    }
}
