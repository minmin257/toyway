<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class HasPremission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $premission)
    {
        // $request, Closure $next, $premission
        $premissions = is_array($premission)
            ? $premission
            : explode('|', $premission);
        foreach ($premissions as $premission) {
            if (auth()->user()->hasPremission($premission)) {
                return $next($request);
            }
        }

        abort(401);
    }
}
