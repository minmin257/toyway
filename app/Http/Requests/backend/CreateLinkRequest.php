<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateLinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required'],
            'content' => ['required'],
            'filepath' => ['required'],
            'link' => ['required'],
            'sort' => ['required','numeric'],
        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'標題不可留空',
            'content.required'=>'內容不可留空',
            'filepath.required'=>'圖片不可留空',
            'link.required'=>'連結不可留空',
            'sort.required'=>'排序不可留空',
            'sort.numeric'=>'排序限輸入數字',
        ];
    }
}
