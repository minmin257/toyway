<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewsBannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'news_id' => ['required','exists:news,id'],
            'filepath' => ['required'],
            'sort' => ['required','numeric'],
        ];
    }

    public function messages()
    {
        return [
            'filepath.required'=>'圖片不可留空',
            'sort.required'=>'排序不可留空',
            'sort.numeric'=>'排序限輸入數字',
        ];
    } 
}
