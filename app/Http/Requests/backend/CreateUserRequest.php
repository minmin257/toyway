<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account' => ['required','unique:users'],
            'password' => ['required','between:6,10','regex:/(^[a-zA-Z0-9!$#%@\+-_]+$)/u'],
            'name' => ['required','regex:/[\x{4e00}-\x{9fa5}a-zA-Z]$/u'],
            'state' => ['required','numeric'],
        ];
    }

    public function messages()
    {
        return [
            'account.required'=>'帳號不能留空。',
            'password.required'=>'密碼不能留空。',
            'name.required'=>'姓名不能留空。',
            'name.regex'=>'姓名格式錯誤。',
            'state.required'=>'啟用狀態不能留空。',
        ];
    }
}
