<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateVideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required'],
            'youtube_id' => ['required'],
            'date' => ['required'],
            'content' => ['required'],
            'sort' => ['required','numeric'],
        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'影片標題不可留空',
            'date.required'=>'日期不可留空',
            'youtube_id.required'=>'youtube ID不可留空',
            'content.required'=>'影片描述不可留空',
            'sort.required'=>'排序不可留空',
            'sort.numeric'=>'排序限輸入數字',
        ];
    } 
}
