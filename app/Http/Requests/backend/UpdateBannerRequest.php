<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject_id.*'=>['sometimes','required','exists:subjects,id'],
            'innerbanner_id.*'=>['sometimes','required','exists:inner_banners,id'],
            'homebanner_id.*' => ['sometimes','required','exists:home_banners,id'],
            'filepath.*' => ['sometimes','required'],
            'innerbanner_filepath.*' => ['sometimes','nullable'],
            'link.*' => ['sometimes','nullable'],
            'sort.*' => ['sometimes','required','numeric'],
            'state.*'=> ['sometimes','required'],
        ];
    }

    public function messages()
    {
        return [
            'filepath.*.required'=>'輪播圖片不可留空',
            'sort.*.required'=>'排序不可留空',
            'sort.*.numeric'=>'排序限輸入數字',
        ];
    } 
}
