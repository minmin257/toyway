<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateNewsBannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'news_id'=>['required','exists:news,id'],
            'id.*'=>['required','exists:news_banners,id'],
            'filepath.*' => ['required'],
            'sort.*' => ['required','numeric'],
        ];
    }

    public function messages()
    {
        return [
            'filepath.*.required'=>'文章輪播圖片不可留空',
            'sort.*.required'=>'排序不可留空',
            'sort.*.numeric'=>'排序限輸入數字',
        ];
    } 
}
