<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required','exists:news,id'],
            'category_id' => ['required','exists:news_categories,id'],
            'title' => ['required'],
            'content' => ['required'],
            'share_comment_url' => ['nullable'],
            'filepath' => ['required'],
            'date' => ['required'],
            'sort' => ['required','numeric'],
        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'標題不可留空',
            'content.required'=>'內容不可留空',
            'filepath.required'=>'圖片不可留空',
            'date.required'=>'日期不可留空',
            'sort.required'=>'排序不可留空',
            'sort.numeric'=>'排序限輸入數字',
        ];
    } 
}
