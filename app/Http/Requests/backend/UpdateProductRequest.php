<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand_id' => ['required','exists:product_brands,id'],
            'name' => ['required'],
            'no' => ['required'],
            'filepath' => ['required'],
            'share_comment_url' => ['nullable'],
            'content' => ['nullable'],
            'sort' => ['required','numeric'],
            'state' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'brand_id.required'=>'商品品牌不可留空',
            'name.required'=>'商品名稱不可留空',
            'no.required'=>'商品編號不可留空',
            'filepath.required'=>'列表圖片不可留空',
            'sort.required'=>'排序不可留空',
            'sort.numeric'=>'排序限輸入數字',
        ];
    } 
}
