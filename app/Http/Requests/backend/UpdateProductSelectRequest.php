<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductSelectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id.*' => ['required','exists:product_selects,id'],
            'name.*' => ['required'],
            'sort.*' => ['required','numeric'],
            'state.*' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'name.*.required'=>'篩選名稱不可留空',
            'sort.*.required'=>'排序不可留空',
            'sort.*.numeric'=>'排序限輸入數字',
        ];
    }
}
