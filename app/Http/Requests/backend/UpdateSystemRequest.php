<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSystemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sitename'=>['required'],
            'message' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'sitename.required'=>'網站名稱不可留空',
            'message.required'=>'維護訊息不可留空',
        ];
    } 
}
