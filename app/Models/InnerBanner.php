<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InnerBanner extends Model
{
    use HasFactory;

    protected $fillable = [
        'subject_id', 'img', 'link', 'sort', 'state', 'delete'
    ];

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
}
