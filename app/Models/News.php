<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Subject;

class News extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id', 'title', 'content', 'date', 'img', 'sort', 'state', 'delete', 'share_comment_url'
    ];

    public function category()
    {
    	return $this->belongsTo(NewsCategory::class);
    }

    public function banners()
    {
    	return $this->hasMany(NewsBanner::class)->where('delete',0)->orderBy('sort','desc');
    }

    public function subject_url()
    {
        $url = Subject::where('url','news')->first()->url;
        return $url;
    }
}
