<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $fillable = [
        'subject_id', 'content'
    ];

    public function subject(){
        return $this->belongsTo(Subject::class);
    }
}
