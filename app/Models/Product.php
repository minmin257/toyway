<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'brand_id', 'name', 'no', 'img', 'content', 'homepage', 'sort', 'state', 'delete', 'share_comment_url'
    ];

    public function brand()
    {
    	return $this->belongsTo(ProductBrand::class);
    }

    public function imgs()
    {
    	return $this->hasMany(ProductImg::class)->where('delete',0)->orderBy('sort','desc');
    }
}
