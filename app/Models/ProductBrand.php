<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductBrand extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id', 'name', 'sort', 'state', 'delete'
    ];

    public function category()
    {
    	return $this->belongsTo(ProductCategory::class);
    }

    public function product()
    {
    	return $this->hasMany(Product::class,'brand_id','id')->where('delete',0)->orderBy('sort','desc');
    }
}
