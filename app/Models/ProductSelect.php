<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductSelect extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'sort', 'state', 'delete'
    ];
}
