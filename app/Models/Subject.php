<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name', 'url', 'link', 'sort', 'state', 'delete'
    ];

    public function banner(){
        return $this->HasOne(InnerBanner::class);
    }
}
