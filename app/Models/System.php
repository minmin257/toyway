<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'sitename', 'website', 'keyword', 'maintain', 'message', 'GAcode','description','member_login'
    ];
}
