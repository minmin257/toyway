<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account',
        'name',
        'password',
        'sort',
        'state',
        'delete'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function premissions(){
        return $this->hasMany(UserHasPremission::class);
    }

    public function hasPremission(string $premission){
        $premissions_id = $this->premissions()->pluck('premission_id');
        if(Premission::whereIn('id',$premissions_id)->where('name',$premission)->get()->count()>0){
           return true; 
        }
        else{
           return false;
        }
    }
}
