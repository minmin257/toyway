<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserHasPremission extends Model
{
    use HasFactory;

    protected $fillable = [
        'premission_id', 'user_id'
    ];
}
