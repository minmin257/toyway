<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'youtube_id', 'date', 'content', 'sort', 'state', 'delete', 'share_comment_url'
    ];
}
