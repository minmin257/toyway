<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\HomeBanner;
use App\Models\InnerBanner;
use Illuminate\Support\Facades\Redis;

class BannerRepository
{
    protected $HomeBanner, $InnerBanner;

    public function __construct(HomeBanner $HomeBanner, InnerBanner $InnerBanner)
    {
        $this->HomeBanner = $HomeBanner;
        $this->InnerBanner = $InnerBanner;
    }

    public function readHomeBanner()
    {
        return $this->HomeBanner->where('delete',0)->orderBy('sort','desc');
    }

    public function readInnerBanner()
    {
        return $this->InnerBanner->where('delete',0)->orderBy('sort','desc');
    }

    public function updateHomeBanner(Request $request){
        foreach($request['homebanner_id'] as $key => $id){
            $this->HomeBanner->FindOrFail($id)->update([
                'img'=>$request['filepath'][$key],
                'link'=>$request['link'][$key],
                'sort'=>$request['sort'][$key],
                'state'=>$request['state'][$key]
            ]);
        }
    }
    
    public function createHomeBanner(Request $request){
        $this->HomeBanner->create([
            'img'=>$request->filepath,
            'link'=>$request->link,
            'sort'=>$request->sort,
        ]);
    }

    public function deleteHomeBanner(Request $request){
        $this->HomeBanner->FindOrFail($request->id)->update([
            'delete'=>1
        ]);
    }

    public function updateInnerBanner(Request $request){
        foreach($request['innerbanner_id'] as $key => $id){
            $this->InnerBanner->FindOrFail($id)->update([
                'img'=>$request['innerbanner_filepath'][$key],
            ]);
        }
    }

}