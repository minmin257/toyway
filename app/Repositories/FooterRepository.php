<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Footer;

class FooterRepository
{
    protected $Footer;

    public function __construct(Footer $Footer)
    {
        $this->model = $Footer;
    }

    public function read()
    {
        return $this->model->first();
    }

    public function update(Request $request){
        $this->model->first()->update([
            'facebook'=>$request->facebook,
            'instgram'=>$request->instagram,
            'youtube'=>$request->youtube,
            'content'=>$request->content,
        ]);
    }

}