<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Link;
use Illuminate\Support\Facades\Redis;

class LinkRepository
{
    protected $Link;

    public function __construct(Link $Link)
    {
        $this->Link = $Link;
    }

    public function read()
    {
        return $this->Link->where('delete',0)->orderBy('sort','desc');
    }

    public function update(Request $request){
        $this->Link->findOrFail($request->id)->update([
            'title'=>$request->title,
            'content'=>$request->content,
            'img'=>$request->filepath,
            'link'=>$request->link,
            'sort'=>$request->sort,
            'state'=>$request->state,
        ]);
    }

    public function delete(Request $request){
        $this->Link->findOrFail($request->id)->update([
            'delete'=>1
        ]);
    }

    public function create(Request $request){
        $this->Link->create([
            'title'=>$request->title,
            'content'=>$request->content,
            'img'=>$request->filepath,
            'link'=>$request->link,
            'sort'=>$request->sort,
            'state'=>$request->state,
        ]);
    }
}