<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\News;
use App\Models\NewsCategory;
use App\Models\NewsBanner;
use Illuminate\Support\Facades\Redis;

class NewsRepository
{
    protected $News, $NewsCategory, $NewsBanner;

    public function __construct(News $News, NewsCategory $NewsCategory, NewsBanner $NewsBanner)
    {
        $this->News = $News;
        $this->NewsCategory = $NewsCategory;
        $this->NewsBanner = $NewsBanner;
    }

    public function readNews()
    {
        return $this->News->where('delete',0)->orderBy('category_id')->orderBy('sort','desc')->orderBy('date','desc')->orderBy('id','desc');
    }

    public function readNewsLast()
    {
        return $this->News->where('delete',0)->orderBy('category_id')->orderBy('sort','asc')->orderBy('date','asc')->orderBy('id','asc');
    }

    public function readNewsWithoutCategory()
    {
        return $this->News->where('delete',0)->orderBy('sort','desc')->orderBy('date','desc')->orderBy('id','desc');
    }

    public function readCategory()
    {
        return $this->NewsCategory;
    }

    public function updateCategory(Request $request){
        foreach($request['id'] as $key => $id){
            $this->NewsCategory->findOrFail($id)->update([
                'name'=>$request['name'][$key]
            ]);
        }
    }

    public function deleteNews(Request $request){
        $this->News->findOrFail($request->id)->update([
            'delete'=>1
        ]);
    }

    public function createNews(Request $request){
        $title = str_replace("\r\n","<br>",$request->title);
        $News = $this->News->create([
            'category_id'=>$request->category_id,
            'img'=>$request->filepath,
            'title'=>$title,
            'content'=>$request->content,
            'date'=>$request->date,
            'sort'=>$request->sort,
            'state'=>$request->state
        ]);

        switch($News->category_id){
            case 1:
                $url = "news";
                break;
            case 2:
                $url = "newProduct";
                break;
            default:
            $url = "reviewProduct";
        }

        
        $News->update([
            'share_comment_url' => url('/news/'.$url.'?id='.$News->id),
        ]);
    }

    public function updateNews(Request $request){
        $title = str_replace("\r\n","<br>",$request->title);
        $this->News->findOrFail($request->id)->update([
            'category_id'=>$request->category_id,
            'img'=>$request->filepath,
            'title'=>$title,
            'share_comment_url' => $request->share_comment_url,
            'content'=>$request->content,
            'date'=>$request->date,
            'sort'=>$request->sort,
            'state'=>$request->state
        ]);
    }

    public function createNewsBanner(Request $request){
        $this->NewsBanner->create([
            'news_id'=>$request->news_id,
            'img'=>$request->filepath,
            'sort'=>$request->sort,
        ]);
    }

    public function updateNewsBanner(Request $request){
        foreach($request['id'] as $key => $id){
            $this->NewsBanner->findOrFail($id)->update([
                'img'=>$request['filepath'][$key],
                'sort'=>$request['sort'][$key],
                'state'=>$request['state'][$key]
            ]);
        }
    }

    public function deleteNewsBanner(Request $request){
        $this->NewsBanner->findOrFail($request->id)->update([
            'delete'=>1,
        ]);
    }
}