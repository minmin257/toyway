<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Page;
use Illuminate\Support\Facades\Redis;

class PageRepository
{
    protected $Page;

    public function __construct(Page $Page)
    {
        $this->model = $Page;
    }

    public function read()
    {
        return $this->model;
    }

    public function update(Request $request){
        $this->model->findOrFail($request->id)->update([
            'content'=>$request->content
        ]);
    }
}