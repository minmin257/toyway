<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductImg;
use App\Models\ProductSelect;
use App\Models\ProductBrand;
use App\Models\ProductHasSelect;

class ProductRepository
{
    protected $Product, $ProductCategory, $ProductImg, $ProductSelect, $ProductBrand, $ProductHasSelect;

    public function __construct(Product $Product, ProductCategory $ProductCategory, ProductImg $ProductImg, ProductSelect $ProductSelect, ProductBrand $ProductBrand, ProductHasSelect $ProductHasSelect)
    {
        $this->Product = $Product;
        $this->ProductCategory = $ProductCategory;
        $this->ProductImg = $ProductImg;
        $this->ProductSelect = $ProductSelect;
        $this->ProductBrand = $ProductBrand;
        $this->ProductHasSelect = $ProductHasSelect;
    }

    public function readProduct(){
        return $this->Product->where('delete',0)->orderBy('sort','desc')->orderBy('id','desc');
    }

    public function readProductLast(){
        return $this->Product->where('delete',0)->orderBy('sort','asc')->orderBy('id','asc');
    }

    public function readCategories(){
        return $this->ProductCategory;
    }

    public function readBrands(){
        return $this->ProductBrand->where('delete',0)->orderBy('sort','desc');
    }

    public function readProductHasSelect(){
        return $this->ProductHasSelect;
    }

    public function readProductSelect(){
        return $this->ProductSelect->where('delete',0)->orderBy('sort','desc');
    }

    public function filter(Request $request){
        if(!is_null($request->selsct)){
            $ProductList = $this->ProductHasSelect->whereIn('product_select_id',$request->selsct)->pluck('product_id');
            $Products = $this->Product->whereIn('id',$ProductList)->where('brand_id',$request->brand)->where('delete',0)->orderBy('sort','desc')->get();
        }
        else{
            $Products = $this->Product->where('brand_id',$request->brand)->where('delete',0)->orderBy('sort','desc')->get();
        }
        return $Products;
    }

    public function homeproductUpdate(Request $request){
        foreach ($request['id'] as $key => $id) {
            $this->Product->findOrFail($id)->update([
                'homepage'=>1,
            ]);
        }
    }

    public function homeproductDelete(Request $request){
        $this->Product->findOrFail($request->id)->update([
            'homepage'=>0,
        ]);
    }

    public function updateCategories(Request $request){
        // foreach ($request['id'] as $key => $id) {
            $this->ProductCategory->findOrFail($request->id)->update([
                'name'=>$request->name,
                'content'=>$request->content,
            ]);
        // }
    }

    public function updateBrands(Request $request){
        foreach ($request['id'] as $key => $id) {
            $this->ProductBrand->findOrFail($id)->update([
                'name'=>$request['name'][$key],
                'sort'=>$request['sort'][$key],
                'state'=>$request['state'][$key],
            ]);
        }
    }

    public function createBrands(Request $request){
        $this->ProductBrand->create([
            'category_id'=>$request->category_id,
            'name'=>$request->name,
            'sort'=>$request->sort,
            'state'=>$request->state,
        ]);
    }

    public function deleteBrands(Request $request){
        $this->ProductBrand->findOrFail($request->id)->update([
            'delete'=>1,
        ]);
    }

    public function createProduct(Request $request){
        $Product = $this->Product->create([
            'brand_id'=>$request->brand_id,
            'name'=>$request->name,
            'no'=>$request->no,
            'img'=>$request->filepath,
            'content'=>$request->content,
            'homepage'=>$request->homepage,
            'sort'=>$request->sort,
            'state'=>$request->state,
        ]);

        $this->updateProductShareComment($Product->id);
        
    }

    public function updateProductShareComment($id){
        $this->Product->findOrFail($id)->update([
            'share_comment_url' => url('/product/inner/'.$id),
        ]);
    }

    public function updateProduct(Request $request){
        $this->Product->findOrFail($request->id)->update([
            'brand_id'=>$request->brand_id,
            'name'=>$request->name,
            'no'=>$request->no,
            'share_comment_url' => $request->share_comment_url,
            'img'=>$request->filepath,
            'content'=>$request->content,
            'sort'=>$request->sort,
            'state'=>$request->state,
        ]);
    }

    public function deleteProduct(Request $request){
        $this->Product->findOrFail($request->id)->update([
            'delete'=>1,
        ]);
    }

    public function updateProductSelect(Request $request){
        foreach ($request['id'] as $key => $id) {
            $this->ProductSelect->findOrFail($id)->update([
                'name'=>$request['name'][$key],
                'sort'=>$request['sort'][$key],
                'state'=>$request['state'][$key],
            ]);
        }
    }

    public function createProductSelect(Request $request){
        $this->ProductSelect->create([
            'name'=>$request->name,
            'sort'=>$request->sort,
            'state'=>$request->state,
        ]);
    }

    public function deleteProductSelect(Request $request){
        $this->ProductSelect->findOrFail($request->id)->update([
            'delete'=>1,
        ]);
    }

    public function updateProductHasSelect(Request $request){
        foreach ($request['id'] as $key => $id) {
            $this->ProductHasSelect->create([
                'product_select_id'=>$request->select_id,
                'product_id'=>$id,
            ]);
        }
    }

    public function deleteProductHasSelect(Request $request){
        $this->ProductHasSelect->where('product_id',$request->id)->where('product_select_id',$request->select_id)->first()->delete();
    }

    public function updateProductImg(Request $request){
        foreach ($request['id'] as $key => $id) {
            $this->ProductImg->findOrFail($id)->update([
                'img'=>$request['filepath'][$key],
                'sort'=>$request['sort'][$key],
                'state'=>$request['state'][$key]
            ]);
        }
    }

    public function deleteProductImg(Request $request){
        $this->ProductImg->findOrFail($request->id)->update([
            'delete'=>1,
        ]);
    }

    public function createProductImg(Request $request){
        $this->ProductImg->create([
            'product_id'=>$request->product_id,
            'img'=>$request->filepath,
            'sort'=>$request->sort,
            'state'=>$request->state
        ]);
    }
}