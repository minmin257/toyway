<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Subject;

class SubjectRepository
{
    protected $Subject;

    public function __construct(Subject $Subject)
    {
        $this->model = $Subject;
    }

    public function read()
    {
        return $this->model->where('delete',0)->orderBy('sort','desc');
    }

    public function update(Request $request)
    {
        foreach($request['id'] as $key => $id){
            $this->model->findOrFail($id)->update([
                'name'=>$request['name'][$key],
                'link'=>$request['link'][$key],
                'sort'=>$request['sort'][$key],
                'state'=>$request['state'][$key]
            ]);
        }
    }
}