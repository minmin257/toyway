<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\System;

class SystemRepository
{
    protected $System;

    public function __construct(System $System)
    {
        $this->model = $System;
    }

    public function read()
    {
        return $this->model->first();
    }

    public function update(Request $request){
        $this->model->first()->update([
            'sitename'=>$request->sitename,
            'keyword'=>$request->keyword,
            'member_login'=>$request->member_login,
            'maintain'=>$request->maintain,
            'message'=>$request->message,
            'GAcode'=>$request->ga_code,
            'description'=>$request->description
        ]);
    }
}