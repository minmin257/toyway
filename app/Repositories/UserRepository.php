<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserHasPremission;

class UserRepository
{
    protected $User;

    public function __construct(User $User, UserHasPremission $UserHasPremission)
    {
        $this->model = $User;
        $this->UserHasPremission = $UserHasPremission;
    }

    public function read()
    {
        return $this->model->where('delete',0);
    }

    public function update(Request $request){
        if($request->filled('password')){
            $this->model->findOrFail($request->id)->update([
                'name'=>$request->name,
                'password'=>bcrypt($request->password),
            ]);
        }
        else{
            $this->model->findOrFail($request->id)->update([
                'name'=>$request->name,
            ]);
        }
    }

    public function update_state(Request $request){
        $this->model->findOrFail($request->id)->update([
            'state'=>$request->state,
        ]);
    }

    public function premission_delete(Request $request){
        $this->UserHasPremission->where('user_id',$request['id'])->delete();
    }

    public function premission_create(Request $request){
        foreach($request['state'] as $key => $value){
            //啟用的value
            if($value){
                $this->UserHasPremission->create([
                    'user_id'=>$request['id'],
                    'premission_id'=>$value,
                ]);
            }
        }
    }

    public function create(Request $request){
        $this->model->create([
            'account'=>$request->account,
            'password'=>bcrypt($request->password),
            'name'=>$request->name,
            'state'=>$request->state,
        ]);
    }

    public function delete(Request $request){
        $this->model->findOrFail($request->id)->update([
            'delete'=>1,
        ]);
    }
}