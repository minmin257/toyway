<?php
namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Video;

class VideoRepository
{
    protected $Video;

    public function __construct(Video $Video)
    {
        $this->model = $Video;
    }

    public function read()
    {
        return $this->model->where('delete',0)->orderBy('sort','desc')->orderBy('date','desc')->orderBy('id','desc');
    }

    public function create(Request $request){
        $Video = $this->model->create([
            'title'=>$request->title,
            'youtube_id'=>$request->youtube_id,
            'content'=>$request->content,
            'sort'=>$request->sort,
            'state'=>$request->state,
            'date'=>$request->date,
        ]);

        $Video->update([
            'share_comment_url' => url('/special/'.$Video->id),
        ]);
    }

    public function update(Request $request){
        $this->model->findOrFail($request->id)->update([
            'title'=>$request->title,
            'youtube_id'=>$request->youtube_id,
            'content'=>$request->content,
            'share_comment_url' => $request->share_comment_url,
            'sort'=>$request->sort,
            'state'=>$request->state,
            'date'=>$request->date,
        ]);
    }

    public function delete(Request $request){
        $this->model->findOrFail($request->id)->update([
            'delete'=>1,
        ]);
    }
}