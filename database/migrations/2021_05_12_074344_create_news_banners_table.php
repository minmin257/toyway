<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('news_id')
                ->constrained('news')
                ->onDelete('cascade');
            $table->string('img');
            $table->integer('sort')->comment('順序')->default(0);
            $table->boolean('state')->comment('啟用狀態(0關閉/1顯示)')->default(1);
            $table->boolean('delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_banners');
    }
}
