<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('brand_id')
                ->constrained('product_brands')
                ->onDelete('cascade');
            $table->string('name');
            $table->string('no')->comment('產品編號');
            $table->string('img');
            $table->string('share_comment_url')->nullable();
            $table->longText('content');
            $table->boolean('homepage')->default(0);
            $table->integer('sort')->comment('順序')->default(0);
            $table->boolean('state')->comment('啟用狀態(0關閉/1顯示)')->default(1);
            $table->boolean('delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
