<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('systems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sitename');
            $table->string('website')->nullable();
            $table->string('keyword')->nullable();
            $table->boolean('maintain')->default(0);
            $table->longText('message');
            $table->longText('description')->nullable();
            $table->longText('GAcode')->nullable();
            $table->boolean('member_login')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('systems');
    }
}
