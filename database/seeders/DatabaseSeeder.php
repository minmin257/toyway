<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(UserTableSeeder::class);
        $this->call(SubjectTableSeeder::class);
        $this->call(PageTableSeeder::class);
        $this->call(LinkTableSeeder::class);
        $this->call(VideoTableSeeder::class);
        $this->call(FooterTableSeeder::class);
        $this->call(HomeBannerTableSeeder::class);
        $this->call(InnerBannerTableSeeder::class);
        $this->call(SystemTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(ProductTableSeeder::class);

    }
}
