<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Footer;

class FooterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Footer::create([
            'content'=>'<h5 class="title">
            <span>東匯企業有限公司</span>
            <span>新城企業有限公司</span>
            <span>元大玩具股份有限公司</span>
          </h5>
          <div class="d-lg-flex justify-content-between pb-3">
            <div class="phone">
              <span>
                <i class="icon">
                  <img src="images/icon-phone.png" alt="">
                </i>
                <a href="tel:0229138859">台北 (02) 2913-8859</a>
              </span>
              <span>
                <i class="icon">
                  <img src="images/icon-phone.png" alt="">
                </i>
                <a href="tel:077214382">高雄 (07) 721-4382</a>
              </span>
            </div>
            <div class="copyright mb-5">
              © 2020  TOYWAY, all rights reserved. 
            </div>
          </div>'
        ]);
    }
}
