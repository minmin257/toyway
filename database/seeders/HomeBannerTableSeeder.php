<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\HomeBanner;

class HomeBannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HomeBanner::create([
            'img'=>'/images/banner-01.jpg',
        ]);
        HomeBanner::create([
            'img'=>'/images/banner-02.jpg',
        ]);
        HomeBanner::create([
            'img'=>'/images/banner-03.jpg',
        ]);
    }
}
