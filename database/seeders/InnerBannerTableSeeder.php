<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\InnerBanner;

class InnerBannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InnerBanner::create([
            'subject_id'=>1,
            'img'=>'/images/banner-03.jpg'
        ]);
        InnerBanner::create([
            'subject_id'=>2,
            'img'=>'/images/banner-03.jpg'
        ]);
        InnerBanner::create([
            'subject_id'=>3,
            'img'=>'/images/banner-03.jpg'
        ]);
        InnerBanner::create([
            'subject_id'=>4,
            'img'=>'/images/banner-03.jpg'
        ]);
        InnerBanner::create([
            'subject_id'=>5,
            'img'=>'/images/banner-03.jpg'
        ]);
        InnerBanner::create([
            'subject_id'=>6,
            'img'=>'/images/banner-03.jpg'
        ]);
        InnerBanner::create([
            'subject_id'=>7,
            'img'=>'/images/banner-03.jpg'
        ]);
        InnerBanner::create([
            'subject_id'=>8,
            'img'=>'/images/banner-03.jpg'
        ]);
        InnerBanner::create([
            'subject_id'=>9,
            'img'=>'/images/banner-03.jpg'
        ]);
        InnerBanner::create([
            'subject_id'=>10,
            'img'=>'/images/banner-03.jpg'
        ]);
    }
}
