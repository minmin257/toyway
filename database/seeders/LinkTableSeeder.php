<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Link;

class LinkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Link::create([
            'title'=>'facebook粉絲頁',
            'content'=>'東匯玩具最新消息<br>
            圖片影片旗下招<br>
            介紹文字1234567891023456',
            'img'=>'/images/fb-img.png',
            'link'=>'http://www.toyway.com.tw/'
        ]);
        Link::create([
            'title'=>'instagram',
            'content'=>'東匯玩具最新消息<br>
            不定時更新限時動態<br>
            沒事滑滑不無聊',
            'img'=>'/images/ig-img.png',
            'link'=>'http://www.toyway.com.tw/'
        ]);
        Link::create([
            'title'=>'YouTube專屬頻道',
            'content'=>'扭蛋玩具開箱與介紹<br>
            大小展覽直擊影片',
            'img'=>'/images/yt-img.png',
            'link'=>'http://www.toyway.com.tw/'
        ]);
    }
}
