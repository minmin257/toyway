<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\NewsCategory;
use App\Models\NewsBanner;
use App\Models\News;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        NewsCategory::create([
            'name'=>'最新消息'
        ]);
        NewsCategory::create([
            'name'=>'新品上市'
        ]);
        NewsCategory::create([
            'name'=>'商品預告'
        ]);
        
        for($i=0; $i<=11; $i++){
            News::create([
                'category_id'=>1,
                'title'=>'C1香港設計團隊Fluffy House X 東匯玩具 旗下最招牌的白雲家族首度扭蛋化 《白雲寶寶環保扭蛋》 標題最多三行 香港設計團隊Fluffy House X 東匯玩具 旗下最招牌的白雲家族首度扭蛋化 《白雲寶寶環保扭蛋》 標題最多三行 香港設計團隊Fluffy',
                'content'=>'<p>
                "杯緣子"的推手-知名的「奇譚俱樂部」首度與台灣原型師 <br>
                RBEN 阿班 (黃柏翔) 聯名合作推出<br>奇譚史上最巨大！最逼真！最具份量感的作品 <br>
                全4種，各500日圓   台灣預計10月到貨<br>
                <br>
                「奇譚俱樂部」首度與台灣原型師 「奇譚俱樂部」首度與台灣原型師<br>
                RBEN 阿班 (黃柏翔) 聯名合作推出RBEN 阿班 (黃柏翔) 聯名合作推出<br>
                奇譚史上最巨大！最逼真！最具份量感的作品 <br>
                <br>
                全4種，各500日圓   台灣預計10月到貨全4種<br>
                "杯緣子"的推手-知名的「奇譚俱樂部」首度與台灣原型師 <br>
                RBEN 阿班 (黃柏翔) 聯名合作推出<br>
                奇譚史上最巨大！最逼真！最具份量感的作品 <br>                </p>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <p>
                  <img src="images/img-01.png" alt="" style="max-width: 100%;display: block;margin:0 auto;">
                </p>
                <p>
                  "杯緣子"的推手-知名的「奇譚俱樂部」首度與台灣原型師 <br>
                  RBEN 阿班 (黃柏翔) 聯名合作推出<br>
                  奇譚史上最巨大！最逼真！最具份量感的作品 
                </p>
                <p>
                  <img src="images/img-02.png" alt="" style="max-width: 100%;display: block;margin:0 auto;">
                </p>
                <p>
                  "杯緣子"的推手-知名的「奇譚俱樂部」首度與台灣原型師 <br>
                  RBEN 阿班 (黃柏翔) 聯名合作推出<br>
                  奇譚史上最巨大！最逼真！最具份量感的作品 
                </p>',
                'date'=>date('Y-m-d'),
                'img'=>'/images/article-img.png',
            ]);
        }

        for($i=0; $i<=11; $i++){
            News::create([
                'category_id'=>2,
                'title'=>'C2香港設計團隊Fluffy House X 東匯玩具 旗下最招牌的白雲家族首度扭蛋化 《白雲寶寶環保扭蛋》 標題最多三行 香港設計團隊Fluffy House X 東匯玩具 旗下最招牌的白雲家族首度扭蛋化 《白雲寶寶環保扭蛋》 標題最多三行 香港設計團隊Fluffy',
                'content'=>'<p>
                "杯緣子"的推手-知名的「奇譚俱樂部」首度與台灣原型師 <br>
                RBEN 阿班 (黃柏翔) 聯名合作推出<br>奇譚史上最巨大！最逼真！最具份量感的作品 <br>
                全4種，各500日圓   台灣預計10月到貨<br>
                <br>
                「奇譚俱樂部」首度與台灣原型師 「奇譚俱樂部」首度與台灣原型師<br>
                RBEN 阿班 (黃柏翔) 聯名合作推出RBEN 阿班 (黃柏翔) 聯名合作推出<br>
                奇譚史上最巨大！最逼真！最具份量感的作品 <br>
                <br>
                全4種，各500日圓   台灣預計10月到貨全4種<br>
                "杯緣子"的推手-知名的「奇譚俱樂部」首度與台灣原型師 <br>
                RBEN 阿班 (黃柏翔) 聯名合作推出<br>
                奇譚史上最巨大！最逼真！最具份量感的作品 <br>                </p>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <p>
                  <img src="images/img-01.png" alt="" style="max-width: 100%;display: block;margin:0 auto;">
                </p>
                <p>
                  "杯緣子"的推手-知名的「奇譚俱樂部」首度與台灣原型師 <br>
                  RBEN 阿班 (黃柏翔) 聯名合作推出<br>
                  奇譚史上最巨大！最逼真！最具份量感的作品 
                </p>
                <p>
                  <img src="images/img-02.png" alt="" style="max-width: 100%;display: block;margin:0 auto;">
                </p>
                <p>
                  "杯緣子"的推手-知名的「奇譚俱樂部」首度與台灣原型師 <br>
                  RBEN 阿班 (黃柏翔) 聯名合作推出<br>
                  奇譚史上最巨大！最逼真！最具份量感的作品 
                </p>',
                'date'=>date('Y-m-d'),
                'img'=>'/images/article-img.png',
            ]);
        }

        for($i=0; $i<=3; $i++){
            News::create([
                'category_id'=>3,
                'title'=>'C3香港設計團隊Fluffy House X 東匯玩具 旗下最招牌的白雲家族首度扭蛋化 《白雲寶寶環保扭蛋》 標題最多三行 香港設計團隊Fluffy House X 東匯玩具 旗下最招牌的白雲家族首度扭蛋化 《白雲寶寶環保扭蛋》 標題最多三行 香港設計團隊Fluffy',
                'content'=>'<p>
                "杯緣子"的推手-知名的「奇譚俱樂部」首度與台灣原型師 <br>
                RBEN 阿班 (黃柏翔) 聯名合作推出<br>奇譚史上最巨大！最逼真！最具份量感的作品 <br>
                全4種，各500日圓   台灣預計10月到貨<br>
                <br>
                「奇譚俱樂部」首度與台灣原型師 「奇譚俱樂部」首度與台灣原型師<br>
                RBEN 阿班 (黃柏翔) 聯名合作推出RBEN 阿班 (黃柏翔) 聯名合作推出<br>
                奇譚史上最巨大！最逼真！最具份量感的作品 <br>
                <br>
                全4種，各500日圓   台灣預計10月到貨全4種<br>
                "杯緣子"的推手-知名的「奇譚俱樂部」首度與台灣原型師 <br>
                RBEN 阿班 (黃柏翔) 聯名合作推出<br>
                奇譚史上最巨大！最逼真！最具份量感的作品 <br>                </p>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <p>
                  <img src="images/img-01.png" alt="" style="max-width: 100%;display: block;margin:0 auto;">
                </p>
                <p>
                  "杯緣子"的推手-知名的「奇譚俱樂部」首度與台灣原型師 <br>
                  RBEN 阿班 (黃柏翔) 聯名合作推出<br>
                  奇譚史上最巨大！最逼真！最具份量感的作品 
                </p>
                <p>
                  <img src="images/img-02.png" alt="" style="max-width: 100%;display: block;margin:0 auto;">
                </p>
                <p>
                  "杯緣子"的推手-知名的「奇譚俱樂部」首度與台灣原型師 <br>
                  RBEN 阿班 (黃柏翔) 聯名合作推出<br>
                  奇譚史上最巨大！最逼真！最具份量感的作品 
                </p>',
                'date'=>date('Y-m-d'),
                'img'=>'/images/article-img.png',
            ]);
        }

        $News = News::all();
        $url = null;
        foreach($News as $key => $New){
            switch($New->category_id){
                case 1:
                    $url = 'news';
                    break;
                case 2:
                    $url = 'newProduct';
                    break;
                default:
                $url = 'reviewProduct';
            }
             
            $New->update([
                // 'share_comment_url'=>'http://127.0.0.1:8000/news/'.$url.'?id='.$New->id
                'share_comment_url'=>'https://toyway.jhong-demo.com.tw/news/'.$url.'?id='.$New->id
            ]);
        }

        NewsBanner::create([
            'news_id'=>1,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>1,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>1,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>2,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>2,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>2,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>3,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>3,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>3,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>4,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>4,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>4,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>5,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>5,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>5,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>6,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>6,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>6,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>7,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>7,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>7,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>8,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>8,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>8,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>9,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>9,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>9,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>10,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>10,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>10,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>11,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>11,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>11,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>12,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>12,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>12,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>13,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>13,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>13,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>14,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>14,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>14,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>15,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>15,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>15,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>16,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>16,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>16,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>17,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>17,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>17,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>18,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>18,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>18,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>19,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>19,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>19,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>20,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>20,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>20,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>21,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>21,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>21,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>22,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>22,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>22,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>23,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>23,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>23,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>24,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>24,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>24,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>25,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>25,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>25,
            'img'=>'/images/img-02.png'
        ]);

        NewsBanner::create([
            'news_id'=>26,
            'img'=>'/images/img-01.png'
        ]);
        NewsBanner::create([
            'news_id'=>26,
            'img'=>'/images/article-img2.png'
        ]);
        NewsBanner::create([
            'news_id'=>26,
            'img'=>'/images/img-02.png'
        ]);
    }
}
