<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Page;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::create([
            'subject_id'=>1,
            'content'=>'<div style="padding:20px;">
            <h3 style="height:45px;display: block;margin:1rem 0;background-image: url(images/title-bg.png);background-position: left top;background-repeat: no-repeat;font-size: 20px;line-height: 32px;">
              <strong style="width: 140px;text-align: center; display: block;">代 理 品 牌</strong>
            </h3>
            <p>
              <img style="width: 100%;display: block;margin:0 auto;" src="images/about-brand.png" alt="">
              <br>
              <br>
            </p>
          </div>
          
          <div style="padding:20px;">
            <h3 style="height:45px;display: block;margin:1rem 0;background-image: url(images/title-bg.png);background-position: left top;background-repeat: no-repeat;font-size: 20px;line-height: 32px;">
              <strong style="width: 140px;text-align: center; display: block;">服 務 項 目</strong>
            </h3>
            <p style="font-size: 18px;">
              東匯玩具成立00年 日本正版扭蛋代理 扭蛋機販售 傳統玩具批發 東匯玩具成立00年 日本正版扭蛋代理 扭蛋機販售 傳統玩具批發 東匯玩具成立00年 日本正版扭蛋代理 扭蛋機販售 傳統玩具批發 東匯玩具成立00年<br>
              <br>
            </p>
          </div>
          
          <hr style="border:1px solid #3f1606;">

          <div style="padding:20px;">
            <h3 style="height:45px;display: block;margin:1rem 0;background-image: url(images/title-bg.png);background-position: left top;background-repeat: no-repeat;font-size: 20px;line-height: 32px;">
              <strong style="width: 140px;text-align: center; display: block;">批 發 須 知</strong>
            </h3>
            <p style="font-size: 18px;">
              東匯玩具成立00年 日本正版扭蛋代理 扭蛋機販售 傳統玩具批發 東匯玩具成立00年 日本正版扭蛋代理 扭蛋機販售 <br>
              <br>
            </p>
          </div>
          
          <hr style="border:1px solid #3f1606;">

          <div style="padding:20px;">
            <h3 style="height:45px;display: block;margin:1rem 0;background-image: url(images/title-bg.png);background-position: left top;background-repeat: no-repeat;font-size: 20px;line-height: 32px;">
              <strong style="width: 140px;text-align: center; display: block;">服 務 項 目</strong>
            </h3>
            <p style="font-size: 18px;">
              購買扭蛋機 批發扭蛋商品 歡迎於上班時間來電<br>
              星期一~星期五  9:00~17:00<br>
              <br>
            </p>
          </div>
          <hr style="border:1px solid #3f1606;">'
        ]);
        
        Page::create([
            'subject_id'=>9,
        ]);

        Page::create([
            'subject_id'=>7,
            'content'=>'圖文編輯預留位置'
        ]);
    }
}
