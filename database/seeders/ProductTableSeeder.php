<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ProductCategory;
use App\Models\ProductBrand;
use App\Models\Product;
use App\Models\ProductImg;
use App\Models\ProductSelect;
use App\Models\ProductHasSelect;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductCategory::create([
            'name'=>'扭蛋商品',
            'content'=>'扭蛋商品敘述'
        ]);
        ProductCategory::create([
            'name'=>'扭蛋機台與周邊',
            'content'=>'扭蛋機台與周邊敘述'
        ]);
        ProductCategory::create([
            'name'=>'一般傳統玩具',
        ]);

        ProductBrand::create([
            'category_id'=>1,
            'name'=>'AOSHIMA',
        ]);
        ProductBrand::create([
            'category_id'=>1,
            'name'=>'ATenterprize',
        ]);
        ProductBrand::create([
            'category_id'=>1,
            'name'=>'BANDAI',
        ]);
        

        ProductBrand::create([
            'category_id'=>2,
            'name'=>'AOSHIMA',
        ]);
        ProductBrand::create([
            'category_id'=>2,
            'name'=>'ATenterprize',
        ]);
        ProductBrand::create([
            'category_id'=>2,
            'name'=>'BANDAI',
        ]);
        

        ProductBrand::create([
            'category_id'=>3,
            'name'=>'AOSHIMA',
        ]);
        ProductBrand::create([
            'category_id'=>3,
            'name'=>'ATenterprize',
        ]);
        ProductBrand::create([
            'category_id'=>3,
            'name'=>'BANDAI',
        ]);
        

        for($i=0; $i<=5; $i++){
            Product::create([
                'brand_id'=>1,
                'name'=>'胖腹動物的生活造型公仔',
                'no'=>'123456789',
                'img'=>'/images/img-product-01.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>4,
                'name'=>'胖腹動物的生活造型公仔',
                'no'=>'123456789',
                'img'=>'/images/img-product-01.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>7,
                'name'=>'胖腹動物的生活造型公仔',
                'no'=>'123456789',
                'img'=>'/images/img-product-01.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);
        }

        for($i=0; $i<=5; $i++){
            Product::create([
                'brand_id'=>1,
                'name'=>'陶藝家LisaLarson 小陶貓系列公仔P4',
                'no'=>'123456789',
                'img'=>'/images/14335_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>4,
                'name'=>'陶藝家LisaLarson 小陶貓系列公仔P4',
                'no'=>'123456789',
                'img'=>'/images/14335_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>7,
                'name'=>'陶藝家LisaLarson 小陶貓系列公仔P4',
                'no'=>'123456789',
                'img'=>'/images/14335_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);
        }
        
        for($i=0; $i<=5; $i++){
            Product::create([
                'brand_id'=>1,
                'name'=>'精靈寶可夢泡泡浴公仔',
                'no'=>'123456789',
                'img'=>'/images/15069_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>4,
                'name'=>'精靈寶可夢泡泡浴公仔',
                'no'=>'123456789',
                'img'=>'/images/15069_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>7,
                'name'=>'精靈寶可夢泡泡浴公仔',
                'no'=>'123456789',
                'img'=>'/images/15069_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);
        }

        for($i=0; $i<=5; $i++){
            Product::create([
                'brand_id'=>2,
                'name'=>'胖腹動物的生活造型公仔',
                'no'=>'123456789',
                'img'=>'/images/img-product-01.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>5,
                'name'=>'胖腹動物的生活造型公仔',
                'no'=>'123456789',
                'img'=>'/images/img-product-01.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>8,
                'name'=>'胖腹動物的生活造型公仔',
                'no'=>'123456789',
                'img'=>'/images/img-product-01.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);
        }

        for($i=0; $i<=5; $i++){
            Product::create([
                'brand_id'=>2,
                'name'=>'陶藝家LisaLarson 小陶貓系列公仔P4',
                'no'=>'123456789',
                'img'=>'/images/14335_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>5,
                'name'=>'陶藝家LisaLarson 小陶貓系列公仔P4',
                'no'=>'123456789',
                'img'=>'/images/14335_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>8,
                'name'=>'陶藝家LisaLarson 小陶貓系列公仔P4',
                'no'=>'123456789',
                'img'=>'/images/14335_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);
        }
        
        for($i=0; $i<=5; $i++){
            Product::create([
                'brand_id'=>2,
                'name'=>'精靈寶可夢泡泡浴公仔',
                'no'=>'123456789',
                'img'=>'/images/15069_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>5,
                'name'=>'精靈寶可夢泡泡浴公仔',
                'no'=>'123456789',
                'img'=>'/images/15069_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>8,
                'name'=>'精靈寶可夢泡泡浴公仔',
                'no'=>'123456789',
                'img'=>'/images/15069_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);
        }
        
        for($i=0; $i<=5; $i++){
            Product::create([
                'brand_id'=>3,
                'name'=>'胖腹動物的生活造型公仔',
                'no'=>'123456789',
                'img'=>'/images/img-product-01.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>6,
                'name'=>'胖腹動物的生活造型公仔',
                'no'=>'123456789',
                'img'=>'/images/img-product-01.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>9,
                'name'=>'胖腹動物的生活造型公仔',
                'no'=>'123456789',
                'img'=>'/images/img-product-01.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);
        }

        for($i=0; $i<=5; $i++){
            Product::create([
                'brand_id'=>3,
                'name'=>'陶藝家LisaLarson 小陶貓系列公仔P4',
                'no'=>'123456789',
                'img'=>'/images/14335_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>6,
                'name'=>'陶藝家LisaLarson 小陶貓系列公仔P4',
                'no'=>'123456789',
                'img'=>'/images/14335_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>9,
                'name'=>'陶藝家LisaLarson 小陶貓系列公仔P4',
                'no'=>'123456789',
                'img'=>'/images/14335_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);
        }
        
        for($i=0; $i<=5; $i++){
            Product::create([
                'brand_id'=>3,
                'name'=>'精靈寶可夢泡泡浴公仔',
                'no'=>'123456789',
                'img'=>'/images/15069_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>6,
                'name'=>'精靈寶可夢泡泡浴公仔',
                'no'=>'123456789',
                'img'=>'/images/15069_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);

            Product::create([
                'brand_id'=>9,
                'name'=>'精靈寶可夢泡泡浴公仔',
                'no'=>'123456789',
                'img'=>'/images/15069_0.jpg',
                'content'=>'<p style="font-size: 18px;">
                500日幣<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
                全3種<br>
                環保扭蛋! 大尺寸<br>
                商品介紹 商品介紹 商品介紹<br>
                商品介紹 商品介紹 商品介紹<br>
              </p>',
            ]);
        }

        $Products = Product::all();
        foreach($Products as $key => $Product){
            $Product->update([
                // 'share_comment_url'=>'http://127.0.0.1:8000/product/inner/'.$Product->id
                'share_comment_url'=>'https://toyway.jhong-demo.com.tw/product/inner/'.$Product->id
            ]);
        }
        // $countProduct = count(Product::all());
        // for($j=1; $j<=$countProduct; $j++){
            ProductImg::create([
                'product_id'=>1,
                'img'=>'/images/img-02.png',
            ]);
            ProductImg::create([
                'product_id'=>1,
                'img'=>'/images/001.png',
            ]);
            ProductImg::create([
                'product_id'=>1,
                'img'=>'/images/002.png',
            ]);
            ProductImg::create([
                'product_id'=>1,
                'img'=>'/images/2983_0.jpg',
            ]);
            ProductImg::create([
                'product_id'=>1,
                'img'=>'/images/6042_0.jpg',
            ]);
            ProductImg::create([
                'product_id'=>1,
                'img'=>'/images/14335_0.jpg',
            ]);
            ProductImg::create([
                'product_id'=>1,
                'img'=>'/images/15069_0.jpg',
            ]);
            
            ProductImg::create([
                'product_id'=>2,
                'img'=>'/images/img-02.png',
            ]);
            ProductImg::create([
                'product_id'=>2,
                'img'=>'/images/001.png',
            ]);
            ProductImg::create([
                'product_id'=>2,
                'img'=>'/images/002.png',
            ]);
            ProductImg::create([
                'product_id'=>2,
                'img'=>'/images/2983_0.jpg',
            ]);
            ProductImg::create([
                'product_id'=>2,
                'img'=>'/images/6042_0.jpg',
            ]);
            ProductImg::create([
                'product_id'=>2,
                'img'=>'/images/14335_0.jpg',
            ]);
            ProductImg::create([
                'product_id'=>2,
                'img'=>'/images/15069_0.jpg',
            ]);

            ProductImg::create([
                'product_id'=>3,
                'img'=>'/images/img-02.png',
            ]);
            ProductImg::create([
                'product_id'=>3,
                'img'=>'/images/001.png',
            ]);
            ProductImg::create([
                'product_id'=>3,
                'img'=>'/images/002.png',
            ]);
            ProductImg::create([
                'product_id'=>3,
                'img'=>'/images/2983_0.jpg',
            ]);
            ProductImg::create([
                'product_id'=>3,
                'img'=>'/images/6042_0.jpg',
            ]);
            ProductImg::create([
                'product_id'=>3,
                'img'=>'/images/14335_0.jpg',
            ]);
            ProductImg::create([
                'product_id'=>3,
                'img'=>'/images/15069_0.jpg',
            ]);
        // }

        ProductSelect::create([
            'name'=>'30',
        ]);
        ProductSelect::create([
            'name'=>'50',
        ]);
        ProductSelect::create([
            'name'=>'60',
        ]);
        ProductSelect::create([
            'name'=>'100',
        ]);
        ProductSelect::create([
            'name'=>'120',
        ]);

        ProductHasSelect::create([
            'product_id'=>1,
            'product_select_id'=>1
        ]);
        ProductHasSelect::create([
            'product_id'=>1,
            'product_select_id'=>2
        ]);
        ProductHasSelect::create([
            'product_id'=>1,
            'product_select_id'=>3
        ]);

        ProductHasSelect::create([
            'product_id'=>2,
            'product_select_id'=>1
        ]);
        ProductHasSelect::create([
            'product_id'=>2,
            'product_select_id'=>2
        ]);

        ProductHasSelect::create([
            'product_id'=>3,
            'product_select_id'=>4
        ]);
        ProductHasSelect::create([
            'product_id'=>3,
            'product_select_id'=>5
        ]);

        ProductHasSelect::create([
            'product_id'=>4,
            'product_select_id'=>4
        ]);
    }
}
