<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Subject;

class SubjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subject::create([
            'name'=>'關於我們',
            'url'=>'about'
        ]);
        Subject::create([
            'name'=>'最新消息',
            'url'=>'news'
        ]);
        Subject::create([
            'name'=>'新品上市',
            'url'=>'newProduct'
        ]);
        Subject::create([
            'name'=>'商品預告',
            'url'=>'reviewProduct'
        ]);
        Subject::create([
            'name'=>'商品目錄',
            'url'=>'product'
        ]);
        Subject::create([
            'name'=>'特別專區',
            'url'=>'special'
        ]);
        Subject::create([
            'name'=>'相關連結',
            'url'=>'link'
        ]);
        Subject::create([
            'name'=>'會員專區',
            'url'=>'member',
            'link'=>'http://www.toyway.com.tw/'
        ]);
        Subject::create([
            'name'=>'暫時欄位',
            'url'=>'other'
        ]);
        Subject::create([
            'name'=>'搜尋結果',
            'url'=>'search'
        ]);
    }
}
