<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\System;

class SystemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        System::create([
            'sitename'=>'東匯玩具',
            'message'=>'網站維護中，造成不便敬請見諒。',
            'keyword'=>'keyword',
            'description'=>'description',
            'GAcode'=>'GAcode'
        ]);
    }
}
