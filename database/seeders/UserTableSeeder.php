<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Premission;
use App\Models\UserHasPremission;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'account'=>'admin',
            'name'=>'admin',
            'password'=>bcrypt('123456')
        ]);
         User::create([
            'account'=>'yuanpu',
            'name'=>'yuanpu',
            'password'=>bcrypt('123456')
        ]);
        
       	Premission::create([
            'name'=>'內容管理',
        ]);
        Premission::create([
            'name'=>'商品管理',
        ]);
        Premission::create([
            'name'=>'權限管理',
        ]);
        Premission::create([
            'name'=>'系統設定',
        ]);

        UserHasPremission::create([
            'user_id'=>'1',
            'Premission_id'=>'1',
        ]);
        UserHasPremission::create([
            'user_id'=>'1',
            'Premission_id'=>'2',
        ]);
        UserHasPremission::create([
            'user_id'=>'1',
            'Premission_id'=>'3',
        ]);
        UserHasPremission::create([
            'user_id'=>'1',
            'Premission_id'=>'4',
        ]);

        UserHasPremission::create([
            'user_id'=>'2',
            'Premission_id'=>'1',
        ]);
        UserHasPremission::create([
            'user_id'=>'2',
            'Premission_id'=>'2',
        ]);
        UserHasPremission::create([
            'user_id'=>'2',
            'Premission_id'=>'3',
        ]);
        UserHasPremission::create([
            'user_id'=>'2',
            'Premission_id'=>'4',
        ]);
    }
}
