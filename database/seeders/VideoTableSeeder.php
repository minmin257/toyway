<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Video;

class VideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i<=9; $i++){
            Video::create([
                'title'=>'12月新品上市! 星之卡比開箱開箱影片 標題顯示最多2行
                12月新品上市! 星之卡比開箱開箱影片 標題顯示最多2行',
                'youtube_id'=>'TazqNeXIxg4',
                'content'=>'<p>
                "杯緣子"的推手-知名的「奇譚俱樂部」首度與台灣原型師 <br>
                RBEN 阿班 (黃柏翔) 聯名合作推出<br>
                奇譚史上最巨大！最逼真！最具份量感的作品 <br>
                全4種，各500日圓   台灣預計10月到貨
              </p>',
                'date'=>date('Y-m-d')
            ]);
        }

        
        $Videos = Video::all();
        foreach($Videos as $key => $Video){
            $Video->update([
                'share_comment_url'=>'https://toyway.jhong-demo.com.tw/special/'.$Video->id
                // 'share_comment_url'=>'http://127.0.0.1:8000/special/'.$Video->id
            ]);
        }
    }
}
