function ajaxCreate(method, url, data){
    return {
        method: method,
        url: url,
        data: data,
        request: function request() {
            var ajaxRequest = {
                type: this.method,
                url: this.url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: this.data,
                error: function(data) {
                    var error = data.responseJSON;
                    errorsHtml = '';
                    $.each(error.errors, function(key,value){
                            errorsHtml += '<li>'+ value +'</li>';
                    });
                
                    Swal.fire({
                        html: '<span class="f-family" style="color: #707070;font-size: 14px;font-weight:600;">'+errorsHtml+'</span>',
                        icon: 'error',
                        showConfirmButton: false,
                        width: '300px',
                        heightAuto: false,
                    })
                },
                success: function(data) {
                    $('#error_message').html('');
                    Swal.fire({
                        title: '新增成功',
                        icon: 'success',
                        showConfirmButton: false,
                        width: '300px',
                        timer: 1000,
                        heightAuto: false
                    }).then(function(){
                        location.reload();
                    })
                }
            };

            $.ajax(ajaxRequest);
        }
    }
}

function ajaxUpdate(method, url, data, redirect = null){
    return {
        method: method,
        url: url,
        data: data,
        redirect: redirect,
        request: function request() {
            var ajaxRequest = {
                type: this.method,
                url: this.url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: this.data,
                error: function(data) {
                    var error = data.responseJSON;
                    var errorsHtml = '';
                    $.each(error.errors, function(key,value){
                        errorsHtml += '<li>'+ value +'</li>';
                    });
                    Swal.fire({
                        html: '<span class="f-family" style="color: #707070;font-size: 14px;font-weight:600;">'+errorsHtml+'</span>',
                        icon: 'error',
                        showConfirmButton: false,
                        width: '300px',
                        heightAuto: false,
                    })
                },
                success: function(data) {
                    $('#error_message').html('');
                     swal.fire({
                         title: '修改成功!',
                         icon: 'success',
                         showConfirmButton: false,
                         timer: 1000,
                     }).then(function(){
                        if(redirect === null)
                        {
                            location.reload();
                        }
                        else
                        {
                            location.href = redirect;
                        }
                     })
                }
            };

            $.ajax(ajaxRequest);
        }

    }
}

function ajaxDelete(method, url, data){
    console.log(method)
    console.log(url)
    console.log(data)
    return {
        method: method,
        url: url,
        data: data,
        request: function request() {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                  confirmButton: 'btn btn-success',
                  cancelButton: 'btn btn-danger',
                  title: 'delete-swal-title',
                },
                buttonsStyling: false,
                width: '300px'
            })
            swalWithBootstrapButtons.fire({
                title: '您確定要刪除嗎？',
                html: '<span style="color: #707070;font-size: 14px;">此動作將不可回復<span>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: '確定',
                cancelButtonText: '取消',
                reverseButtons: true,
            }).then((result) => {
                if (result.value) {
                    console.log(result.value)
                    $.ajax({
                        type: this.method,
                        url: this.url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: this.data,
                        },
                        success: function(data) {
                            Swal.fire({
                              title: '刪除成功',
                              icon: 'success',
                              showCancelButton: false,
                              showConfirmButton: false,
                              timer: 970,
                              width: '300px',
                              heightAuto: false 
                            }).then(function(){
                                console.log(location.pathname);
                                location.reload()
                                // window.location.replace('http://127.0.0.1:8000'+location.pathname);
                                // window.location.replace('http://bossdog.jhong-demo.com.tw'+location.pathname);
                            })
                        },
                    });

                }
            })
        }
    }
}

function ajaxDeleteArray(method, url, data){
    console.log(method)
    console.log(url)
    console.log(data)
    return {
        method: method,
        url: url,
        data: data,
        request: function request() {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                  confirmButton: 'btn btn-success',
                  cancelButton: 'btn btn-danger',
                  title: 'delete-swal-title',
                },
                buttonsStyling: false,
                width: '300px'
            })
            swalWithBootstrapButtons.fire({
                title: '您確定要刪除嗎？',
                html: '<span style="color: #707070;font-size: 14px;">此動作將不可回復<span>',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: '確定',
                cancelButtonText: '取消',
                reverseButtons: true,
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: this.method,
                        url: this.url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: this.data[0],
                            select_id: this.data[1],
                        },
                        success: function(data) {
                            Swal.fire({
                              title: '刪除成功',
                              icon: 'success',
                              showCancelButton: false,
                              showConfirmButton: false,
                              timer: 970,
                              width: '300px',
                              heightAuto: false 
                            }).then(function(){
                                location.reload()
                            })
                        },
                    });

                }
            })
        }
    }
}

function ajaxMail(method, url, data, redirect = null){
    return {
        method: method,
        url: url,
        data: data,
        redirect: redirect,
        request: function request() {
            var ajaxRequest = {
                type: this.method,
                url: this.url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: this.data,
                error: function(data) {
                    var error = data.responseJSON;
                    errorsHtml = '';
                    errorsHtml += '<li>'+ error.errors.reply +'</li>';
                    // errorsHtml += '</ul></div></div>';
                    // console.log(error.errors)
                    Swal.fire({
                        html: '<span class="f-family" style="color: #707070;font-size: 14px;font-weight:600;">'+errorsHtml+'</span>',
                        icon: 'error',
                        showConfirmButton: false,
                        width: '300px',
                        heightAuto: false,
                        // timer: 3000
                    })
                },
                beforeSend: function(data) {
                    $('#error_message').html('');
                    Swal.fire({
                        html: '<span class="f-family" style="color: #707070;font-size: 14px;font-weight:600;">郵件發送中，請稍後！</span>',
                        icon: 'warning',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        width: '300px',
                        timer: 1500,
                        // onOpen: () => {
                        //     swal.showLoading();
                        // }
                    })
                },
                success: function(data) {
                    Swal.fire({
                        html: '<span class="f-family" style="color: #707070;font-size: 14px;font-weight:600;">成功寄出！</span>',
                        title: '',
                        icon: 'success',
                        showConfirmButton: false,
                        width: '300px',
                        timer: 1000,
                        heightAuto: false
                    }).then(function(){
                        if(redirect === null)
                        {
                            location.reload();
                        }
                        else
                        {
                            location.href = redirect;
                        }
                    })
                }
            };

            $.ajax(ajaxRequest);
        }
    }
}


