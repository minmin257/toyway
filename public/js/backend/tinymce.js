var editor_config = {
    path_absolute : "https://toyway.jhong-demo.com.tw/",
    selector: "textarea.editor",
    toolbar_mode: 'floating',
    // tinycomments_mode: 'embedded',
    // tinycomments_author: 'Author name',
    force_br_newlines : true,
    force_p_newlines : false,
    forced_root_block : '',
    content_style: 'body { font-family:微軟正黑體,sans-serif; font-size:14px; }',
    language: 'zh_TW',
    language_url: "/js/backend/tinymce/langs/zh-TW.js",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern",
      "autoresize"
    ],
    menubar: true,
    toolbar: "insertfile undo redo | styleselect | fontsizeselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist" +
        " outdent indent | link image media",
    // toolbar: 'undo redo | styleselect | forecolor | fontsizeselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | outdent indent | link image | code',
    relative_urls: false, 
    height: 370,
    min_height: 360,
    max_height: 650,
    // 右下tinyMce超連結
    branding: false,
    textcolor_rows: "6",
    textcolor_cols: "8",
    textcolor_map: [
      "000000", "Black",
      "993300", "Burnt orange",
      "333300", "Dark olive",
      "003300", "Dark green",
      "003366", "Dark azure",
      "000080", "Navy Blue",
      "333399", "Indigo",
      "333333", "Very dark gray",
      "800000", "Maroon",
      "FF6600", "Orange",
      "808000", "Olive",
      "008000", "Green",
      "008080", "Teal",
      "0000FF", "Blue",
      "666699", "Grayish blue",
      "808080", "Gray",
      "FF0000", "Red",
      "FF9900", "Amber",
      "99CC00", "Yellow green",
      "339966", "Sea green",
      "33CCCC", "Turquoise",
      "3366FF", "Royal blue",
      "800080", "Purple",
      "999999", "Medium gray",
      "FF00FF", "Magenta",
      "FFCC00", "Gold",
      "FFFF00", "Yellow",
      "00FF00", "Lime",
      "00FFFF", "Aqua",
      "00CCFF", "Sky blue",
      "993366", "Red violet",
      "FFFFFF", "White",
      "FF99CC", "Pink",
      "FFCC99", "Peach",
      "FFFF99", "Light yellow",
      "CCFFCC", "Pale green",
      "CCFFFF", "Pale cyan",
      "99CCFF", "Light sky blue",
      "1f8bbc", "1f8bbc",
      "006eab", "006eab",
      "fe0002", "fe0002",
      "ffae00", "ffae00",



    ],
    // file_browser_callback : function(field_name, url, type, win) {
    //   var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
    //   var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

    //   var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
    //   if (type == 'image') {
    //     cmsURL = cmsURL + "&type=Images";
    //   } else {
    //     cmsURL = cmsURL + "&type=Files";
    //   }

    //   tinyMCE.activeEditor.windowManager.open({
    //     // theme : 'advanced',
    //     file : cmsURL,
    //     title : 'Filemanager',
    //     width : x * 0.8,
    //     height : y * 0.8,
    //     resizable : "yes",
    //     close_previous : "no"
    //   });
    // }
    // field_name
    file_picker_callback: function (callback, value, meta, ) {
        let x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        let y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        let type = 'image' === meta.filetype ? 'Images' : 'Files',
            url  = editor_config.path_absolute + 'laravel-filemanager?editor=tinymce5&type=' + type;
        // var cmsURL = window.location.origin + '/laravel-filemanager?field_name=' +cmsURL+ 'url';
        // // var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        // if (meta.filetype == 'image') {
        //   cmsURL = cmsURL + "&type=Images";
        // } else {
        //   cmsURL = cmsURL + "&type=Files";
        // }   

        // tinymce.activeEditor.windowManager.openUrl({
        //   url : url,
        //   file : cmsURL,
        //   title : 'Filemanager',
        //   width : x * 0.8,
        //   height : y * 0.8,
        //   resizable : "yes",
        //   close_previous : "no",
        //   onMessage: (api, message) => {
        //         callback(message.content);
        //         // console.log(field_name)
        //     }
        // });

        tinymce.activeEditor.windowManager.openUrl({
            url : url,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            onMessage: (api, message) => {
                callback(message.content);
            }
        });
    }
  };

  tinymce.init(editor_config);
  
