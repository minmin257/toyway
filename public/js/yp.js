
// 導入WOW動態
// new WOW().init();




$(function(){

	// 回到頂端
    $('.btn-goTop').click(function(){
      var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
      $body.animate({
        scrollTop: 0
      }, 600);
      return false;
    });
    $(window).scroll(function () {
      var scrollVal = $(this).scrollTop();
      if(scrollVal > 200){
        $('.btn-goTop').fadeIn();
      }else{
        $('.btn-goTop').fadeOut();  
      };
    });



  // footer高度
  var fh = $('.footer').height();
  // console.log(fh);
  $('.wrap').css({
    'margin-bottom':-fh,
    'padding-bottom':fh
  });
  $(window).resize(function(){
    var fh = $('.footer').height();
    // console.log(fh);
    $('.wrap').css({
      'margin-bottom':-fh,
      'padding-bottom':fh
    });
  });


	// menu固定
    var hh = $('.header').height();
    var mh = $('.menu').height();
    var mt = $('.menu').offset().top;
    $('.header-block').css({
      'height':hh
    });
    $('.menu.fixed').css({
      'top': hh
    });
    $('.menu.fixed+.menu-block').css({
      'height': mh
    });

    $(window).resize(function(){
      var hh = $('.header').height();
      var mh = $('.menu').height();
      var mt = $('.menu').offset().top;
      $('.header-block').css({
        'height':hh
      });
      $('.menu.fixed').css({
        'top': hh
      });
      $('.menu.fixed+.menu-block').css({
        'height': mh
      });
    });


    $(window).scroll(function () {
      var hh = $('.header').height();
      var mh = $('.menu').height();
      var scrollVal = $(this).scrollTop();
      if(scrollVal >= mt){
        $('.menu').addClass('fixed');
        $('.menu.fixed').css({
          'top': hh
        });
        $('.menu-block').css({
          'height': mh
        });
      }else{
        $('.menu').removeClass('fixed');
        $('.menu-block').css({
          'height': 0
        });
      };
    });



    // 其他
      $('.log-in-click').click(function(){
        $('.logout-cart').show();
        $('.login-btn').hide();
      });
      $('.logOUTbtn').click(function(){
        $('.login-btn').show();
        $('.logout-cart').hide();
      });

      $('.select-btn').click(function(){
        $(this).toggleClass('active');
      });



      $('.product-img-item').click(function(){
        var imgSRC = $(this).children("img").attr('src');
        $('.product-imgL>img').attr('src',imgSRC);
      });





      var ww = $(window).width();
      if (ww >= 786 ) {
        $('.brand-list .collapse').addClass('show');
        $('.brand-list .brand-list-btn').attr('aria-expanded','true');
      }
      $(window).resize(function(){
        var rww = $(window).width();
        if ( rww >= 786 ) {
          $('.brand-list .collapse').addClass('show');
          $('.brand-list .brand-list-btn').attr('aria-expanded','true');
        } else{
          $('.brand-list .collapse').removeClass('show');
          $('.brand-list .brand-list-btn').attr('aria-expanded','false');
        }
      });



      // var ww = $(window).width();
      // if( ww <= 990){
      //   $('.menuw').addClass('container').removeClass('container-fluid');
      // }else{
      //   $('.menuw').addClass('container-fluid').removeClass('container');
      // }
      // $(window).resize(function(){
      //   var ww = $(window).width();
      //   if( ww <= 990){
      //     $('.menuw').addClass('container').removeClass('container-fluid');
      //   }else{
      //     $('.menuw').addClass('container-fluid').removeClass('container');
      //   }
      // });







    // dropdown-hover
      // $(".dropdown").hover(            
      // function() {
      //     $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
      //     $(this).toggleClass('open');            
      // },
      // function() {
      //     $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
      //     $(this).toggleClass('open');          
      // });





});