@extends('backend.authority.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        編輯帳號
                    </h1>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <a href="{{ route('management') }}" title=""><button type="button" class="btn btn-default">返回</button></a>
                        <input type="button" class="btn btn-primary" value="儲存變更" id="updateButton">
                        <div class="space"></div>
                        @include('errors.errors')
                        <form id="form">
                            <input type="hidden" name="id" value="{{ $User->id }}">
                            <div class="form-group">
                                <label>管理帳號</label>
                                <input type="text" class="form-control" value="{{ $User->account }}" readonly>
                            </div>
                            <div class="form-group">
                                <label>姓名</label>
                                <input type="text" name="name" class="form-control" value="{{ $User->name }}" required>
                            </div>
                            <div class="form-group">
                                <label>修改密碼</label>
                                <input type="password" name="password" class="form-control">
                                <small>若不更改密碼，請空白 ＊6-10碼，可用英文大小寫及基本符號!$#%@\+-_</small>
                            </div>
                            <div class="form-group">
                                <label>確認密碼</label>
                                <input type="password" name="password_confirmation" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>建立日期</label>
                                <p>
                                    {{ $User->created_at }}
                                </p>
                            </div>
                            

                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $('#updateButton').on("click",function(){
        var ajaxRequest = new ajaxUpdate('POST','{{ route('profile.update') }}',$('#form').serialize());
		ajaxRequest.request();
    })
</script>
@endsection