@extends('backend.authority.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        帳號管理
                    </h1>
                </div>
                <div class="row">
            
                    <div class="col-xs-12">
                        <a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
                        <div class="space"></div>
                    </div>

                    <div class="col-xs-12">
                        <div class="table-responsive text-nowrap">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>編輯</th>
                                        <th>刪除</th>
                                        <th>帳號</th>
                                        <th>姓名</th>
                                        <th>權限</th>
                                        <th>狀態</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($Users as $User)
                                    <form id="form{{ $User->id }}">
                                        <input type="hidden" name="id" value="{{ $User->id }}">
                                        <tr>
                                            <td data-title="編輯">
                                                <a href="{{ route('management.edit',['id'=>$User->id]) }}" class="btn btn-info btn-outline btn-circle">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                            </td>
                                            <td data-title="刪除">
                                                <button class="btn btn-danger btn-outline btn-circle" type="button" name="deleteButton" id="deleteButton_{{$User->id}}">
                                                    <i class="far fa-trash-alt"></i>
                                                </button>
                                            </td>
                                            <td data-title="帳號">
                                                {{ $User->account }}
                                            </td>
                                            <td data-title="姓名">
                                                <input type="hidden" name="name" value="{{ $User->name }}">
                                                {{ $User->name }}
                                            </td>
                                            <td data-title="權限">
                                                <a href="{{ route('premission',['id'=>$User->id]) }}" class="btn btn-success" title="">
                                                    權限
                                                </a>
                                            </td>
                                            <td data-title="狀態">
                                                <select name="state" class="form-control" id="changeState_{{ $User->id }}">
                                                    @if($User->state)
                                                        <option value="1" selected>啟用</option>
                                                        <option value="0">停用</option>
                                                    @else
                                                        <option value="1">啟用</option>
                                                        <option value="0" selected>停用</option>
                                                    @endif
                                                </select>
                                            </td>
                                        </tr>
                                    </form>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 新增區塊 -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">新增帳號</h4>
                </div>
                <div class="modal-body">
                    <form id="form_create">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>帳號</label>
                                        <input type="text" class="form-control" name="account">
                                    </div>
                                    <div class="form-group">
                                        <label>密碼</label>
                                        <input type="password" class="form-control" name="password">
                                    </div>
                                    <div class="form-group">
                                        <label>姓名</label>
                                        <input type="text" class="form-control" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label>狀態</label>
                                        <select name="state" class="form-control">
                                            <option value="1">啟用</option>
                                            <option value="0">停用</option>
                                        </select>
                                    </div>
                
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="button" id="createButton" class="btn btn-primary" value="確定送出">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    var changeStates = $('select[name=state]')
    $.each(changeStates, function(i, el){
        $(el).on("change",function(){
            var id = this.id.split('_');
            var ajaxRequest = new ajaxUpdate('POST','{{ route('management.update') }}',$('#form'+id[1]).serialize());
            ajaxRequest.request();
        })
    })

    $('#createButton').on("click",function(){
        tinyMCE.triggerSave();
        var ajaxRequest = new ajaxCreate('POST', '{{ route('user.create') }}', $('#form_create').serialize());
        ajaxRequest.request();
    })

    var deleteButtons = $('button[name=deleteButton]');
    $.each(deleteButtons, function(i,el){
        $(el).on("click",function(){
            var id = this.id.split('_');
            var ajaxRequest = new ajaxDelete('POST', '{{ route('user.delete') }}', id[1]);
            ajaxRequest.request();
        })
    })
</script>
@endsection