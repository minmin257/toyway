@extends('backend.authority.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        {{ $User->name }} - 權限設定
                    </h1>
                </div>
                @include('success.success')
                <div class="row">
                    <div class="col-xs-12">
                        <a href="{{ route('management') }}" class="btn btn-default" title="">返回</a>
                        <input type="button" class="btn btn-primary" value="儲存變更" id="updateButton">
                        <div class="space"></div>

                        <div class="table-responsive text-nowrap">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>標題</th>
                                        <th>啟用否</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <form id="form">
                                        {{-- method="post" action="{{ route('premission.update') }}" --}}
                                        <input type="hidden" name="id" value="{{ $User->id }}">
                                        @foreach($Premissions as $Premission)
                                        <tr>
                                            <td data-title="標題">
                                                {{ $Premission->name }}
                                            </td>
                                            <td data-title="啟用否">
                                                @if($User->premissions->contains('premission_id',$Premission->id))
                                                    <select name="state[]" class="form-control on">
                                                        <option class="on" value="{{ $Premission->id }}" selected>啟用</option>
                                                        <option class="off" value="0">停用</option>
                                                    </select>
                                                @else
                                                    <select name="state[]" class="form-control off">
                                                        <option class="on" value="{{ $Premission->id }}">啟用</option>
                                                        <option class="off" value="0" selected>停用</option>
                                                    </select>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        
                                    </form>
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    //select 換顏色
    $('select.form-control').change(function(){
        var color = $(this).find(':selected').attr('class');
        if ( color === "on") { //已選
            $(this).removeClass('off').addClass('on');
        }else if ( color === "off") {
            $(this).removeClass('on').addClass('off');
        }
    });

    $('#updateButton').on("click",function(){
        var ajaxRequest = new ajaxUpdate('POST','{{ route('premission.update') }}',$('#form').serialize());
		ajaxRequest.request();
    })
</script>
@endsection