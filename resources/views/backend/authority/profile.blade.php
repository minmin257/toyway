@extends('backend.authority.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        基本資料
                    </h1>
                </div>
                @include('success.success')
                <div class="row">
                    <div class="col-xs-12">
                        <input type="button" id="updateButton" class="btn btn-primary" value="儲存變更">
                        <div class="space"></div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <form id="form">
                            <input type="hidden" name="id" value="{{ auth()->user()->id }}">
                            <div class="form-group">
                                <label>管理帳號</label>
                                <input type="text" class="form-control" value="{{ auth()->user()->account }}" readonly>
                            </div>
                            <div class="form-group">
                                <label>姓名</label>
                                <input type="text" name="name" class="form-control" value="{{ auth()->user()->name }}" required>
                            </div>
                            <div class="form-group">
                                <label>修改密碼</label>
                                <input type="password" name="password" class="form-control">
                                <small>若不更改密碼，請空白 ＊6-10碼，可用英文大小寫及基本符號!$#%@\+-_</small>
                            </div>
                            <div class="form-group">
                                <label>確認密碼</label>
                                <input type="password" name="password_confirmation" class="form-control">
                            </div>
                        </form>
                        {{-- 錯誤警示位置 --}}
                        @include('errors.errors')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    $('#updateButton').on("click",function(){
        var ajaxRequest = new ajaxUpdate('POST','{{ route('profile.update') }}',$('#form').serialize());
		ajaxRequest.request();
    })
</script>
@endsection