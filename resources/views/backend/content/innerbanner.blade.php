@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        內頁Banner管理
                    </h1>
                </div>
                @include('success.success')
                <div class="row">
                    <div class="col-xs-12">
                        <input type="button" class="btn btn-primary" id="updateButton" value="儲存變更">
                        <div class="space"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="table-responsive text-nowrap">
                            <table class="table" style="table-layout: fixed;">
                                <thead>
                                    <tr style="display: flex;">
                                        <th style="flex-basis: 10%">標題</th>
                                        <th style="flex-basis: 10%; text-align: center;">刪除圖片</th>
                                        <th style="flex-basis: 30%">圖片<span style="color: red;">(1920 * 640px)</span></th>
                                        <th style="flex-basis: 50%"></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <form id="form">
                                        @foreach($Banners as $key => $Banner)
                                                <input type="hidden" name="innerbanner_id[]" value="{{ $Banner->id }}">
                                                <input type="hidden" id="thumbnail{{$Banner->id}}" id="filepath_{{ $Banner->id }}" name="innerbanner_filepath[]" value="{{ $Banner->img }}">
                                                <tr style="display: flex;">
                                                    <td data-title="標題" style="flex-basis: 10%">
                                                        {{ $Banner->subject->name }}	
                                                        <input type="hidden" class="form-control" name=subject_id[] value="{{ $Banner->subject->id }}">	
                                                    </td>
                                                    <td data-title="刪除圖片" style="text-align: center; flex-basis: 10%;">
                                                        <button class="btn btn-danger btn-outline btn-circle" type="button" id="deleteButton_{{ $Banner->id }}" name="deleteButton">
                                                            <i class="far fa-trash-alt"></i>
                                                        </button>
                                                    </td>
                                                    <td data-title="圖片" style="flex-basis: 30%">
                                                        <div class="img-responsive img-shadow lfm banner-img" data-input="thumbnail{{$Banner->id}}" data-preview="holder{{$Banner->id}}" id="holder{{$Banner->id}}"><img src="{{ $Banner->img ? $Banner->img : '/images/img-input.jpg' }}"></div>
                                                    </td>
                                                    <td style="flex-basis: 50%">
                                                    </td>
                                                </tr>	
                                        @endforeach
                                    </form>
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $('.lfm').filemanager('image');
    });
    
    $('#updateButton').on("click",function(){
        var ajaxRequest = new ajaxUpdate('POST','{{ route('innerbanner.update') }}',$('#form').serialize());
		ajaxRequest.request();
    })

    var deleteButtons = $('button[name=deleteButton]')
    $.each(deleteButtons, function(i, el){
        $(el).on("click", function(){
            id = this.id.split('_')
            $('#holder'+id[1]+' img').attr('src','/images/img-input.jpg')
            $('#thumbnail'+id[1]).val(null)
        })
    })
</script>
@endsection