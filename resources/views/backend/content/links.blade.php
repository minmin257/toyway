@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        連結列表
                    </h1>
                </div>
                @include('success.success')
                <div class="row">
                    <div class="col-xs-12">
                        <a href="#" class="btn btn-default" role="button" data-toggle="modal"
                           data-target="#myModal">新增</a>
                        <div class="space"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="table-responsive text-nowrap">
                            <table class="table" style="table-layout: fixed;">
                                <thead>
                                    <tr style="display: flex;">
                                        <th style="flex-basis: 10%">編輯</th>
                                        <th style="flex-basis: 10%">刪除</th>
                                        <th style="flex-basis: 60%">標題</th>
                                        <th style="flex-basis: 10%; text-align: center;">順序</th>
                                        <th style="flex-basis: 10%">啟用狀態</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <form id="form">
                                        @foreach($Links as $key => $Link)
                                                <input type="hidden" name="id[]" value="{{ $Link->id }}">
                                                <input type="hidden" id="thumbnail{{$key}}" name="filepath[]" value="{{ $Link->img }}">
                                                <tr style="display: flex;">
                                                    <td data-title="編輯" style="flex-basis: 10%">
                                                        <a href="{{ route('links.edit',['id'=>$Link->id]) }}" class="btn btn-info btn-outline btn-circle">
                                                            <i class="fas fa-pencil-alt"></i>
                                                        </a>
                                                    </td>
                                                    <td data-title="刪除" style="flex-basis: 10%">
                                                        <button class="btn btn-danger btn-outline btn-circle" type="button" id="deleteButton_{{ $Link->id }}" name="deleteButton">
                                                            <i class="far fa-trash-alt"></i>
                                                        </button>
                                                    </td>
                                                    <td data-title="標題" style="flex-basis: 60%">
                                                        {{ $Link->title }}	
                                                    </td>
                                                    <td data-title="順序" style="flex-basis: 10%; text-align: center;">
                                                        {{ $Link->sort }}
                                                    </td>
                                                    <td data-title="啟用狀態" style="flex-basis: 10%">
                                                        @if ($Link->state == 1)
                                                            啟用
                                                        @else
                                                            關閉    
                                                        @endif
                                                    </td>
                                                </tr>	
                                        @endforeach
                                    </form>
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 新增區塊 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">新增連結</h4>
            </div>
            <div class="modal-body">
                <form id="form_create">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>圖片</label>
                                    <button class="form-control btn btn-primary lfm" data-input="thumbnail" data-preview="holder">
                                        <i class="far fa-image"></i>上傳圖片
                                    </button>
                                    <input id="thumbnail" class="form-control" type="hidden" name="filepath">
                                </div>
                                <div class="form-group">
                                    <div class="create-news-img" data-input="thumbnail" data-preview="holder" id="holder"></div>
                                </div>
                                <div class="form-group">
                                    <label>標題</label>
                                    <input type="text" class="form-control" name="title">
                                </div>
                                <div class="form-group">
                                    <label>連結</label>
                                    <input type="text" class="form-control" name="link" value="">
                                </div>
                                <div class="form-group">
                                    <label>內容</label>
                                    <textarea name="content" class="form-control editor"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>順序</label>
                                    <input type="number" class="form-control" name="sort" pattern="[0-9]" min="0" value="0">
                                </div>
                                <div class="form-group">
                                    <label>啟用狀態</label>
                                    <select class="form-control" name="state">
                                        <option value="1" selected>啟用</option>
                                        <option value="0">關閉</option>
                                    </select>
                                </div>
                                {{-- 錯誤警示位置 --}}
                                @include('errors.errors')
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->
                </form>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-primary" value="確定送出" id="createButton">
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(function() {
        $('.lfm').filemanager('image');
    });

    var deleteButtons = $('button[name=deleteButton]');
    $.each(deleteButtons, function(i,el){
        $(el).on("click",function(){
            var id = this.id.split('_');
            var ajaxRequest = new ajaxDelete('POST', '{{ route('links.delete') }}', id[1]);
            ajaxRequest.request();
        })
    })

    $('#createButton').on("click",function(){
        tinyMCE.triggerSave();
        var ajaxRequest = new ajaxCreate('POST', '{{ route('links.create') }}', $('#form_create').serialize());
        ajaxRequest.request();
    })
</script>
@endsection