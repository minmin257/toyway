@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        連結列表－編輯
                    </h1>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <a href="{{ route('links') }}" class="btn btn-default">返回</a>
                        <input type="button" class="btn btn-primary" value="儲存變更" id="updateButton">
                        <div class="space"></div>
                        
                        <form id="form">
                            <input type="hidden" name="id" value="{{ $Link->id }}">
                            <div class="form-group row">
                                <div class="col-xs-2">
                                    <label>列表圖片<span style="color: red;"> (點擊圖片更換)</span></label>
                                    <input id="thumbnail" class="form-control" type="hidden" name="filepath" value="{{ $Link->img }}">
                                    <div class="update-news-img lfm" data-input="thumbnail" data-preview="holder" id="holder"><img src="{{ $Link->img }}"></div>
                                </div>
                                <div class="col-xs-4">
                                    <label>標題</label>
                                    <input type="text" class="form-control" name="title" value="{{ $Link->title }}">

                                    <label>連結</label>
                                    <input type="text" class="form-control" name="link" value="{{ $Link->link }}">

                                    <label>順序</label>
                                    <input type="number" class="form-control" name="sort" pattern="[0-9]" min="0" value="{{ $Link->sort }}">
                                
                                    <label>啟用狀態</label>
                                    <select class="form-control" name="state">
                                        @if($Link->state)
                                        <option value="1" selected>啟用</option>
                                        <option value="0">關閉</option>
                                        @else
                                        <option value="1">啟用</option>
                                        <option value="0" selected>關閉</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-6">
                                <label>內容</label>
                                <textarea name="content" class="form-control editor">{!! $Link->content !!}</textarea>
                                </div>
                            </div>
                            @include('errors.errors')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $('.lfm').filemanager('image');
    });

    $('#updateButton').on("click",function(){
        tinyMCE.triggerSave();
        var ajaxRequest = new ajaxUpdate('POST','{{ route('links.update') }}',$('#form').serialize(),'{{ route('links') }}');
        ajaxRequest.request();
    })
</script>
@endsection
    {{-- function update() 
		{
			tinyMCE.triggerSave();
			var ajaxRequest = new ajaxUpdate('POST','{{ route('news.update') }}',$('#form').serialize(),'{{ route('backend.newsList') }}');
			ajaxRequest.request();
		} --}}