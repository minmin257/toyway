@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        分類管理
                    </h1>
                </div>
                @include('success.success')
                <div class="row">
                    <div class="col-xs-12">
                        <input type="button" class="btn btn-primary" id="updateButton" value="儲存變更">
                        <div class="space"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="table-responsive text-nowrap">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>消息列表</th>
                                    <th>分類名稱</th>
                                </tr>
                                </thead>

                                <tbody>
                                    <form id="form">
                                    @foreach($NewsCategories as $Category)
                                        <input type="hidden" name="id[]" value="{{ $Category->id }}">
                                        <tr>
                                            <td data-title="消息列表">
                                                <a href="{{ route('news.lists',['id'=>$Category->id]) }}" class="btn btn-warning btn-outline btn-circle">
                                                    <i class="fas fa-bars"></i>
                                                </a>
                                            </td>
                                            <td data-title="分類名稱">
                                                <input type="text" class="form-control" name="name[]" value="{{ $Category->name }}">
                                            </td>
                                        </tr>
                                    @endforeach
                                    </form>
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
<script>
    $('#updateButton').on("click",function(){
        var ajaxRequest = new ajaxUpdate('POST','{{ route('news.categories.update') }}',$('#form').serialize());
		ajaxRequest.request();
    })
</script>
@endsection