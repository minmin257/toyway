@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        文章列表－編輯
                    </h1>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <a href="{{ route('news.lists') }}" class="btn btn-default">返回</a>
                        <input type="button" class="btn btn-primary" value="儲存變更" id="updateButton">
                        <div class="space"></div>
                        
                        <form id="form">
                            <input type="hidden" name="id" value="{{ $News->id }}">
                            <div class="form-group row">
                                <div class="col-xs-2">
                                    <label>列表圖片<span style="color: red;"> (點擊圖片更換)</span></label>
                                    <input id="thumbnail" class="form-control" type="hidden" name="filepath" value="{{ $News->img }}">
                                    <div class="update-news-img lfm" data-input="thumbnail" data-preview="holder" id="holder"><img src="{{ $News->img }}"></div>
                                </div>
                                <div class="col-xs-4">
                                    <label>日期</label>
                                    <input type="date" class="form-control" name="date" value="{{ $News->date }}">
                                
                                    <label>順序</label>
                                    <input type="number" class="form-control" name="sort" pattern="[0-9]" min="0" value="{{ $News->sort }}">
                                
                                    <label>啟用狀態</label>
                                    <select class="form-control" name="state">
                                        @if($News->state)
                                        <option value="1" selected>啟用</option>
                                        <option value="0">關閉</option>
                                        @else
                                        <option value="1">啟用</option>
                                        <option value="0" selected>關閉</option>
                                        @endif
                                    </select>
                                    
                                    <label>文章分類</label>
                                    <select class="form-control" name="category_id">
                                        @foreach ($NewsCategories as $Category)
                                            @if ($Category->id == $News->category_id)
                                                <option value="{{ $Category->id }}" selected>{{ $Category->name }}</option>
                                            @else    
                                                <option value="{{ $Category->id }}">{{ $Category->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    <label>Share&Comment URL</label>
                                    <input type="text" class="form-control" name="share_comment_url" value="{{ $News->share_comment_url }}">

                                    <label>標題</label>
                                    {{-- <input type="text" class="form-control" name="title" value="{{ $News->title }}"> --}}
                                    <textarea name="title" class="form-control" rows="3" style="resize:none;overflow:hidden;">{!! $News->title !!}</textarea>
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-6">
                                <label>內容</label>
                                <textarea name="content" class="form-control editor">{!! $News->content !!}</textarea>
                                </div>
                            </div>
                            @include('errors.errors')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $('.lfm').filemanager('image');
    });

    $('#updateButton').on("click",function(){
        tinyMCE.triggerSave();
        var ajaxRequest = new ajaxUpdate('POST','{{ route('news.update') }}',$('#form').serialize(),'{{ route('news.lists') }}');
        ajaxRequest.request();
    })
</script>
@endsection
    {{-- function update() 
		{
			tinyMCE.triggerSave();
			var ajaxRequest = new ajaxUpdate('POST','{{ route('news.update') }}',$('#form').serialize(),'{{ route('backend.newsList') }}');
			ajaxRequest.request();
		} --}}