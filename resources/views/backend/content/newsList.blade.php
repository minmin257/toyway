@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        文章列表
                    </h1>
                </div>
                @include('success.success')
                <div class="row">
                    <div class="col-xs-12">
                        @if($check)
                            <a href="{{ route('news.categories') }}" class="btn btn-info">返回分類</a>
                            <a href="{{ route('news.lists') }}" class="btn btn-warning">全部文章</a>
                        @endif
                        <a href="#" class="btn btn-default" role="button" data-toggle="modal"
                           data-target="#myModal">新增</a>
                        <div class="space"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="table-responsive text-nowrap">
                            <table class="table">
                                <thead>
                                <tr style="display: flex;">
                                    <th style="flex-basis: 5%">輪播圖</th>
                                    <th style="flex-basis: 5%">編輯</th>
                                    <th style="flex-basis: 5%">刪除</th>
                                    <th style="flex-basis: 10%">分類</th>
                                    <th style="flex-basis: 10%">日期</th>
                                    <th style="flex-basis: 45%">標題</th>
                                    <th style="flex-basis: 10%; text-align: center;">順序</th>
                                    <th style="flex-basis: 10%">啟用狀態</th>
                                </tr>
                                </thead>

                                <tbody>
                                    <form id="form">
                                    @foreach($News as $New)
                                        <input type="hidden" name="id[]" value="{{ $New->id }}">
                                        <tr style="display: flex;">
                                            <td data-title="輪播圖" style="flex-basis: 5%">
                                                <a href="{{ route('news.banner',['id'=>$New->id]) }}" class="btn btn-warning btn-outline btn-circle">
                                                    <i class="fas fa-bars"></i>
                                                </a>
                                            </td>
                                            <td data-title="編輯" style="flex-basis: 5%">
                                                <a href="{{ route('news.edit',['id'=>$New->id]) }}" class="btn btn-info btn-outline btn-circle">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                            </td>
                                            <td data-title="刪除" style="flex-basis: 5%">
                                                <button class="btn btn-danger btn-outline btn-circle" type="button" id="deleteButton_{{ $New->id }}" name="deleteButton">
                                                    <i class="far fa-trash-alt"></i>
                                                </button>
                                            </td>
                                            <td data-title="分類" style="flex-basis: 10%">
                                                {{ $New->category->name }}
                                            </td>
                                            <td data-title="日期" style="flex-basis: 10%">
                                                {{ $New->date }}
                                            </td>
                                            <td data-title="標題" style="flex-basis: 45%; width: 500px;display: block;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;">
                                                {!! $New->title !!}
                                            </td>
                                            <td data-title="順序" style="flex-basis: 10%; text-align: center;">
                                                {{ $New->sort }}
                                            </td>
                                            <td data-title="啟用狀態" style="flex-basis: 10%">
                                                @if ($New->state == 1)
                                                    啟用
                                                @else
                                                    關閉    
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </form>
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- 新增區塊 -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">新增最新消息</h4>
                </div>
                <div class="modal-body">
                    <form id="form_create">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>消息分類</label>
                                        <select class="form-control" name="category_id">
                                            <option value="" style="display:none">請選擇消息分類</option>
                                            @foreach ($NewsCategories as $Category)
                                                <option value="{{ $Category->id }}">{{ $Category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>列表圖片</label>
                                        <button class="form-control btn btn-primary lfm" data-input="thumbnail" data-preview="holder">
                                            <i class="far fa-image"></i>上傳圖片
                                        </button>
                                        <input id="thumbnail" class="form-control" type="hidden" name="filepath">
                                    </div>
                                    <div class="form-group">
                                        <div class="create-news-img" data-input="thumbnail" data-preview="holder" id="holder"></div>
                                    </div>
                                    <div class="form-group">
                                        <label>標題</label>
                                        <textarea name="title" class="form-control" rows="3" style="resize:none;overflow:hidden;"></textarea>
                                        {{-- <input type="text" class="form-control" name="title"> --}}
                                    </div>
                                    <div class="form-group">
                                        <label>日期</label>
                                        <input type="date" class="form-control" name="date" value="{{ date('Y-m-d') }}">
                                    </div>
                                    <div class="form-group">
                                        <label>內容</label>
                                        <textarea name="content" class="form-control editor"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>順序</label>
                                        <input type="number" class="form-control" name="sort" pattern="[0-9]" min="0" value="0">
                                    </div>
                                    <div class="form-group">
                                        <label>啟用狀態</label>
                                        <select class="form-control" name="state">
                                            <option value="1" selected>啟用</option>
                                            <option value="0">關閉</option>
                                        </select>
                                    </div>
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" value="確定送出" id="createButton">
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
<script>
    $(function() {
        $('.lfm').filemanager('image');
    });

    var deleteButtons = $('button[name=deleteButton]');
    $.each(deleteButtons, function(i,el){
        $(el).on("click",function(){
            var id = this.id.split('_');
            var ajaxRequest = new ajaxDelete('POST', '{{ route('news.delete') }}', id[1]);
            ajaxRequest.request();
        })
    })

    $('#createButton').on("click",function(){
        tinyMCE.triggerSave();
        var ajaxRequest = new ajaxCreate('POST', '{{ route('news.create') }}', $('#form_create').serialize());
        ajaxRequest.request();
    })
</script>
@endsection