@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        @switch($Page->subject_id)
                            @case(1)
                                關於我們
                                @break
                            @case(9)
                                預留頁面
                                @break
                            @default
                                相關連結
                        @endswitch
                    </h1>
                </div>
                <div class="row">
                    <div class="col-xs-7">
                        <input type="button" class="btn btn-primary" value="儲存變更" id="updateButton">
                        <div class="space"></div>
                        
                        <form id="form">
                            <input type="hidden" name="id" value="{{ $Page->id }}">
                            <div class="form-group">
                                <label>內容</label>
                                <textarea name="content" class="form-control editor">{!! $Page->content !!}</textarea>
                            </div>
                            @include('errors.errors')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $('.lfm').filemanager('image');
    });

    $('#updateButton').on("click",function(){
        tinyMCE.triggerSave();
        var ajaxRequest = new ajaxUpdate('POST','{{ route('pages.update') }}',$('#form').serialize());
        ajaxRequest.request();
    })
</script>
@endsection