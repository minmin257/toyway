@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        分類管理
                    </h1>
                </div>
                @include('success.success')
                <div class="row">
                    <div class="col-xs-12">
                        {{-- <input type="button" class="btn btn-primary" id="updateButton" value="儲存變更"> --}}
                        <div class="space"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="table-responsive text-nowrap">
                            <table class="table">
                                <thead>
                                <tr style="display: flex;">
                                    <th style="flex-basis: 10%">品牌列表</th>
                                    <th style="flex-basis: 10%">編輯</th>
                                    <th style="flex-basis: 80%">分類名稱</th>
                                </tr>
                                </thead>

                                <tbody>
                                    <form id="form">
                                    @foreach($ProductCategories as $Category)
                                        <input type="hidden" name="id[]" value="{{ $Category->id }}">
                                        <tr style="display: flex;">
                                            <td data-title="品牌列表" style="flex-basis: 10%">
                                                <a href="{{ route('product.brands',['id'=>$Category->id]) }}" class="btn btn-warning btn-outline btn-circle">
                                                    <i class="fas fa-bars"></i>
                                                </a>
                                            </td>
                                            <td data-title="編輯" style="flex-basis: 10%">
                                                <a href="{{ route('product.categories.edit',['id'=>$Category->id]) }}" class="btn btn-info btn-outline btn-circle">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                            </td>
                                            <td data-title="分類名稱" style="flex-basis: 80%">
                                                {{ $Category->name }}
                                                {{-- <input type="text" class="form-control" name="name[]" value="{{ $Category->name }}"> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </form>
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
