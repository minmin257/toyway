@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        商品分類－編輯
                    </h1>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <a href="{{ route('product.categories') }}" class="btn btn-default">返回</a>
                        <input type="button" class="btn btn-primary" value="儲存變更" id="updateButton">
                        <div class="space"></div>
                        
                        <form id="form">
                            <input type="hidden" name="id" value="{{ $ProductCategory->id }}">
                            <div class="form-group row">
                                <div class="col-xs-7">
                                    <label>分類名稱</label>
                                    <input type="text" class="form-control" name="name" value="{{ $ProductCategory->name }}">
                                
                                    <label>商品描述</label>
                                    <textarea name="content" class="form-control editor">{!! $ProductCategory->content !!}</textarea>
                                </div>
                            </div>
                            @include('errors.errors')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $('.lfm').filemanager('image');
    });
    
    $('#updateButton').on("click",function(){
        tinyMCE.triggerSave();
        var ajaxRequest = new ajaxUpdate('POST','{{ route('product.categories.update') }}',$('#form').serialize());
		ajaxRequest.request();
    })
</script>
@endsection