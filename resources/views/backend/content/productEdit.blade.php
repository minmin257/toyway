@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        商品列表－編輯
                    </h1>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <a href="{{ route('product.lists') }}" class="btn btn-default">返回</a>
                        <input type="button" class="btn btn-primary" value="儲存變更" id="updateButton">
                        <div class="space"></div>
                        
                        <form id="form">
                            <input type="hidden" name="id" value="{{ $Product->id }}">
                            <div class="form-group row">
                                <div class="col-xs-3">
                                    <label>列表圖片<span style="color: red;"> (點擊圖片更換)</span></label>
                                    <input id="thumbnail" class="form-control" type="hidden" name="filepath" value="{{ $Product->img }}">
                                    <div class="update-product-img lfm" data-input="thumbnail" data-preview="holder" id="holder"><img src="{{ $Product->img }}"></div>
                                </div>
                                <div class="col-xs-4">
                                    <label>商品分類</label>
                                    <select class="form-control" id="category" name="category_id">
                                        @foreach ($ProductCategories as $Category)
                                            @if ($Category->id == $Product->brand->category->id)
                                                <option value="{{ $Category->id }}" selected>{{ $Category->name }}</option>
                                            @else    
                                                <option value="{{ $Category->id }}">{{ $Category->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    <label>商品品牌</label>
                                    <select class="form-control" id="brand" name="brand_id">
                                        <option value="{{ $Product->brand->id }}" selected>{{ $Product->brand->name }}</option>
                                    </select>

                                    <label>商品名稱</label>
                                    <input type="text" class="form-control" name="name" value="{{ $Product->name }}">

                                    <label>商品編號</label>
                                    <input type="text" class="form-control" name="no" value="{{ $Product->no }}">
                                    
                                    <label>Share&Comment URL</label>
                                    <input type="text" class="form-control" name="share_comment_url" value="{{ $Product->share_comment_url }}">

                                    <label>順序</label>
                                    <input type="number" class="form-control" name="sort" pattern="[0-9]" min="0" value="{{ $Product->sort }}">
                                
                                    <label>啟用狀態</label>
                                    <select class="form-control" name="state">
                                        @if($Product->state)
                                        <option value="1" selected>啟用</option>
                                        <option value="0">關閉</option>
                                        @else
                                        <option value="1">啟用</option>
                                        <option value="0" selected>關閉</option>
                                        @endif
                                    </select>
                                </div>
                                
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-7">
                                    <label>商品描述</label>
                                    <textarea name="content" class="form-control editor">{!! $Product->content !!}</textarea>
                                </div>
                            </div>
                            @include('errors.errors')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $('.lfm').filemanager('image');
    });

    $('#updateButton').on("click",function(){
        tinyMCE.triggerSave();
        var ajaxRequest = new ajaxUpdate('POST','{{ route('product.update') }}',$('#form').serialize(),'{{ route('product.lists') }}');
        ajaxRequest.request();
    })

    //監聽category
    $('#category').on("change",function(){
        var id = event.target.value;
        $.ajax({
            type: 'POST',
            url: '{{ route('homeproduct.fliter') }}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data: {
                id: id,
            },
            success: function(data){
                $('#brand').empty();
                $('#brand').append('<option value="" style="display:none">請選擇品牌</option>');  
                data.forEach(function(item){
                    $('#brand').append('<option value="'+item.id+'">'+item.name+'</option>');    
                });                    
            }
        })
    });
</script>
@endsection