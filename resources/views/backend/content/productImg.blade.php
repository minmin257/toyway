@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        商品圖片
                    </h1>
                </div>
                @include('success.success')
                <div class="row">
                    <div class="col-xs-12">
                        <a href="{{ route('product.lists') }}" class="btn btn-default">返回</a>
                        <a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
                        <input type="button" class="btn btn-primary" id="updateButton" value="儲存變更">
                        <div class="space"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="table-responsive text-nowrap">
                            <table class="table" style="table-layout: fixed;">
                                <thead>
                                    <tr>
                                        <th>刪除</th>
                                        <th>圖片</th>
                                        <th>順序</th>
                                        <th>啟用狀態</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <form id="form">
                                        @if(count($Imgs))
                                            <input type="hidden" name="product_id" value="{{ $Product->id }}">
                                            @foreach($Imgs as $key => $Img)
                                                <input type="hidden" name="id[]" value="{{ $Img->id }}">
                                                <input type="hidden" id="thumbnail{{$key}}" name="filepath[]" value="{{ $Img->img }}">
                                                <tr>
                                                    <td data-title="刪除">
                                                        <button class="btn btn-danger btn-outline btn-circle" id="deleteButton_{{ $Img->id }}" name="deleteButtons" type="button">
                                                            <i class="far fa-trash-alt"></i>
                                                        </button>
                                                    </td>
                                                    <td data-title="圖片">
                                                        <div class="img-responsive img-shadow lfm product-img" style="overflow: hidden;" data-input="thumbnail{{$key}}" data-preview="holder{{$key}}" id="holder{{$key}}"><img src="{{ $Img->img }}"></div>
                                                    </td>
                                                    <td data-title="順序">
                                                        <input type="number" class="form-control" name="sort[]" pattern="[0-9]" min="0" value="{{ $Img->sort }}">
                                                    </td>
                                                    <td data-title="啟用狀態">
                                                        <select class="form-control" name="state[]">
                                                            @if($Img->state)
                                                            <option value="1" selected>啟用</option>
                                                            <option value="0">關閉</option>
                                                            @else
                                                            <option value="1">啟用</option>
                                                            <option value="0" selected>關閉</option>
                                                            @endif
                                                        </select>
                                                    </td>
                                                </tr>	
                                            @endforeach
                                        @endif    
                                    </form>
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>	


<!-- 新增區塊 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">新增圖片</h4>
            </div>
            <div class="modal-body">
                <form id="form_create">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="hidden" name="product_id" value="{{ $Product->id }}">
                                <div class="form-group">
                                    <label>圖片</label>
                                    <button class="form-control btn btn-primary lfm" data-input="thumbnail" data-preview="holder">
                                        <i class="far fa-image"></i>上傳圖片
                                    </button>
                                    <input id="thumbnail" class="form-control" type="hidden" name="filepath">
                                </div>
                                <div class="form-group">
                                    <div class="create-product-img" data-input="thumbnail" data-preview="holder" id="holder"></div>
                                </div>
                                <div class="form-group">
                                    <label>順序</label>
                                    <input type="number" class="form-control" name="sort" pattern="[0-9]" value="0">
                                </div>
                                <div class="form-group">
                                    <label>啟用狀態</label>
                                    <select class="form-control" name="state">
                                        <option value="1" selected>啟用</option>
                                        <option value="0">關閉</option>
                                    </select>
                                </div>
                                @include('errors.errors')
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->
                </form>
            </div>
            <div class="modal-footer">
                <input type="button" id="createButton" class="btn btn-primary" value="確定送出">
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $('.lfm').filemanager('image');
    });
    
    $('#updateButton').on("click",function(){
        var ajaxRequest = new ajaxUpdate('POST','{{ route('product.img.update') }}',$('#form').serialize());
		ajaxRequest.request();
    })

    $('#createButton').on("click",function(){
        var ajaxRequest = new ajaxUpdate('POST','{{ route('product.img.create') }}',$('#form_create').serialize());
		ajaxRequest.request();
    })

    var deleteButtons = $('button[name=deleteButtons]')
    $.each(deleteButtons, function(i, el) {
        $(el).on("click",function(){
            var id = this.id.split('_');
            var ajaxRequest = new ajaxDelete('POST','{{ route('product.img.delete') }}',id[1]);
            ajaxRequest.request();
        })
    });
</script>
@endsection