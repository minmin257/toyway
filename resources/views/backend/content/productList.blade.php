@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        商品列表
                    </h1>
                </div>
                @include('success.success')
                <div class="row">
                    <div class="col-xs-6">
                        @if($check)
                            <a href="{{ route('product.brands',['id'=>$Products->first()->brand->category->id]) }}" class="btn btn-info">返回品牌列表</a>
                            <a href="{{ route('product.lists') }}" class="btn btn-warning">全部商品</a>
                        @endif
                        <a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
                        <input type="button" id="updateButton" class="btn btn-primary" value="儲存變更">
                    </div>
                    @if(!$check)
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-xs-6">
                            <select class="form-control" id="index_category" name="category">
                                <option value="" style="display: none">請選擇主分類</option>
                                @foreach ($ProductCategories as $Category)
                                    <option value="{{ $Category->id }}" name="category_item" id="category{{ $Category->id }}">{{ $Category->name }}</option>
                                @endforeach
                            </select>
                            </div>
                            <div class="col-xs-6">
                            <select class="form-control" id="index_brand" name="brand">
                                <option value="" style="display: none">請選擇品牌</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="space"></div>
                    <div class="col-xs-12">
                        <div class="table-responsive text-nowrap">
                            <div id="hole">
                                @include('backend.content.productListHole')
                            </div>
                            {{-- <table class="table">
                                <thead>
                                <tr>
                                    <th>刪除</th>
                                    <th>編輯</th>
                                    <th>商品分類</th>
                                    <th>商品品牌</th>
                                    <th>商品編號</th>
                                    <th>商品名稱</th>
                                    <th>順序</th>
                                    <th>啟用狀態</th>
                                </tr>
                                </thead>

                                <tbody>
                                    <form id="form">
                                    @include('backend.content.productListHole')
                                    </form> --}}
                                    {{-- 錯誤警示位置 --}}
                                    {{-- @include('errors.errors')
                                </tbody>
                            </table> --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- 新增區塊 -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 1100px">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">新增商品</h4>
                </div>
                <div class="modal-body">
                    <form id="form_create">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    @if(!$check)
                                    <div class="form-group">
                                        <label>分類名稱</label>
                                        <select class="form-control" id="category" name="category">
                                            <option value="" style="display: none">請選擇主分類</option>
                                            @foreach ($ProductCategories as $Category)
                                                <option value="{{ $Category->id }}" name="category_item" id="category{{ $Category->id }}">{{ $Category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>品牌名稱</label>
                                        <select class="form-control" id="brand" name="brand_id">
                                            <option value="" style="display: none">請選擇品牌</option>
                                        </select>
                                    </div>
                                    @else
                                        <div class="form-group">
                                            <label>分類名稱</label>
                                            <input type="text" class="form-control" value="{{ $Products->first()->brand->category->name }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>品牌名稱</label>
                                            <input type="text" class="form-control" name="brand_name" value="{{ $Products->first()->brand->name }}" readonly>
                                            <input type="hidden" class="form-control" name="brand_id" value="{{ $Products->first()->brand->id }}">
                                        </div>
                                    @endif
                                    
                                    <div class="form-group">
                                        <label>名稱</label>
                                        <input type="text" class="form-control" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label>商品編號</label>
                                        <input type="text" class="form-control" name="no">
                                    </div>
                                    <div class="form-group">
                                        <label>商品描述</label>
                                        <textarea name="content" class="form-control editor"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-6"> 
                                    <div class="form-group">
                                        <label>列表圖片</label>
                                        <button class="form-control btn btn-primary lfm" data-input="thumbnail" data-preview="holder">
                                            <i class="far fa-image"></i>上傳圖片
                                        </button>
                                        <input id="thumbnail" class="form-control" type="hidden" name="filepath">
                                    </div>
                                    <div class="form-group">
                                        <div class="create-product-img" data-input="thumbnail" data-preview="holder" id="holder"></div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>順序</label>
                                        <input type="number" class="form-control" name="sort" pattern="[0-9]" min="0" value="0">
                                    </div>
                                    <div class="form-group">
                                        <label>顯示於首頁</label>
                                        <select class="form-control" name="homepage">
                                            <option value="1">顯示</option>
                                            <option value="0" selected>不顯示</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>啟用狀態</label>
                                        <select class="form-control" name="state">
                                            <option value="1" selected>啟用</option>
                                            <option value="0">關閉</option>
                                        </select>
                                    </div>
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" value="確定送出" id="createButton">
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
<script>
    $(function() {
        $('.lfm').filemanager('image');
    });

    //監聽category
    $('#index_category').on("change",function(){
        var id = event.target.value;
        $.ajax({
            type: 'POST',
            url: '{{ route('homeproduct.fliter') }}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data: {
                id: id,
            },
            success: function(data){
                $('#index_brand').empty();
                $('#index_brand').append('<option value="" style="display:none">請選擇品牌</option>');  
                data.forEach(function(item){
                    $('#index_brand').append('<option value="'+item.id+'">'+item.name+'</option>');    
                });                    
            }
        })
    });

    //監聽index_brand
    $('#index_brand').on("change",function(){
        var id = event.target.value;
        $.ajax({
            type: 'POST',
            url: '{{ route('productList.fliter.product') }}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data: {
                id: id,
            },
            success: function(data){
                console.log(data)
                $('#hole').html(data);       
            }
        })   
    });

    //監聽category
    $('#category').on("change",function(){
        var id = event.target.value;
        $.ajax({
            type: 'POST',
            url: '{{ route('homeproduct.fliter') }}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data: {
                id: id,
            },
            success: function(data){
                $('#brand').empty();
                $('#brand').append('<option value="" style="display:none">請選擇品牌</option>');  
                data.forEach(function(item){
                    $('#brand').append('<option value="'+item.id+'">'+item.name+'</option>');    
                });                    
            }
        })
    });

    function product_delete(id){
        var ajaxRequest = new ajaxDelete('POST','{{ route('product.delete') }}',id);
		ajaxRequest.request();
    }
    

    $('#createButton').on("click",function(){
        tinyMCE.triggerSave();
        var ajaxRequest = new ajaxCreate('POST', '{{ route('product.create') }}', $('#form_create').serialize());
        ajaxRequest.request();
    })
</script>
@endsection
{{-- var deleteButtons = $('button[name=deleteButton]');
    $.each(deleteButtons, function(i,el){
        $(el).on("click",function(){
            var id = this.id.split('_');
            var ajaxRequest = new ajaxDelete('POST', '{{ route('product.delete') }}', id[1]);
            ajaxRequest.request();
        })
    }) --}}