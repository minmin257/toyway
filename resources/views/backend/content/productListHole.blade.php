<table class="table">
    <thead>
    <tr>
        <th>刪除</th>
        <th>編輯</th>
        <th>圖片</th>
        <th>商品分類</th>
        <th>商品品牌</th>
        <th>商品編號</th>
        <th>商品名稱</th>
        <th>順序</th>
        <th>啟用狀態</th>
    </tr>
    </thead>

    <tbody>
        <form id="form">
            @foreach($Products as $Product)
            <input type="hidden" name="id[]" value="{{ $Product->id }}">
            <tr>
                <td data-title="刪除">
                    <button class="btn btn-danger btn-outline btn-circle" type="button" id="deleteButton_{{ $Product->id }}" name="deleteButton" onclick="product_delete({{ $Product->id }})">
                        <i class="far fa-trash-alt"></i>
                    </button>
                </td>
                <td data-title="編輯">
                    <a href="{{ route('product.edit',['id'=>$Product->id]) }}" class="btn btn-info btn-outline btn-circle">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                </td>
                <td data-title="圖片">
                    <a href="{{ route('product.img',['id'=>$Product->id]) }}" class="btn btn-warning btn-outline btn-circle">
                        <i class="fas fa-bars"></i>
                    </a>
                </td>
                <td data-title="商品分類">
                    {{ $Product->brand->category->name }}
                </td>
                <td data-title="商品品牌">
                    {{ $Product->brand->name }}
                </td>
                <td data-title="商品編號">
                    {{ $Product->no }}
                </td>
                <td data-title="商品名稱">
                    {{ $Product->name }}
                </td>
                <td data-title="順序">
                    {{ $Product->sort }}
                </td>
                <td data-title="啟用狀態">
                    @if ($Product->state == 1)
                        啟用
                    @else
                        關閉    
                    @endif
                </td>
            </tr>
        @endforeach
        </form>
        {{-- 錯誤警示位置 --}}
        @include('errors.errors')
    </tbody>
</table>
