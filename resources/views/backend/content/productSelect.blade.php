@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        篩選管理
                    </h1>
                </div>
                @include('success.success')
                <div class="row">
                    <div class="col-xs-12">
                        <a href="#" class="btn btn-default" role="button" data-toggle="modal"
                           data-target="#myModal">新增</a>
                        <input type="button" class="btn btn-primary" id="updateButton" value="儲存變更">
                        <div class="space"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="table-responsive text-nowrap">
                            <table class="table" style="table-layout: fixed;">
                                <thead>
                                    <tr style="display: flex;">
                                        <th style="flex-basis: 5%">商品列表</th>
                                        <th style="flex-basis: 5%">刪除</th>
                                        <th style="flex-basis: 30%">名稱</th>
                                        <th style="flex-basis: 30%">順序</th>
                                        <th style="flex-basis: 30%">啟用狀態</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <form id="form">
                                        @foreach($ProductSelects as $key => $ProductSelect)
                                            <input type="hidden" name="id[]" value="{{ $ProductSelect->id }}">
                                            <tr style="display: flex;">
                                                <td data-title="商品列表" style="flex-basis: 5%">
                                                    <a href="{{ route('product.selects.lists',['id'=>$ProductSelect->id]) }}" class="btn btn-warning btn-outline btn-circle">
                                                        <i class="fas fa-bars"></i>
                                                    </a>
                                                </td>
                                                <td data-title="刪除" style="flex-basis: 5%">
                                                    <button class="btn btn-danger btn-outline btn-circle" type="button" id="deleteButton_{{ $ProductSelect->id }}" name="deleteButton">
                                                        <i class="far fa-trash-alt"></i>
                                                    </button>
                                                </td>
                                                <td data-title="名稱" style="flex-basis: 30%">
                                                    <input type="text" class="form-control" name=name[] value="{{ $ProductSelect->name }}">	
                                                </td>
                                                <td data-title="順序" style="flex-basis: 30%">
                                                    <input type="number" class="form-control" name="sort[]" pattern="[0-9]"  min="0" value="{{ $ProductSelect->sort }}">
                                                </td>
                                                <td data-title="啟用狀態" style="flex-basis: 30%">
                                                    <select class="form-control" name="state[]">
                                                        @if($ProductSelect->state)
                                                        <option value="1" selected>啟用</option>
                                                        <option value="0">關閉</option>
                                                        @else
                                                        <option value="1">啟用</option>
                                                        <option value="0" selected>關閉</option>
                                                        @endif
                                                    </select>
                                                </td>
                                            </tr>	
                                        @endforeach
                                    </form>
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>	

<!-- 新增區塊 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">新增篩選</h4>
            </div>
            <div class="modal-body">
                <form id="form_create">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>名稱</label>
                                    <input type="text" class="form-control" name="name">
                                </div>
                                <div class="form-group">
                                    <label>順序</label>
                                    <input type="number" class="form-control" name="sort" pattern="[0-9]" min="0" value="0">
                                </div>
                                <div class="form-group">
                                    <label>啟用狀態</label>
                                    <select class="form-control" name="state">
                                        <option value="1" selected>啟用</option>
                                        <option value="0">關閉</option>
                                    </select>
                                </div>
                                {{-- 錯誤警示位置 --}}
                                @include('errors.errors')
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->
                </form>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-primary" value="確定送出" id="createButton">
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $('#updateButton').on("click",function(){
        var ajaxRequest = new ajaxUpdate('POST','{{ route('product.selects.update') }}',$('#form').serialize());
		ajaxRequest.request();
    })

    $('#createButton').on("click",function(){
        tinyMCE.triggerSave();
        var ajaxRequest = new ajaxCreate('POST', '{{ route('product.selects.create') }}', $('#form_create').serialize());
        ajaxRequest.request();
    })

    var deleteButtons = $('button[name=deleteButton]');
    $.each(deleteButtons, function(i,el){
        $(el).on("click",function(){
            var id = this.id.split('_');
            var ajaxRequest = new ajaxDelete('POST', '{{ route('product.selects.delete') }}', id[1]);
            ajaxRequest.request();
        })
    })
</script>
@endsection