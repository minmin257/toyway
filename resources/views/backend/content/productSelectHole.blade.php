<div class="table-responsive text-nowrap">
    <table class="table" style="table-layout: fixed;">
        <thead>
            <tr style="background-color:#e9e9e9">
                <th></th>
                <th>產品主分類</th>
                <th>產品品牌</th>
                <th>產品名稱</th>
            </tr>
        </thead>
        <tbody id="product">                     
            <form id="form">
                @foreach($Products as $key => $Product)
                    <tr>
                        <td data-title="">
                            <input type="checkbox" id="product{{ $Product->id }}" name="id[]" value="{{ $Product->id }}">
                        </td>
                        <td data-title="產品主分類">
                            {{ $Product->brand->category->name }}
                        </td>
                        <td data-title="產品品牌">
                            {{ $Product->brand->name }}
                        </td>
                        <td data-title="產品名稱">
                            {{ $Product->name }}
                        </td>
                    </tr>	
                @endforeach
            </form>
            {{-- 錯誤警示位置 --}}
            @include('errors.errors')
        </tbody>
    </table>
</div>    