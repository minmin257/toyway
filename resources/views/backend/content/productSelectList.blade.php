@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        篩選管理
                    </h1>
                </div>
                @include('success.success')
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">  
                            <div class="col-xs-6">
                                <div class="row">
                                    <div class="col-xs-6">
                                    <select class="form-control" id="category" name="category">
                                        <option value="" style="display: none">請選擇主分類</option>
                                        @foreach ($ProductCategories as $Category)
                                            <option value="{{ $Category->id }}" name="category_item" id="category{{ $Category->id }}">{{ $Category->name }}</option>
                                        @endforeach
                                    </select>
                                    </div>
                                    <div class="col-xs-6">
                                    <select class="form-control" id="brand" name="brand">
                                        <option value="" style="display: none">請選擇品牌</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
							<input type="button" id="updateButton" class="btn btn-primary" value="儲存變更">
                        </div>
                        <div class="space"></div>       
                    </div>
                    <div class="col-xs-12">
                        <label>已勾選列表</label>
                        <div class="table-responsive text-nowrap">
                            <table class="table" style="table-layout: fixed;">
                                <thead>
                                    <tr style="background-color:#e9e9e9">
                                        <th>刪除</th>
                                        <th>產品主分類</th>
                                        <th>產品品牌</th>
                                        <th>產品名稱</th>
                                    </tr>
                                </thead>

                                <tbody id="has_product">
                                    <input type="hidden" id="select_id" value="{{ $select_id }}">
                                    @foreach($HasProducts as $key => $Product)
                                        <tr>
                                            <td data-title="">
                                                <button class="btn btn-danger btn-outline btn-circle" id="deleteButton_{{ $Product->id }}" name="deleteButton" type="button">
                                                    <i class="far fa-trash-alt"></i>
                                                </button>
                                            </td>
                                            <td data-title="產品主分類">
                                                {{ $Product->brand->category->name }}
                                            </td>
                                            <td data-title="產品品牌">
                                                {{ $Product->brand->name }}
                                            </td>
                                            <td data-title="產品名稱">
                                                {{ $Product->name }}
                                            </td>
                                        </tr>	
                                    @endforeach
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </tbody>
                            </table>
                        </div>
                        <label>未勾選列表 <span style="color:red;">( 僅顯示產品狀態為"啟用"且未被勾選項目 )<span></label>
                        <div id="hole">
                            @include('backend.content.productSelectHole')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>	

@endsection

@section('js')
<script>
    //監聽category
    $('#category').on("change",function(){
        var id = event.target.value;
        console.log(select_id)
        $.ajax({
            type: 'POST',
            url: '{{ route('homeproduct.fliter') }}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data: {
                id: id,
            },
            success: function(data){
                $('#brand').empty();
                $('#brand').append('<option value="" style="display:none">請選擇品牌</option>');  
                data.forEach(function(item){
                    $('#brand').append('<option value="'+item.id+'">'+item.name+'</option>');    
                });                    
            }
        })
    });

    //監聽brand
    $('#brand').on("change",function(){
        var id = event.target.value;
        var select_id = $('#select_id').val();
        $.ajax({
            type: 'POST',
            url: '{{ route('select.fliter.product') }}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data: {
                id: id,
                select_id: select_id
            },
            success: function(data){
                $('#hole').html(data);       
            }
        })   
    });

    $('#updateButton').on("click",function(){
        var id = [];
        var count = 0;
        var products = $('input[name^=id]');
        var select_id = $('#select_id').val();
        $.each(products, function(i, el){
            if(el.checked){
                id[count] = el.value;
                count ++;
            }
        });

        $.ajax({
            type: 'POST',
            url: '{{ route('product.selects.lists.update') }}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data: {
                id: id,
                select_id: select_id
            },
            error: function(data) {
                errorsHtml = '';
                $.each(error.errors, function(key,value){
                        errorsHtml += '<li>'+ value +'</li>';
                });
            
                Swal.fire({
                    html: '<span class="f-family" style="color: #707070;font-size: 14px;font-weight:600;">'+errorsHtml+'</span>',
                    icon: 'error',
                    showConfirmButton: false,
                    width: '300px',
                    heightAuto: false,
                })
            },
            success: function(data) {
                $('#error_message').html('');
                    swal.fire({
                        title: '修改成功!',
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 1000,
                    }).then(function(){
                        location.reload();
                })
            }
        })
    })
    
    var deleteButtons = $('button[name=deleteButton]');
    $.each(deleteButtons, function(i,el){
        $(el).on("click", function(){
            var id = this.id.split('_');
            var select_id = $('#select_id').val();
            var array = [id[1],select_id]
            var ajaxRequest = new ajaxDeleteArray('POST', '{{ route('product.selects.lists.delete') }}', array);
            ajaxRequest.request();
        })
    })
</script>
@endsection
{{-- $('#updateButton').on("click",function(){
        var id = [];
        var count = 0;
        var products = $('input[name^=id]');
        $.each(products, function(i, el){
            if(el.checked){
                id[count] = el.value;
                count ++;
            }
        });

        $.ajax({
            type: 'POST',
            url: '{{ route('homeproduct.update') }}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data: {
                id: id,
            },
            error: function(data) {
                errorsHtml = '';
                $.each(error.errors, function(key,value){
                        errorsHtml += '<li>'+ value +'</li>';
                });
            
                Swal.fire({
                    html: '<span class="f-family" style="color: #707070;font-size: 14px;font-weight:600;">'+errorsHtml+'</span>',
                    icon: 'error',
                    showConfirmButton: false,
                    width: '300px',
                    heightAuto: false,
                })
            },
            success: function(data) {
                $('#error_message').html('');
                    swal.fire({
                        title: '修改成功!',
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 1000,
                    }).then(function(){
                        location.reload();
                })
            }
        })
    })

    var deleteButtons = $('button[name=deleteButton]');
    $.each(deleteButtons, function(i,el){
        $(el).on("click", function(){
            var id = this.id.split('_');
            var ajaxRequest = new ajaxDelete('POST', '{{ route('homeproduct.delete') }}', id[1]);
            ajaxRequest.request();
        })
    }) --}}