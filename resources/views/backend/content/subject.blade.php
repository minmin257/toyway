@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        頁面管理
                    </h1>
                </div>
                @include('success.success')
                <div class="row">
                    <div class="col-xs-12">
                        <input type="button" class="btn btn-primary" id="updateButton" value="儲存變更">
                        <div class="space"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="table-responsive text-nowrap">
                            <table class="table" style="table-layout: fixed;">
                                <thead>
                                    <tr>
                                        <th>標題</th>
                                        <th>連結<span style="color: red;"> (不對外連結請留空)</span></th>
                                        <th>順序</th>
                                        <th>啟用狀態</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <form id="form">
                                        @foreach($Subjects as $key => $Subject)
                                            <input type="hidden" name="id[]" value="{{ $Subject->id }}">
                                            <tr>
                                                <td data-title="標題">
                                                    <input type="text" class="form-control" name=name[] value="{{ $Subject->name }}">	
                                                </td>
                                                <td data-title="連結">
                                                    <input type="text" class="form-control" name=link[] value="{{ $Subject->link }}">	
                                                </td>
                                                <td data-title="順序">
                                                    <input type="number" class="form-control" name="sort[]" pattern="[0-9]" value="{{ $Subject->sort }}">
                                                </td>
                                                <td data-title="啟用狀態">
                                                    <select class="form-control" name="state[]">
                                                        @if($Subject->state)
                                                        <option value="1" selected>啟用</option>
                                                        <option value="0">關閉</option>
                                                        @else
                                                        <option value="1">啟用</option>
                                                        <option value="0" selected>關閉</option>
                                                        @endif
                                                    </select>
                                                </td>
                                            </tr>	
                                        @endforeach
                                    </form>
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>	
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $('.lfm').filemanager('image');
    });
    
    $('#updateButton').on("click",function(){
        var ajaxRequest = new ajaxUpdate('POST','{{ route('subject.update') }}',$('#form').serialize());
		ajaxRequest.request();
    })
</script>
@endsection