@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        影片列表
                    </h1>
                </div>
                @include('success.success')
                <div class="row">
                    <div class="col-xs-12">
                        <a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
                        <div class="space"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="table-responsive text-nowrap">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>刪除</th>
                                    <th>編輯</th>
                                    <th>youtube ID</th>
                                    <th>影片標題</th>
                                    <th>日期</th>
                                    <th>順序</th>
                                    <th>啟用狀態</th>
                                </tr>
                                </thead>

                                <tbody>
                                    <form id="form">
                                    @foreach($Videos as $Video)
                                        <input type="hidden" name="id[]" value="{{ $Video->id }}">
                                        <tr>
                                            <td data-title="刪除">
                                                <button class="btn btn-danger btn-outline btn-circle" type="button" id="deleteButton_{{ $Video->id }}" name="deleteButton">
                                                    <i class="far fa-trash-alt"></i>
                                                </button>
                                            </td>
                                            <td data-title="編輯">
                                                <a href="{{ route('videos.edit',['id'=>$Video->id]) }}" class="btn btn-info btn-outline btn-circle">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                            </td>
                                            <td data-title="日期">
                                                {{ $Video->date }}
                                            </td>
                                            <td data-title="youtube ID">
                                                {{ $Video->youtube_id }}
                                            </td>
                                            <td data-title="影片標題">
                                                {{ $Video->title }}
                                            </td>
                                            <td data-title="順序">
                                                {{ $Video->sort }}
                                            </td>
                                            <td data-title="啟用狀態">
                                                @if ($Video->state == 1)
                                                    啟用
                                                @else
                                                    關閉    
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </form>
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- 新增區塊 -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">新增影片</h4>
                </div>
                <div class="modal-body">
                    <form id="form_create">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>日期</label>
                                        <input type="date" class="form-control" name="date">
                                    </div>
                                    <div class="form-group">
                                        <label>影標題片</label>
                                        <input type="text" class="form-control" name="title">
                                    </div>
                                    <div class="form-group">
                                        <label>youtube ID</label>
                                        <input type="text" class="form-control" name="youtube_id">
                                    </div>
                                    <div class="form-group">
                                        <label>影片描述</label>
                                        <textarea name="content" class="form-control editor"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>順序</label>
                                        <input type="number" class="form-control" name="sort" pattern="[0-9]" min="0" value="0">
                                    </div>
                                    <div class="form-group">
                                        <label>啟用狀態</label>
                                        <select class="form-control" name="state">
                                            <option value="1" selected>啟用</option>
                                            <option value="0">關閉</option>
                                        </select>
                                    </div>
                                    {{-- 錯誤警示位置 --}}
                                    @include('errors.errors')
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" value="確定送出" id="createButton">
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
<script>
    $(function() {
        $('.lfm').filemanager('image');
    });

    var deleteButtons = $('button[name=deleteButton]');
    $.each(deleteButtons, function(i,el){
        $(el).on("click",function(){
            var id = this.id.split('_');
            var ajaxRequest = new ajaxDelete('POST', '{{ route('videos.delete') }}', id[1]);
            ajaxRequest.request();
        })
    })

    $('#createButton').on("click",function(){
        tinyMCE.triggerSave();
        var ajaxRequest = new ajaxCreate('POST', '{{ route('videos.create') }}', $('#form_create').serialize());
        ajaxRequest.request();
    })
</script>
@endsection