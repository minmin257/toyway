@extends('backend.content.default')
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        影片列表－編輯
                    </h1>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <a href="{{ route('videos') }}" class="btn btn-default">返回</a>
                        <input type="button" class="btn btn-primary" value="儲存變更" id="updateButton">
                        <a href="/images/youtube.jpg" class="btn btn-danger" target="_blank">點此觀看取得ID教學</a>
                        <div class="space"></div>

                        <form id="form">
                            <input type="hidden" name="id" value="{{ $Video->id }}">
                            <div class="form-group row">
                                <div class="col-xs-7">
                                    <label>日期</label>
                                    <input type="date" class="form-control" name="date" value="{{ $Video->date }}">

                                    <label>影片標題</label>
                                    <input type="text" class="form-control" name="title" value="{{ $Video->title }}">

                                    <label>youtube ID</label>
                                    <input type="text" class="form-control" name="youtube_id" value="{{ $Video->youtube_id }}">
                                    
                                    <label>Share&Comment URL</label>
                                    <input type="text" class="form-control" name="share_comment_url" value="{{ $Video->share_comment_url }}">
                                    
                                    <label>順序</label>
                                    <input type="number" class="form-control" name="sort" pattern="[0-9]" min="0" value="{{ $Video->sort }}">
                                
                                    <label>啟用狀態</label>
                                    <select class="form-control" name="state">
                                        @if($Video->state)
                                        <option value="1" selected>啟用</option>
                                        <option value="0">關閉</option>
                                        @else
                                        <option value="1">啟用</option>
                                        <option value="0" selected>關閉</option>
                                        @endif
                                    </select>

                                    <label>影片描述</label>
                                    <textarea name="content" class="form-control editor">{!! $Video->content !!}</textarea>
                                </div>
                            </div>
                            @include('errors.errors')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $('#updateButton').on("click",function(){
        tinyMCE.triggerSave();
        var ajaxRequest = new ajaxUpdate('POST','{{ route('videos.update') }}',$('#form').serialize(),'{{ route('videos') }}');
        ajaxRequest.request();
    })
</script>
@endsection