@inject('System', 'App\Repositories\SystemRepository')
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
		<meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ $System->read()->sitename }}</title>
		<link rel="shortcut icon" href="/images/logo-icon.png" />
		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{{ asset('css/backend/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/backend/bootstrap_r.css') }}">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
        <script src="https://cdn.tiny.cloud/1/2lj659gf36k2ckrjxa64v2aftl982mxpwxrltkel26m7do0o/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
		<link rel="stylesheet" href="{{ asset('css/backend/backend.css') }}">
		<style>
			.swal2-cancel.btn{
				margin-right:10px;
			}
			.img-responsive.img-shadow img{
				object-fit: cover;
				width: 100%;
			}
			.create-img img{
				object-fit: cover;
				width: 100%;
				height: 15rem !important;
			}
			.create-news-img img{
				object-fit: cover;
				width: 100%;
				height: 100% !important;
			}
			.update-news-img img{
				object-fit: cover;
				width: 100%;
    			height: 370px !important;
			}
			.banner-img img{
				object-fit: cover;
				width: 100%;
				height: 10rem !important;
			}
			.product-img img{
				object-fit: cover;
				width: 100%;
				height: 20rem !important;
			}
			.create-product-img img{
				object-fit: cover;
				width: 75%;
				height: 75% !important;
			}
			.update-product-img img{
				object-fit: cover;
				width: 100%;
    			height: 390px !important;
			}
		</style>
		{!! NoCaptcha::renderJs() !!}
	</head>

	<body>
		<!-- 上方登入狀態欄 -->
		@yield('navbar')

		<div class="main-container" id="main-container">
			<!-- 側邊功能欄 -->
			@yield('sidebar')

			<!-- 內容頁 -->
			@yield('content')

			<!-- 頁尾 -->
			@yield('footer')
		</div>
	</body>
	<script src="https://cdn.tiny.cloud/1/2lj659gf36k2ckrjxa64v2aftl982mxpwxrltkel26m7do0o/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
	<script type="text/javascript" src="/js/backend/tinymce.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
	<script type="text/javascript" src="{{ asset('js/backend/back.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/backend/bootstrap.min.js') }}"></script>
	<script src="/js/ajax.js"></script>
	<script type="text/javascript" charset="utf-8">
      $(document).on('focusin', function(e) {
        if ($(e.target).closest(".tox-tinymce-aux, .moxman-window, .tam-assetmanager-root").length) {
          e.stopImmediatePropagation();
        }
      });
    </script>
	@yield('js')
</html>
