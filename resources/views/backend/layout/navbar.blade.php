<div id="navbar" class="navbar navbar-default">
	<div class="navbar-container" id="navbar-container">
		<button type="button" class="navbar-toggle menu-toggler pull-right" id="menu-toggler" data-target="#sidebar">
			<span class="sr-only">選單切換</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>

		<div class="navbar-header pull-left">
			<a href="{{ route('backend.index') }}"><img src="/images/logo.png" alt=""></a>
		</div>

		<div class="navbar-buttons navbar-header pull-right" role="navigation">
			<ul class="nav jhong-nav" style="">

				<li class="light-blue dropdown-modal">
					<a href="#" data-toggle="dropdown" class="dropdown-toggle">
						{{ auth()->user()->name }}
						<i class="fa fa-caret-down"></i>
					</a>
					<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow">
						<li>
							<a href="{{ route('system') }}">
								<i class="nav-icon fa fa-cog"></i>設定
							</a>
						</li>

						<li>
							<a href="{{ route('profile') }}">
								<i class="nav-icon fa fa-user"></i>基本資料
							</a>
						</li>

						<li class="divider"></li>

						<li>
							<a href="{{ route('logout') }}">
								<i class="nav-icon fa fa-power-off"></i>登出
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>

