<div id="sidebar" class="sidebar responsive" data-sidebar="true" data-sidebar-scroll="true" data-sidebar-hover="true">
	<ul class="nav nav-list">
		<li class="{{ active('backend.index') }}">
			<a href="{{ route('backend.index') }}">
				<i class="menu-icon fas fa-tachometer-alt"></i>
				<span class="menu-text">首頁</span>
			</a>
			<b class="arrow"></b>
		</li>
		@if(auth()->user()->hasPremission('內容管理'))
		<li class="{{ Request::segment(2) === 'content' ? 'open' : '' }}">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon far fa-edit"></i>
				<span class="menu-text">內容管理</span>
				<b class="arrow fas fa-angle-down"></b>
			</a>
			<b class="arrow"></b>

			<ul class="submenu">
				<li class="{{ (Request::segment(3) === 'homebanner' || Request::segment(3) === 'homeproduct') ? 'open' : '' }}">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fas fa-caret-right"></i>
						首頁管理
						<b class="arrow fas fa-angle-down"></b>
					</a>
					<b class="arrow"></b>

					<ul class="submenu nav-hide">
						<li class="{{ active('homebanner') }}">
							<a href="{{ route('homebanner') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								首頁輪播
							</a>
							<b class="arrow"></b>
						</li>
						<li class="{{ active('homeproduct') }}">
							<a href="{{ route('homeproduct') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								商品管理
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
				</li>
				<li class="{{ active('innerbanner') }}">
					<a href="{{ route('innerbanner') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						內頁Banner管理
					</a>
					<b class="arrow"></b>
				</li>
				<!--頁面管理-->
				<li class="{{ Request::segment(3) === 'page' ? 'open' : '' }}">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fas fa-caret-right"></i>
						頁面管理
						<b class="arrow fas fa-angle-down"></b>
					</a>
					<b class="arrow"></b>
					<ul class="submenu nav-hide">
						<li class="{{ Request::segment(4) === 'subject' ? 'active' : '' }}">
							<a href="{{ route('subject') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								選單標題管理
							</a>
							<b class="arrow"></b>
						</li>
						<li class="{{ Request::segment(5) === 'about' ? 'active' : '' }}">
							<a href="{{ route('pages',['name'=>'about']) }}">
								<i class="menu-icon fas fa-caret-right"></i>
								關於我們管理
							</a>
							<b class="arrow"></b>
						</li>
						<li class="{{ Request::segment(5) === 'link' ? 'active' : '' }}">
							<a href="{{ route('pages',['name'=>'link']) }}">
								<i class="menu-icon fas fa-caret-right"></i>
								相關連結管理
							</a>
							<b class="arrow"></b>
						</li>
						<li class="{{ Request::segment(5) === 'other' ? 'active' : '' }}">
							<a href="{{ route('pages',['name'=>'other']) }}">
								<i class="menu-icon fas fa-caret-right"></i>
								預留頁面管理
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
				</li>
				<!--文章管理-->
				<li class="{{ (Request::segment(3) === 'news' || Request::segment(3) === 'newsCategories') ? 'open' : '' }}">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fas fa-caret-right"></i>
						文章管理
						<b class="arrow fas fa-angle-down"></b>
					</a>
					<b class="arrow"></b>
					<ul class="submenu nav-hide">
						<li class="{{ active('news.categories') }}">
							<a href="{{ route('news.categories') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								分類管理
							</a>
							<b class="arrow"></b>
						</li>
						<li class="{{ ((Request::segment(3) === 'news' && Request::segment(4) === 'lists') || (Request::segment(3) === 'news' && Request::segment(4) === 'edit') || (Request::segment(3) === 'news' && Request::segment(4) === 'banner')) ? 'active' : '' }}">
							<a href="{{ route('news.lists') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								文章列表
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
				</li>
				<li class="{{ Request::segment(3) === 'videos' ? 'open' : '' }}">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fas fa-caret-right"></i>
						影片管理
						<b class="arrow fas fa-angle-down"></b>
					</a>
					<b class="arrow"></b>

					<ul class="submenu nav-hide">
						<li class="{{ (Request::segment(3) === 'videos' && Request::segment(4) !== 'type') ? 'active' : '' }}">
							<a href="{{ route('videos') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								影片列表
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
				</li>
				<li class="{{ Request::segment(3) === 'links' ? 'open' : '' }}">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fas fa-caret-right"></i>
						相關連結管理
						<b class="arrow fas fa-angle-down"></b>
					</a>
					<b class="arrow"></b>

					<ul class="submenu nav-hide">
						<li class="{{ (Request::segment(3) === 'links' && Request::segment(4) !== 'lists') ? 'active' : '' }}">
							<a href="{{ route('links') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								連結列表
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
				</li>
				@if(auth()->user()->hasPremission('商品管理'))
				<li class="{{ Request::segment(3) === 'products' ? 'open' : '' }}">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fas fa-caret-right"></i>
						商品管理
						<b class="arrow fas fa-angle-down"></b>
					</a>
					<b class="arrow"></b>

					<ul class="submenu nav-hide">
						<li class="{{ (Request::segment(3) === 'products' && Request::segment(4) == 'categories') || (Request::segment(3) === 'products' && Request::segment(4) == 'brands') ? 'active' : '' }}">
							<a href="{{ route('product.categories') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								分類管理
							</a>
							<b class="arrow"></b>
						</li>
						<li class="{{ (Request::segment(3) === 'products' && Request::segment(4) === 'selects') ? 'active' : '' }}">
							<a href="{{ route('product.selects') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								篩選管理
							</a>
							<b class="arrow"></b>
						</li>
						<li class="{{ (Request::segment(3) === 'products' && Request::segment(4) !== 'categories' && Request::segment(4) !== 'brands'  && Request::segment(4) !== 'selects') ? 'active' : '' }}">
							<a href="{{ route('product.lists') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								商品列表
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
				</li>
				@endif
			</ul>
		</li>
		@endif
		<li class="{{  Request::segment(2) === 'authority' ? 'open' : '' }}">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fas fa-users-cog"></i>
				<span class="menu-text">權限管理</span>
				<b class="arrow fas fa-angle-down"></b>
			</a>
			<b class="arrow"></b>

			<ul class="submenu">
				<li class="{{ active('profile') }}">
					<a href="{{ route('profile') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						基本資料
					</a>
					<b class="arrow"></b>
				</li>
				@if(auth()->user()->hasPremission('權限管理'))
				<li class="{{ (Request::segment(3) === 'management' || Request::segment(3) === 'premission') ? 'active' : '' }}">
					<a href="{{ route('management') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						帳號管理
					</a>
					<b class="arrow"></b>
				</li>
				@endif
			</ul>
		</li>
		@if(auth()->user()->hasPremission('系統設定'))
		<li class="{{ Request::segment(2) === 'system' ? 'open' : '' }}">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fas fa-cogs"></i>
				<span class="menu-text">系統設定</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="{{ active('system') }}">
					<a href="{{ route('system') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						一般設定
					</a>
					<b class="arrow"></b>
				</li>
				<li class="{{ active('footer') }}">
					<a href="{{ route('footer') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						頁尾/連結設定
					</a>
					<b class="arrow"></b>
				</li>
			</ul>
		</li>
		@endif
	</ul>
	
	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i id="sidebar-toggle-icon" class="ace-icon fas fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fas fa-angle-double-right"></i>
	</div>
</div>
