@extends('backend.module.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							{{ auth()->user()->name }} 歡迎登入~
						</h1>
					</div>
					<div class="quickLink">
						<div class="row">
							<div class="col-12 col-xs-6 col-md-3">
								<a href="{{ route('product.lists') }}" class="img-100 btn btn-mainColor1 btn-outline btn-lg mb-10">
									<i class="fas fa-tag"></i> 商品管理
								</a>
							</div>
							<div class="col-12 col-xs-6 col-md-3">
								<a href="{{ route('system') }}" class="img-100 btn btn-mainColor1 btn-outline btn-lg mb-10">
									<i class="fas fa-tag"></i> 網站設定
								</a>
							</div>
						</div>
					</div>
					<div class="highestAuthorityNote">
						<p>
							如有任何疑問，請與所屬專案業務聯絡，謝謝！<br>
							Tel：(02)8712-2020
						</p>
					</div>
				</div>
			</div>
		</div>
	@endsection
