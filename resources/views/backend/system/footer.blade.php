@extends('backend.system.default')
@section('content')
	<div class="main-content">
		<div class="main-content-inner">
			<div class="page-content">
				<div class="page-header">
					<h1>
						頁尾/連結設定
					</h1>
				</div>
				@include('success.success')
				<div class="row">
					<div class="col-xs-12">
						<input type="button" class="btn btn-primary" value="儲存變更" id="updateButton">
						<div class="space"></div>
					</div>

					<div class="col-xs-6">
						<form id="form">
							<div class="form-group">
								<label>Facebook</label>
								<input type="text" name="facebook" class="form-control" value="{{ $Footer->facebook }}" required>
							</div>
                            <div class="form-group">
								<label>Instagram</label>
								<input type="text" name="instagram" class="form-control" value="{{ $Footer->instgram }}" required>
							</div>
                            <div class="form-group">
								<label>YouTube</label>
								<input type="text" name="youtube" class="form-control" value="{{ $Footer->youtube }}" required>
							</div>
							<div class="form-group">
								<label>頁尾內容</label>
                                <textarea name="content" class="form-control editor">{!! $Footer->content !!}</textarea>
							</div>
						</form>
						{{-- 錯誤警示位置 --}}
						@include('errors.errors')
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
<script>
    $('#updateButton').on("click",function(){
        tinyMCE.triggerSave();
        var ajaxRequest = new ajaxUpdate('POST','{{ route('footer.update') }}',$('#form').serialize());
		ajaxRequest.request();
    })
</script>
@endsection
