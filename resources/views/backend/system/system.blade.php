@extends('backend.system.default')
@section('content')
	<div class="main-content">
		<div class="main-content-inner">
			<div class="page-content">
				<div class="page-header">
					<h1>
						一般設定
					</h1>
				</div>
				@include('success.success')
				<div class="row">
					<div class="col-xs-12">
						<input type="button" class="btn btn-primary" value="儲存變更" id="updateButton">
						<div class="space"></div>
					</div>

					<div class="col-xs-6">
						<form id="form">
							{{-- @csrf method="post" action="{{ route('normal.update') }}" --}}
							<div class="form-group">
								<label>網站名稱</label>
								<input type="text" name="sitename" class="form-control" value="{{ $System->sitename }}" required>
							</div>
                            {{-- <div class="form-group">
								<label>網站連結</label>
								<input type="text" name="website" class="form-control" value="{{ $System->website }}" required>
							</div> --}}
							<div class="form-group">
								<label>description</label>
								<input type="text" name="description" class="form-control" value="{{ $System->description }}" required>
							</div>
							<div class="form-group">
								<label>keywords</label>
								<input type="text" name="keyword" class="form-control" value="{{ $System->keyword }}" required>
							</div>
							<div class="form-group">
								<label>GA碼</label>
								<textarea rows="5" name="ga_code" class="form-control" required>{{ $System->GAcode }}</textarea>
							</div>
							@if(auth()->user()->id == 1 || auth()->user()->id == 2)
							<div class="form-group">
								@if($System->member_login)
									<div class="input-wrapper">
										<label>會員登入　　</label>
										<input type="radio" id="open_member_login" name="member_login" value="1" checked>
										<label for="open_member_login">開啟</label>
										<span></span>
										<input type="radio" id="close_member_login" name="member_login" value="0">
										<label for="close_member_login">關閉</label>
									</div>
								@else
									<div class="input-wrapper">
										<label>會員登入　　</label>
										<input type="radio" id="open_member_login" name="member_login" value="1">
										<label for="open_member_login">開啟</label>
										<span></span>
										<input type="radio" id="close_member_login" name="member_login" value="0" checked>
										<label for="close_member_login">關閉</label>
									</div>
								@endif
							</div>
							@endif
							<div class="form-group">
								@if($System->maintain)
									<div class="input-wrapper">
										<label>網站維護　　</label>
										<input type="radio" id="open_maintain" name="maintain" value="1" checked>
										<label for="open_maintain">開啟</label>
										<span></span>
										<input type="radio" id="close_maintain" name="maintain" value="0">
										<label for="close_maintain">關閉</label>
									</div>
								@else
									<div class="input-wrapper">
										<label>網站維護　　</label>
										<input type="radio" id="open_maintain" name="maintain" value="1">
										<label for="open_maintain">開啟</label>
										<span></span>
										<input type="radio" id="close_maintain" name="maintain" value="0" checked>
										<label for="close_maintain">關閉</label>
									</div>
								@endif
							</div>
							<div class="form-group">
								<label>維護訊息</label>
								<textarea rows="5" name="message" class="form-control" required>{{ $System->message }}</textarea>
							</div>
						</form>
						{{-- 錯誤警示位置 --}}
						@include('errors.errors')
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
<script>
    $('#updateButton').on("click",function(){
        tinyMCE.triggerSave();
        var ajaxRequest = new ajaxUpdate('POST','{{ route('system.update') }}',$('#form').serialize());
		ajaxRequest.request();
    })
</script>
@endsection
