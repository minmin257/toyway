@extends('errors.default')

@section('code', '404')
@section('title', __('Page Not Found'))

@section('image')
<div style="background-image: url('/svg/404.svg');" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
</div>
@endsection
@if (Request::segment(1) === 'Products' )
	@section('message', __('Sorry, we don’t have this product.'))
@else
	@section('message', __('Sorry, the page you are looking for could not be found.'))
@endif