@extends('front.homepage.default')
@section('banner')
<div class="banner">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($Banners as $key => $Banner)
                <div class="carousel-item {{ ($key == 0)?'active':null }}">
                    <a href="{{ $Banner->link }}"><img src="{{ str_replace( " ", "%20",$Banner->img) }}" class="d-block w-100"></a>
                </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
@endsection
@section('content')
<div class="content">
    <h3 class="title-block">
        <strong>熱 門 文 章</strong>
    </h3>
    <div class="article-list">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-4">
                <a href="{{ route('front.special',['video'=>$Video->id]) }}" class="article-item video">
                    <div class="type">
                        <span>特別專區</span>
                    </div>
                    <div class="img" style="background-image: url(http://img.youtube.com/vi/{{ $Video->youtube_id }}/0.jpg)">
                    </div>
                    <div class="title">
                        <span>
                            {{ $Video->title }}
                        </span>
                    </div>
                </a>
            </div>
            @foreach($News as $key => $New)
                <div class="col-12 col-sm-6 col-md-4">
                    @if($New->category_id == 1)
                        <a href="{{ route('front.news',['url'=>'news','id'=>$New->id]) }}" class="article-item">
                    @elseif($New->category_id == 2)
                        <a href="{{ route('front.news',['url'=>'newProduct','id'=>$New->id]) }}" class="article-item">
                    @else
                        <a href="{{ route('front.news',['url'=>'reviewProduct','id'=>$New->id]) }}" class="article-item">
                    @endif        
                        <div class="type">
                            <span>{{ $New->category->name }}</span>
                        </div>
                        <div class="img" style="background-image: url({{ str_replace( " ", "%20",$New->img) }})">
                        </div>
                        <div class="title">
                            <span>
                                {!! $New->title !!}
                            </span>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>

    @if(count($Products))
        <h3 class="title-block">
            <strong>商 品 目 錄</strong>
        </h3>
        <div class="product-list">
            <div class="row justify-content-center">
                @foreach($Products as $key => $Product)
                    <div class="col-6 col-md">
                        <a href="{{ route('front.product.inner',['product'=>$Product->id]) }}" class="product-item">
                            <div class="img" style="background-image: url({{ str_replace( " ", "%20",$Product->img) }});"></div>
                            <div class="num sr-only">
                                {{ $Product->no }}
                            </div>
                            <div class="title sr-only">
                                {{ $Product->name }}
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            <div class="list-more tr">
                <a href="{{ route('front.product') }}" title="">
                    更多商品
                </a>
            </div>
        </div>
    @endif
    

</div>
@endsection