@inject('System', 'App\Repositories\SystemRepository')
@inject('SetOgimage', 'App\Services\SetOgImageService')

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- 網站標題icon -->
    {{-- <title>{{ $System->read()->sitename }}</title> --}}
    <link rel="shortcut icon" href="/images/icon-logo.png" />
    <meta name="keywords" content="{{ $System->read()->keyword }}">
    <meta name="description" content="{{ $System->read()->description }}">
    @yield('fb-og')
    
    @php
    // dd($SetOgimage->getCurrentUrl());
      $check = Str::of($SetOgimage->getCurrentUrl());
      if($check == "/" && is_null(request()->segment(1))){
        // dd("product");
        $og_img = 'https://toyway.ypdemo.com.tw/images/link-img.jpg';
        $og_title = $System->read()->sitename;
      }
      else if($check->startsWith("/news")){
        // dd("news ");
        if($check->contains("id=")){
          $og_img = $News->img;
          $og_title = Str::replace('<br>','',$News->title);
        }
        else{
          $og_img = 'https://toyway.ypdemo.com.tw/images/link-img.jpg';
          $og_title = $Subject->name;
        }
      }
      else if($check->startsWith("/product")){
        if(request()->segment(2) == 'inner'){
          $og_img = $Product->img;
          $og_title = $Product->name;
        }
        else{
          $og_img = 'https://toyway.ypdemo.com.tw/images/link-img.jpg';
          $og_title = $Subject->name;
        }
      }
      else if($check->startsWith("/special")){
        if(!is_null(request()->segment(2))){
          $og_img = 'http://img.youtube.com/vi/'.$Video->youtube_id.'/0.jpg';
          $og_title = Str::replace('<br>','',$Video->title);
        }
        else{
          $og_img = 'https://toyway.ypdemo.com.tw/images/link-img.jpg';
          $og_title = $Subject->name;
        }
      }
      else {
        $og_img = 'https://toyway.ypdemo.com.tw/images/link-img.jpg';
        $og_title = $System->read()->sitename;
      }
    @endphp
    <title>{{ $og_title }}</title>
    <meta property="og:url" content="{{ request()->fullUrl() }}">
    <meta property="og:image" content="{{ $og_img }}">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@400;500;700;900&display=swap" rel="stylesheet">
    <link href="/css/bootstrap_r.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/lightbox.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <style>
      .article-page-list .article-page-item>.img {
				background-position: center;
			}
      .shareBTN-group.tr {
        display: flex;
        justify-content: flex-end;
        align-items: center;
      }
      #twitter-widget-0 {
        margin-left:5px;
      }
    </style>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v10.0&appId=859900817922743&autoLogAppEvents=1" nonce="16b2tXMA"></script>
    {!! $System->read()->GAcode !!}
  </head>
  <body>

    <div class="wrap">
      <!-- header================================================== -->
      @yield('header')

      <!-- banner================================================== -->
      @yield('banner')

      <!-- menu======================================================= -->
      @yield('nav')

      <!-- content=============================================== -->
      <content class="mb-80">
        @yield('content')
      </content>

    </div>

    <!-- footer==================================================== -->
    @yield('footer')

    <a href="javascript:;" class="btn-goTop" title="">
      <i class="fas fa-chevron-up"></i>
    </a>

    <!-- Modal -->
    <div class="modal fade" id="searchModal" tabindex="-1" aria-labelledby="searchModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-body">
            <div class="mb-40 d-block">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form id="search_form" action="{{ route('front.search') }}" method="GET">
              <div class="form-group">
                <div class="input-group mb-3">
                  <input type="text" name="keyword" class="form-control" placeholder="請輸入關鍵字" aria-label="請輸入關鍵字" aria-describedby="button-addon-search">
                  <div class="input-group-append">
                    <button class="btn btn-search" type="button" id="button-addon-search" onclick="$('#search_form').submit()"><i class="fas fa-search"></i></button>
                    {{-- <a href="" class="btn btn-search" id="button-addon-search"><i class="fas fa-search"></i></a> --}}
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- JavaScript -->
    <script src="/js/jquery.1.11.1.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/wow.js"></script>
    <script src="/js/lightbox.js"></script>
    <script src="https://kit.fontawesome.com/588be6838c.js"></script>
    <script src="/js/yp.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <!--twitter-->
    <script type="text/javascript" async src="https://platform.twitter.com/widgets.js"></script>
    <!--link-->
    <script src="https://www.line-website.com/social-plugins/js/thirdparty/loader.min.js" async="async" defer="defer"></script>
    <script>
      //copy url
      $('#copylink').on("click", function(){
          var dummy = document.createElement('input'),
          text = window.location.href;
  
          document.body.appendChild(dummy);
          dummy.value = text;
          dummy.select();
          document.execCommand('copy');
          document.body.removeChild(dummy);

          Swal.fire({
            text: '複製成功',
            icon: 'success',
            showConfirmButton: false,
            width: '300px',
            height: '150px',
            timer: 1000,
            heightAuto: false
          })
      })
  </script>
  @yield('js')
  </body>
</html>