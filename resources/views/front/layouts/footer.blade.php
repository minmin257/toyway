@inject('Footer', 'App\Repositories\FooterRepository')
<div class="footer">
    <div class="deco-line"></div>
    <div class="content">
        {!! $Footer->read()->content !!}
    </div>
</div>