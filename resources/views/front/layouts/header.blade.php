@inject('Footer', 'App\Repositories\FooterRepository')
@inject('Subject', 'App\Repositories\SubjectRepository')
@inject('System', 'App\Repositories\SystemRepository')
<div class="header fixed">
    <div class="deco-line"></div>
    <div class="content">
        <nav class="navbar navbar-expand-lg justify-content-start">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>

            <a class="navbar-brand" href="{{ route('front.homepage') }}">
                <img class="w-100" src="/images/logo.png" alt="">
            </a>

            <div class="header-icon-group order-lg-2 ml-auto">
                <ul>
                    <li>
                        <a href="https://www.facebook.com/{{ $Footer->read()->facebook }}" target="_blank">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/{{ $Footer->read()->instgram }}" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/{{ $Footer->read()->youtube }}" target="_blank">
                            <i class="fab fa-youtube"></i>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="modal" data-target="#searchModal">
                            <i class="fas fa-search"></i>
                        </a>
                    </li>
                </ul>
                @if($System->read()->member_login == 1)
                    <div class="login-btn">
                        <a class="btn" href="http://www.toyway.com.tw/" target="_blank">
                            會 員 登 入
                        </a>
                        <!-- <a class="btn log-in-click" href="javascript:;">
                        會 員 登 入
                        </a> -->
                    </div>
                @endif
                
                <!--display:none-->
                <div class="logout-cart">
                    <ul>
                        <li>
                            <a href="javascript:;">
                                <i class="fas fa-user"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="logOUTbtn">
                                <i class="fas fa-sign-out-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" class="header-cart">
                                <i class="fas fa-shopping-cart"></i>
                                <span class="badge badge-danger badge-pill">1</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--display:none-->
            <div class="header-slogan order-lg-1">
                <img src="/images/header-text.png" alt="">
            </div>

            <div class="d-lg-none w-100">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        @foreach($Subject->read()->where('state',1)->where('id','!=',10)->get() as $key => $Sub)
                        @if(!is_null($Sub->link))
                            <li>
                                <a class="nav-link" href="{{ $Sub->link }}">{{ $Sub->name }}</a>
                            </li>
                        @else
                            @switch($Sub->url)
                                @case('about')
                                @case('other')
                                    <li class="{{ Request::segment(2) == $Sub->url ? 'active' : '' }}">
                                        <a class="nav-link" href="{{ route('front.page',['url'=>$Sub->url]) }}">{{ $Sub->name }}</a>
                                    </li>
                                    @break
                                @case('news')
                                @case('newProduct')
                                @case('reviewProduct')
                                    <li class="{{ Request::segment(2) == $Sub->url ? 'active' : '' }}">
                                        <a class="nav-link" href="{{ route('front.news',['url'=>$Sub->url]) }}">{{ $Sub->name }}</a>
                                    </li>
                                    @break 
                                @case('product')  
                                    <li class="{{ Request::segment(1) == $Sub->url ? 'active' : '' }}">
                                        <a class="nav-link" href="{{ route('front.product') }}">{{ $Sub->name }}</a>
                                    </li>
                                    @break 
                                @case('special')  
                                    <li class="{{ Request::segment(1) == $Sub->url ? 'active' : '' }}">
                                        <a class="nav-link" href="{{ route('front.special') }}">{{ $Sub->name }}</a>
                                    </li>
                                    @break  
                                @case('link')  
                                    <li class="{{ Request::segment(1) == $Sub->url ? 'active' : '' }}">
                                        <a class="nav-link" href="{{ route('front.link') }}">{{ $Sub->name }}</a>
                                    </li>
                                    @break      
                                @default
                            @endswitch
                        @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="header-block"></div>