@inject('Subject', 'App\Repositories\SubjectRepository')
<nav class="menu">
    <div class="content">
        <ul>
            @foreach($Subject->read()->where('state',1)->where('id','!=',10)->get() as $key => $Sub)
                @if(!is_null($Sub->link))
                    <li>
                        <a class="nav-link" href="{{ $Sub->link }}" target="_blank">{{ $Sub->name }}</a>
                    </li>
                @else
                    @switch($Sub->url)
                        @case('about')
                        @case('other')
                            <li class="{{ Request::segment(2) == $Sub->url ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('front.page',['url'=>$Sub->url]) }}">{{ $Sub->name }}</a>
                            </li>
                            @break
                        @case('news')
                        @case('newProduct')
                        @case('reviewProduct')
                            <li class="{{ Request::segment(2) == $Sub->url ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('front.news',['url'=>$Sub->url]) }}">{{ $Sub->name }}</a>
                            </li>
                            @break 
                        @case('product')  
                            <li class="{{ Request::segment(1) == $Sub->url ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('front.product') }}">{{ $Sub->name }}</a>
                            </li>
                            @break 
                        @case('special')  
                            <li class="{{ Request::segment(1) == $Sub->url ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('front.special') }}">{{ $Sub->name }}</a>
                            </li>
                            @break  
                        @case('link')  
                            <li class="{{ Request::segment(1) == $Sub->url ? 'active' : '' }}">
                                <a class="nav-link" href="{{ route('front.link') }}">{{ $Sub->name }}</a>
                            </li>
                            @break      
                        @default
                    @endswitch
                @endif
            @endforeach
        </ul>
    </div>
</nav>
<div class="menu-block"></div>