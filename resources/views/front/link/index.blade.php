@extends('front.link.default')
@section('banner')
@if(isset($Subject->banner))
    <div class="banner">
        <img src="{{ str_replace( " ", "%20",$Subject->banner->img) }}" class="d-block w-100">
    </div>
@endif
@endsection
@section('content')
<div class="content">
    <div class="my-3">
        <div class="row justify-content-center">
            <div class="col-12">
                <!-- ↓圖文編輯器↓ -->
                {{ $LinkCoontent->content }}
                <!-- ↑圖文編輯器↑ -->
            </div>
            <div class="col-12 col-sm-10 col-lg-8">
                <!-- link -->
                @foreach($Links as $key => $Link)
                    <a class="link-item" href="{{ $Link->link }}" target="_blank">
                        <div class="row no-gutters">
                            <div class="col-12 col-md-5">
                                <div class="link-img" style="background-image: url({{ str_replace( " ", "%20",$Link->img) }});">
                                </div>
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="link-text">
                                    <h4>{{ $Link->title }}</h4>
                                    {!! $Link->content !!}
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection