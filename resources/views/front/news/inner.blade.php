@extends('front.news.default')
@section('fb-og')
{{-- <meta property="og:image" content="{{ $News->img }}"> --}}
{{-- <meta property="og:url" content="{{ route('front.news',['url'=>$Subject->url,'id'=>$NewsLast]) }}"> --}}
{{-- <meta property="og:title" content="{{ $News->title }}"> --}}
{{-- <meta property="og:description" content="{{ $News->title }}"> --}}
@endsection
@section('banner')
@if(isset($Subject->banner))
    <div class="banner">
        <img src="{{ str_replace( " ", "%20",$Subject->banner->img) }}" class="d-block w-100">
    </div>
@endif
@endsection
@section('content')
<div class="content">
    <h3 class="title-block">
        @switch($Subject->url)
            @case('news')
                <strong>最 新 消 息</strong>
                @break
            @case('newProduct')
                <strong>新 品 上 市</strong>
                @break
            @default
                <strong>商 品 預 告</strong>
        @endswitch
    </h3>


    <div class="page-btn-group">
        <a href="{{ route('front.news',['url'=>$Subject->url,'id'=>$NewsLast]) }}" class="page-btn page-btn-prev">
          <span></span> 上一頁
        </a>
        <a href="{{ route('front.news',['url'=>$Subject->url,'id'=>$NewsNext]) }}" class="page-btn page-btn-next">
          下一頁 <span></span>
        </a>
    </div>


    <div class="article-block">
        <div class="article-content">
            <!-- 日期 -->
            <div class="date">
                {{ $News->date }}
            </div>
            <!-- 標題 -->
            <h3 class="article-title">
                {!! $News->title !!}
            </h3>
            <!-- 輪播圖 -->
            @if(count($News->banners->where('state',1)->where('delete',0)))
            <div class="article-carousel">
                <div id="carouselArticle" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($News->banners->where('state',1) as $key => $banner)
                            <div class="carousel-item {{ ($key == 0)?'active':null }}">
                                <a data-lightbox="articleImg" href="{{ str_replace( " ", "%20",$banner->img) }}"><div class="img" style="background-image: url({{ str_replace( " ", "%20",$banner->img) }});"></div></a>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselArticle" role="button" data-slide="prev">
                        <img src="/images/icon-arrow.png" alt="">
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselArticle" role="button" data-slide="next">
                        <img src="/images/icon-arrow.png" alt="" style="transform: rotateY(180deg);">
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            @endif

            <!-- 分享按鈕 -->
            <div class="shareBTN-group tr">
                <!--facebook-->
                <a href="https://www.facebook.com/sharer/sharer.php?u={{ $News->share_comment_url }}" target="_blank">
                    <img src="/images/icon-fb.png">
                </a>
                <!--line-->
                <a href="https://social-plugins.line.me/lineit/share?url={{ $News->share_comment_url }}" target="_blank">
                    <img src="/images/icon-line.png">
                </a>
                <!--twitter-->
                <a href="https://twitter.com/share?url={{ $News->share_comment_url }}" target="_blank">
                    <img src="/images/icon-twitter.png">
                </a>
                <!--copylink-->
                <a id="copylink">
                    <img src="/images/icon-link.png" alt="">
                </a>
                {{-- <div class="fb-share-button" data-href="{{ $News->share_comment_url }}" data-layout="button" data-size="large" style="margin-right:5px;">
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore"></a>
                </div> --}}
                {{-- <div class="line-it-button" data-lang="zh_Hant" data-type="share-b" data-ver="3" data-url="{{ $News->share_comment_url }}" data-color="default" data-size="small" data-count="false" style="display: none;"></div> --}}
                {{-- <a class="twitter-share-button" data-size="large" data-url="{{ $News->share_comment_url }}" href="https://twitter.com/intent/tweet?text=東匯玩具"></a> --}}
            </div>

            <!-- 內文 -->
            <div class="article-text">
                <!-- ↓圖文編輯器↓ -->
                {!! $News->content !!}
                <!-- ↑圖文編輯器↑ -->
            </div>
            


            <div class="page-btn-group">
                <a href="{{ route('front.news',['url'=>$Subject->url,'id'=>$NewsLast]) }}" class="page-btn page-btn-prev">
                  <span></span> 上一頁
                </a>
                <a href="{{ route('front.news',['url'=>$Subject->url,'id'=>$NewsNext]) }}" class="page-btn page-btn-next">
                  下一頁 <span></span>
                </a>
            </div>

            <!-- FB留言外掛 -->
            @if(!is_null($News->share_comment_url))
                <div class="fb-function">
                    <div class="fb-comments" data-href="{{ $News->share_comment_url }}" data-width="100%" data-numposts="5"></div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection