@extends('front.news.default')
@section('banner')
@if(isset($Subject->banner))
    <div class="banner">
        <img src="{{ str_replace( " ", "%20",$Subject->banner->img) }}" class="d-block w-100">
    </div>
@endif 
@endsection
@section('content')
<div class="content">
    @if(count($News)>0)
    <h3 class="title-block">
        <strong>{{ $News->first()->category->name }}</strong>
        {{-- @switch($Subject->url)
            @case('news')
                <strong>最 新 消 息</strong>
                @break
            @case('newProduct')
                <strong>新 品 上 市</strong>
                @break
            @default
                <strong>商 品 預 告</strong>
        @endswitch --}}
    </h3>
    
    <ul class="article-page-list">
        @foreach($News as $key => $New)
            <li>
                {{-- <a class="article-page-item" href="{{ route('front.news',['url'=>$Subject->url,'id'=>$New->id]) }}"> --}}
                <div class="article-page-item" onclick="parent.location.href='/news/{{ $Subject->url }}?id={{ $New->id }}';">
                    <div class="img" style="background-image: url({{ str_replace( " ", "%20",$New->img) }});">
                    </div>
                    <div class="block">
                        <div class="title">
                            {!! $New->title !!}
                        </div>
                        <div class="text">
                            {!! $New->content !!}
                        </div>
                        <div class="date">
                            {!! $New->date !!}
                        </div>
                    </div>
                {{-- </a> --}}
            </li>
        @endforeach
    </ul>

    <!-- page -->
    <nav aria-label="Page navigation" class="page-group">
        <ul class="pagination justify-content-center">
            @foreach ($News->appends(request()->input())->links()->elements as $key => $element)
                @if (count($element) > 1)
                    <li class="page-item" id="last_page">
                        <a class="page-link" href="" tabindex="-1" aria-disabled="true">
                            <i class="fas fa-angle-left"></i>
                        </a>
                    </li>
                    @foreach ($element as $key => $element)
                        <li name="news_link" id="{{ $key }}" class="page-item" onclick="change_page(this.id)">
                            <a class="page-link" href="{{ $element }}">{{ $key }}</a>
                        </li>
                    @endforeach
                    <li class="page-item" id="next_page">
                        <a class="page-link" href="">
                            <i class="fas fa-angle-right"></i>
                        </a>
                    </li>
                @endif
            @endforeach 
        </ul>
    </nav>
    @endif
    {{-- <div class="page">
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
                @foreach ($News->appends(request()->input())->links()->elements as $key => $element)
                    @if (count($element) > 1)
                        <li class="page-item" id="last_page">
                            <a class="page-link" href="" tabindex="-1" aria-disabled="true">
                                <span aria-hidden="true">
                                    <
                                </span>
                            </a>
                        </li>
                        @foreach ($element as $key => $element)
                            <li name="news_link" id="{{ $key }}" class="page-item" onclick="change_page(this.id)">
                                <a class="page-link" href="{{ $element }}">{{ $key }}</a>
                            </li>
                        @endforeach
                        <li class="page-item" id="next_page">
                            <a class="page-link" href="">
                                <span aria-hidden="true">
                                    >
                                </span>
                            </a>
                        </li>
                    @endif
                @endforeach    
            </ul>
        </nav>
    </div> --}}
</div>
@endsection
@section('js')
<script>
$(document).ready(function(){
    var link_id_array = [];
    var current_link_id; //當前頁面
    var lastest_link_id; //最後一頁
    var first_link_id; //第一頁
    var next_link_id; 
    var next_link_item;
    var last_link_id;
    var last_link_item;
    var page = []; //紀錄active

    //取得所有頁面array
    $('li[name=news_link]').each(function(){
        link_id_array[$(this).attr('id')] = $(this);
    });

    //取得最後一頁及第一頁
    first_link_id = link_id_array[1]; //第一頁
    var reverse_link_array = link_id_array.reverse();
    lastest_link_id = reverse_link_array[1]; //最後一頁
    
    if(window.location.search == ''){
        //第一頁當做當前頁面
        $('li[id='+first_link_id.attr('id')+']').addClass('active');
        current_link_id = first_link_id.attr('id');
    }else{
        //取得當前頁面
        page = window.location.search.split('=');
        $('li[id='+page[1]+']').addClass('active');
        current_link_id = $('li[id='+page[1]+']').attr('id')
    }
    
    //第一頁時上一頁禁用
    if(current_link_id == 1){
        $('li[id=last_page]').addClass('disabled');
    }
    else{
        $('li[id=last_page]').removeClass('disabled');
    }
    //最後一頁時下一頁禁用
    if(current_link_id == link_id_array.length-1){
        $('li[id=next_page]').addClass('disabled');
    }
    else{
        $('li[id=next_page]').removeClass('disabled');
    }
    
    //設定到最後一頁之後改用減的
    next_link_id = Number(current_link_id)+1;
    next_link_item = $('li[id='+next_link_id+'] a').attr('href');
    $('li[id=next_page] a').attr('href',next_link_item);

    last_link_id = Number(current_link_id)-1;
    last_link_item = $('li[id='+last_link_id+'] a').attr('href');
    $('li[id=last_page] a').attr('href',last_link_item);
    
});

function change_page(id){
    var link_id_array = [];
    var current_link_id;
    
    $('li[id='+id+']').addClass('active');

    //取得所有頁面array
    $('li[name=news_link]').each(function(){
        link_id_array[$(this).attr('id')] = $(this);
    });
     
    //當前頁面
    $('li[name=news_link].active').each(function(){
        current_link_id = $(this).attr('id');
        $('li[id='+current_link_id+']').removeClass('active');
    });
}
</script>
@endsection