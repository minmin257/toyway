@extends('front.page.default')
@section('banner')
@if(isset($Subject->banner))
    <div class="banner">
        <img src="{{ str_replace( " ", "%20",$Subject->banner->img) }}" class="d-block w-100">
    </div>
@endif
@endsection
@section('content')
<div class="content">
    <div class="my-3">
        <!-- ↓圖文編輯器↓ -->
        @foreach($Pages as $key => $Page)
            @if($Page->subject->url == Request::segment(2))
                {!! $Page->content !!}
            @endif
        @endforeach
        <!-- ↑圖文編輯器↑ -->
    </div>
</div>
@endsection