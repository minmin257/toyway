<div class="product-list">
    <div class="row">
        @if(isset($CurrentBrand) && !is_null($CurrentBrand))
            @foreach($Products as $key => $Product)
                <div class="col-6 col-xs-tw col-md-tw">
                    <a href="{{ route('front.product.inner',['product'=>$Product->id,'brand'=>$CurrentBrand->id]) }}" class="product-item">
                        <div class="img" style="background-image: url({{ str_replace( " ", "%20",$Product->img) }});"></div>
                        <div class="num">
                            {{ $Product->no }}
                        </div>
                        <div class="title">
                            {{ $Product->name }}
                        </div>
                    </a>
                </div>
            @endforeach
        @else
            @foreach($Products as $key => $Product)
                <div class="col-6 col-xs-tw col-md-tw">
                    <a href="{{ route('front.product.inner',['product'=>$Product->id]) }}" class="product-item">
                        <div class="img" style="background-image: url({{ str_replace( " ", "%20",$Product->img) }});"></div>
                        <div class="num">
                            {{ $Product->no }}
                        </div>
                        <div class="title">
                            {{ $Product->name }}
                        </div>
                    </a>
                </div>
            @endforeach
        @endif
    </div>
</div>