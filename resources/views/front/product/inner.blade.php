@extends('front.product.default')
@section('fb-og')
{{-- <meta property="og:image" content="{{ $Product->img }}"> --}}
{{-- <meta property="og:url" content="{{ route('front.product.inner',['product'=>$Product->id]) }}"> --}}
{{-- <meta property="og:title" content="{{ $News->title }}"> --}}
{{-- <meta property="og:description" content="{{ $Product->name }}"> --}}
@endsection
@section('banner')
@if(isset($Subject->banner))
<div class="banner">
    <img src="{{ str_replace( " ", "%20",$Subject->banner->img) }}" class="d-block w-100">
</div>
@endif
@endsection
@section('content')
<div class="content">
    <div class="category-list">
        <div class="row justify-content-between">
            @foreach($ProductCategories as $key => $Category)
                @if($Category->id == $CurrentCategory->id)
                    <div class="col-12 col-md">
                        <a class="category-item active" href="{{ route('front.product',['category'=>$Category->id]) }}" title="">
                            {{ $Category->name }}
                        </a>
                    </div>
                @else  
                    <div class="col-12 col-md">
                        <a class="category-item" href="{{ route('front.product',['category'=>$Category->id]) }}" title="">
                            {{ $Category->name }}
                        </a>
                    </div>
                @endif
            @endforeach 
            <!-- <div class="col-12 col-md">
                <a class="category-item" href="product_category.html" title="">
                  會員訂單下載
                </a>
              </div> -->
        </div>
    </div>
    @if(!is_null($CurrentBrand))
        <div class="page-btn-group">
            <a href="{{ route('front.product.inner',['product'=>$ProductLast,'brand'=>$CurrentBrand]) }}" class="page-btn page-btn-prev">
            <span></span> 上一頁
            </a>
            <a href="{{ route('front.product.inner',['product'=>$ProductNext,'brand'=>$CurrentBrand]) }}" class="page-btn page-btn-next">
            下一頁 <span></span>
            </a>
        </div>
    @else
        <div class="page-btn-group">
            <a href="{{ route('front.product.inner',['product'=>$ProductLast]) }}" class="page-btn page-btn-prev">
            <span></span> 上一頁
            </a>
            <a href="{{ route('front.product.inner',['product'=>$ProductNext]) }}" class="page-btn page-btn-next">
            下一頁 <span></span>
            </a>
        </div>
    @endif

    <div class="product-block">
        <div class="row">
            <div class="col-12 col-md-6 mb-10">
                <!-- 商品大圖 -->
                <div class="product-imgL">
                    <img id="product_main_img" src="{{ str_replace( " ", "%20",$Product->img) }}" alt="">
                </div>
            </div>
            <div class="col-12 col-md-6 mb-10">
                <!-- 商品資訊 -->
                <div class="product-num">
                    商品編號：NO.<span>{{ $Product->no }}</span>
                </div>
                <div class="product-title">
                    {{ $Product->name }}
                </div>
                <div class="product-text">
                    <!-- ↓圖文編輯器↓ -->
                    {!! $Product->content !!}
                    <!-- ↑圖文編輯器↑ -->
                </div>
                <!-- 購物車用 -->
                <!-- <div class="product-cart">
                  <form class="form-inline mb-10">
                    <div class="form-group mb-2">
                      <label for="inputcount">數量：</label>
                      <input type="number" class="form-control product-count" id="inputcount" value="1">
                    </div>
                  </form>
                  <div class="row">
                    <div class="col-6 col-lg-4">
                      <a class="btn product-btn-1" href="javascript:;" title="">
                        加入購物車
                      </a>
                    </div>
                    <div class="col-6 col-lg-4">
                      <a class="btn product-btn-2" href="javascript:;" title="">
                        我要預購
                      </a>
                    </div>
                  </div>
                </div> -->
                <!-- 分享按鈕 -->
                <div class="shareBTN-group tr">
                    <!--facebook-->
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ $Product->share_comment_url }}" target="_blank">
                        <img src="/images/icon-fb.png">
                    </a>
                    <!--line-->
                    <a href="https://social-plugins.line.me/lineit/share?url={{ $Product->share_comment_url }}" target="_blank">
                        <img src="/images/icon-line.png">
                    </a>
                    <!--twitter-->
                    <a href="https://twitter.com/share?url={{ $Product->share_comment_url }}" target="_blank">
                        <img src="/images/icon-twitter.png">
                    </a>
                    <!--copylink-->
                    <a id="copylink">
                        <img src="/images/icon-link.png" alt="">
                    </a>
                    {{-- <div class="fb-share-button" data-href="{{ $News->share_comment_url }}" data-layout="button" data-size="large" style="margin-right:5px;">
                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore"></a>
                    </div> --}}
                    {{-- <div class="line-it-button" data-lang="zh_Hant" data-type="share-b" data-ver="3" data-url="{{ $News->share_comment_url }}" data-color="default" data-size="small" data-count="false" style="display: none;"></div> --}}
                    {{-- <a class="twitter-share-button" data-size="large" data-url="{{ $News->share_comment_url }}" href="https://twitter.com/intent/tweet?text=東匯玩具"></a> --}}
                </div>
            </div>
            <div class="col-12 mb-10">
                <div class="product-img-list">
                    <div class="row">
                        <!--產品大圖-->
                        <div class="col-6 col-md-3 col-lg-2 mb-2">
                            <a href="javascript:;" class="product-img-item" id="productImg_0">
                                <img name="productImg" src="{{ str_replace( " ", "%20",$Product->img) }}" alt="">
                            </a>
                        </div>
                        @foreach($Product->imgs->where('state',1) as $key => $imgs)
                            <div class="col-6 col-md-3 col-lg-2 mb-2">
                                <a href="javascript:;" class="product-img-item" id="productImg_{{ $imgs->id }}">
                                    <img name="productImg" src="{{ str_replace( " ", "%20",$imgs->img) }}" alt="">
                                    
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        @if(!is_null($CurrentBrand))
            <div class="page-btn-group">
                <a href="{{ route('front.product.inner',['product'=>$ProductLast,'brand'=>$CurrentBrand]) }}" class="page-btn page-btn-prev">
                <span></span> 上一頁
                </a>
                <a href="{{ route('front.product.inner',['product'=>$ProductNext,'brand'=>$CurrentBrand]) }}" class="page-btn page-btn-next">
                下一頁 <span></span>
                </a>
            </div>
        @else
            <div class="page-btn-group">
                <a href="{{ route('front.product.inner',['product'=>$ProductLast]) }}" class="page-btn page-btn-prev">
                <span></span> 上一頁
                </a>
                <a href="{{ route('front.product.inner',['product'=>$ProductNext]) }}" class="page-btn page-btn-next">
                下一頁 <span></span>
                </a>
            </div>
        @endif
            
        <!-- FB留言外掛 -->
        @if(!is_null($Product->share_comment_url))
            <div class="fb-function">
                <div class="fb-comments" data-href="{{ $Product->share_comment_url }}" data-width="100%" data-numposts="5"></div>
            </div>
        @endif
    </div>
</div>
@endsection
@section('js')
<script>
    var imgs = document.getElementsByName('productImg');
    Array.prototype.forEach.call(imgs, function(el) {
        el.addEventListener("click", function(){
            var src = this.getAttribute('src');
            var main_img = document.getElementById('product_main_img');
            main_img.getAttribute('src',src)
        });
    });
</script>
@endsection