@extends('front.product.default')
@section('banner')
@if(isset($Subject->banner))
<div class="banner">
    <img src="{{ str_replace( " ", "%20",$Subject->banner->img) }}" class="d-block w-100">
</div>
@endif
@endsection
@section('content')
<div class="content">
    <div class="category-list">
        <div class="row justify-content-between">
            @if(is_null($CurrentCategory))
                @foreach($ProductCategories as $key => $Category)
                    <div class="col-12 col-md">
                        <a class="category-item {{ ($key == 0)?'active':null }}" href="{{ route('front.product',['category'=>$Category->id]) }}" title="">
                            {{ $Category->name }}
                        </a>
                    </div>
                @endforeach
            @else
            
                @foreach($ProductCategories as $key => $Category)
                    @if($Category->id == $CurrentCategory->id)
                        <div class="col-12 col-md">
                            <a class="category-item active" href="{{ route('front.product',['category'=>$Category->id]) }}" title="">
                                {{ $Category->name }}
                            </a>
                        </div>
                    @else  
                        <div class="col-12 col-md">
                            <a class="category-item" href="{{ route('front.product',['category'=>$Category->id]) }}" title="">
                                {{ $Category->name }}
                            </a>
                        </div>
                    @endif
                @endforeach 
            @endif
            <!-- <div class="col-12 col-md">
                <a class="category-item" href="product_category.html" title="">
                  會員訂單下載
                </a>
              </div> -->
        </div>
    </div>
    <div class="brand-list">
        @if(!is_null($CurrentBrand))
            <a class="brand-list-btn" data-toggle="collapse" href="#collapseList" role="button" aria-expanded="false" aria-controls="collapseList">
                {{ $CurrentBrand->name }} 
                <span><i class="fas fa-angle-down"></i></span>
            </a>
            <div class="collapse" id="collapseList">
        @endif
            <div class="row">
                @if(is_null($CurrentCategory) && count($ProductCategories->first()->brands->where('state',1))!=0)
                    @foreach($ProductCategories->first()->brands->where('state',1) as $key => $Brand)
                        <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                            <a class="brand-item" href="{{ route('front.product',['category'=>$ProductCategories->first()->id,'brand'=>$Brand->id]) }}">
                                {{ $Brand->name }}
                            </a>
                        </div>
                    @endforeach
                @else
                    @foreach($ProductCategories as $key => $Category)
                        @if($Category->id == $CurrentCategory->id)
                            @if(is_null($CurrentBrand) && count($Category->brands->where('state',1))!=0)
                                @foreach($Category->brands->where('state',1) as $key => $Brand)
                                    <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                                        <a class="brand-item" href="{{ route('front.product',['category'=>$Category->id,'brand'=>$Brand->id]) }}">
                                            {{ $Brand->name }}
                                        </a>
                                    </div>
                                @endforeach
                            @else
                                @if(count($Category->brands->where('state',1))!=0)
                                    @foreach($Category->brands->where('state',1) as $key => $Brand)
                                        @if($Brand->id == $CurrentBrand->id)
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                                                <a class="brand-item active" href="{{ route('front.product',['category'=>$Category->id,'brand'=>$Brand->id]) }}">
                                                    {{ $Brand->name }}
                                                </a>
                                            </div>
                                        @else
                                            <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                                                <a class="brand-item" href="{{ route('front.product',['category'=>$Category->id,'brand'=>$Brand->id]) }}">
                                                    {{ $Brand->name }}
                                                </a>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            @endif
                        @endif
                    @endforeach
                @endif
            </div>
        @if(!is_null($CurrentBrand))
            </div>
        @endif
    </div>

    <div class="category-text">
        <!-- ↓圖文編輯器↓ -->
        @if(is_null($CurrentCategory))
            {!! $ProductCategories->first()->content !!}
        @else
            @foreach($ProductCategories as $key => $Category)
                @if($Category->id == $CurrentCategory->id)
                    {!! $Category->content !!}
                @endif
            @endforeach
        @endif
        <!-- ↑圖文編輯器↑ -->
    </div>

    @if(!is_null($CurrentBrand) && count($Selects)!=0)
        <div class="price-filter">
            <a class="price-filter-btn" data-toggle="collapse" href="#collapsePrice" role="button" aria-expanded="true" aria-controls="collapsePrice">
                <i class="fas fa-plus"></i>篩選器(可複選)
            </a>
            <div class="collapse show" id="collapsePrice">
                <div class="select-list mt-10">
                    @foreach($Selects as $key => $Select)
                        <a href="javascript:;" class="select-btn" id="select_{{ $Select->id }}" name="productSelect">
                            <i class="far fa-times-circle"></i>
                            {{ $Select->name }}
                        </a>
                    @endforeach
                </div>
                <p class="">
                    <button class="select-btn-check" style="border: none;" type="button" onclick="select_submit()">
                        <i class="fas fa-check"></i>確定
                    </button>
                </p>
            </div>
        </div>
    @endif
    <div id="hole">
        @include('front.product.hole')
    </div>

    <!-- page -->
    <nav aria-label="Page navigation" class="page-group">
        <ul class="pagination justify-content-center">
            @if(count($Products->appends(request()->input())->links()->elements[0])>1)
            <li class="page-item" id="last_page">
                <a class="page-link" href="" tabindex="-1" aria-disabled="true">
                    <i class="fas fa-angle-left"></i>
                </a>
            </li>
            @endif
            @foreach ($Products->appends(request()->input())->links()->elements as $key => $element)
                @if (is_array($element) && count($element) > 1)
                    @foreach ($element as $key => $element)
                        <li name="news_link" id="{{ $key }}" class="page-item" onclick="change_page(this.id)">
                            <a class="page-link" href="{{ $element }}">{{ $key }}</a>
                        </li>
                    @endforeach
                @elseif(!is_array($element) || (is_array($element) && count($element)!=1))
                    <li name="news_link" class="page-item">
                        <a class="page-link" href="javascript:;">{{ $element }}</a>
                    </li>
                @endif
            @endforeach 
            @if(count($Products->appends(request()->input())->links()->elements[0])>1)
            <li class="page-item" id="next_page">
                <a class="page-link" href="">
                    <i class="fas fa-angle-right"></i>
                </a>
            </li>
            @endif
        </ul>
    </nav>
</div>
@endsection
@section('js')
<script>
$(document).ready(function(){
    var link_id_array = [];
    var current_link_id; //當前頁面
    var lastest_link_id; //最後一頁
    var first_link_id; //第一頁
    var next_link_id; 
    var next_link_item;
    var last_link_id;
    var last_link_item;
    var page = []; //紀錄active

    //取得所有頁面array
    $('li[name=news_link]').each(function(){
        link_id_array[$(this).attr('id')] = $(this);
    });

    //取得最後一頁及第一頁
    first_link_id = link_id_array[1]; //第一頁
    var reverse_link_array = link_id_array.reverse();
    lastest_link_id = reverse_link_array[1]; //最後一頁
    
    if(window.location.search == ''){
        //第一頁當做當前頁面
        $('li[id='+first_link_id.attr('id')+']').addClass('active');
        current_link_id = first_link_id.attr('id');
    }else{
        //取得當前頁面
        page = window.location.search.split('=');
        $('li[id='+page[1]+']').addClass('active');
        current_link_id = $('li[id='+page[1]+']').attr('id')
    }
    
    //第一頁時上一頁禁用
    if(current_link_id == 1){
        $('li[id=last_page]').addClass('disabled');
    }
    else{
        $('li[id=last_page]').removeClass('disabled');
    }
    //最後一頁時下一頁禁用
    if(current_link_id == link_id_array.length-1){
        $('li[id=next_page]').addClass('disabled');
    }
    else{
        $('li[id=next_page]').removeClass('disabled');
    }
    
    //設定到最後一頁之後改用減的
    next_link_id = Number(current_link_id)+1;
    next_link_item = $('li[id='+next_link_id+'] a').attr('href');
    $('li[id=next_page] a').attr('href',next_link_item);

    last_link_id = Number(current_link_id)-1;
    last_link_item = $('li[id='+last_link_id+'] a').attr('href');
    $('li[id=last_page] a').attr('href',last_link_item);
    
});

function change_page(id){
    var link_id_array = [];
    var current_link_id;
    
    $('li[id='+id+']').addClass('active');

    //取得所有頁面array
    $('li[name=news_link]').each(function(){
        link_id_array[$(this).attr('id')] = $(this);
    });
     
    //當前頁面
    $('li[name=news_link].active').each(function(){
        current_link_id = $(this).attr('id');
        $('li[id='+current_link_id+']').removeClass('active');
    });
}

function select_submit(){
    //取得已選擇select
    var selected = [];
    var count = 0;
    $('a.select-btn.active').each(function(){
        console.log($(this).attr('id'))
        var select = $(this).attr('id');
        var selects = select.split('_');
        selected[count] = selects[1];
        count = count+1;
    });

    //取得目前分類、品牌
    var arr = window.location.pathname.split('/');
    var category_id = arr[2];
    var brand_id = arr[3];

    // if(selected.length == 0){
    //     // Swal.fire({
    //     //     icon: 'warning',
    //     //     text: 'Please Choose some options!',
    //     //     width: 400,
    //     //     showConfirmButton: false,
    //     //     timer: 2000,
    //     // })
    //     $.ajax({
    //         type: 'POST',
    //         url: '{{ route('front.product.fliter') }}',
    //         headers: {
    //             'X-CSRF-TOKEN': '{{ csrf_token() }}'
    //         },
    //         data: {
    //             selsct: selected,
    //             category: category_id,
    //             brand: brand_id
    //         },
    //         success: function(data){
    //             $('#hole').html(data)
    //         }
    //     })
    // }
    // else{
        $.ajax({
            type: 'POST',
            url: '{{ route('front.product.fliter') }}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data: {
                selsct: selected,
                category: category_id,
                brand: brand_id
            },
            success: function(data){
                $('#hole').html(data)
            }
        })
    // }
    //if selected = 0 顯示提示訊息 else 送出 ajax 表單
}
</script>
@endsection