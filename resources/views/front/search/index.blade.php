@extends('front.link.default')
@section('banner')
@if(isset($Subject->banner))
<div class="banner">
    <img src="{{ str_replace( " ", "%20",$Subject->banner->img) }}" class="d-block w-100">
</div>
@endif
@endsection
@section('content')
<div class="content">
    <div class="row mt-3">
        <div class="col-12 col-md-6">
            <form id="search_input" action="{{ route('front.search') }}" method="GET">
                <div class="input-group searchbar">
                    <div class="input-group-prepend">
                        <button class="btn btn-search" type="button" id="button-addon-search2" onclick="$('#search_input').submit()">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                    <input type="text" name="keyword" id="keyword" class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon-search2" value="{{ $keyword }}">
                </div>
            </form>
        </div>
        <div class="col-12 col-md-6">
            <div class="search-select">
                共<span>{{ $total }}</span>項結果 │
                <a href="{{ route('front.search',['select_option'=>'time','keyword'=>$keyword]) }}" class="{{ strripos(Request::fullUrl(),"select_option=interrelated") ? '' : 'active' }}">依時間排序</a> │
                <a href="{{ route('front.search',['select_option'=>'interrelated','keyword'=>$keyword]) }}" class="{{ strripos(Request::fullUrl(),"select_option=interrelated") ? 'active' : '' }}">依關聯性排序</a>
            </div>
        </div>
    </div>
    <ul class="search-list">
        <!--搜尋結果-->
        @foreach($Results as $key => $Result)
            @if(isset($Result->title))
                @if(isset($Result->youtube_id))
                    <li>
                        <div class="search-item" onclick="parent.location.href='/special/{{ $Result->id }}';">
                            {{-- <a href="{{ route('front.special',['video'=>$Result->id]) }}" class="search-item"> --}}
                            <div class="img" style="background-image: url(http://img.youtube.com/vi/{{ $Result->youtube_id }}/0.jpg);">
                            </div>
                            <div class="block">
                                <div class="title" id="title_{{ $key }}">
                                    {{ $Result->title }}
                                </div>
                                <div class="text" id="content_{{ $key }}">
                                    {!! $Result->content !!}
                                </div>
                            </div>
                            {{-- </a> --}}
                        </div>
                    </li>
                @else
                    <li>
                        <div class="search-item" onclick="parent.location.href='/news/{{ $Result->subject_url() }}?id={{ $Result->id }}';">
                        {{-- <a href="{{ route('front.news',['url'=>$Result->subject_url(),'id'=>$Result->id]) }}" class="search-item"> --}}
                            <div class="img" style="background-image: url({{ str_replace( " ", "%20",$Result->img) }});">
                            </div>
                            <div class="block">
                                <div class="title" id="title_{{ $key }}">
                                    {!! $Result->title !!}
                                </div>
                                <div class="text" id="content_{{ $key }}">
                                    {!! $Result->content !!}
                                </div>
                            </div>
                        {{-- </a> --}}
                        </div>
                    </li>
                @endif
            @else
                <li>
                    <div class="search-item" onclick="parent.location.href='/product/inner/{{ $Result->id }}';">
                        {{-- <a href="{{ route('front.product.inner',['product'=>$Result->id]) }}" class="search-item"> --}}
                        <div class="img" style="background-image: url({{ str_replace( " ", "%20",$Result->img) }});">
                        </div>
                        <div class="block">
                            <div class="title" id="title_{{ $key }}">
                                {{ $Result->name }}
                            </div>
                            <div class="text" id="content_{{ $key }}">
                                {!! $Result->content !!}
                            </div>
                        </div>
                        {{-- </a> --}}
                    </div>
                </li>
            @endif
        @endforeach
    </ul>

    <!-- page -->
    <nav aria-label="Page navigation" class="page-group">
        <ul class="pagination justify-content-center">
            {{-- {{dd($Results->appends(request()->input())->links()->elements)}} --}}
            @foreach ($Results->appends(request()->input())->links()->elements as $key => $element)
                @if (count($element) > 1)
                    <li class="page-item" id="last_page">
                        <a class="page-link" href="" tabindex="-1" aria-disabled="true">
                            <i class="fas fa-angle-left"></i>
                        </a>
                    </li>
                    @foreach ($element as $key => $element)
                        @php
                            $element = explode('/',$element);
                        @endphp
                        <li name="news_link" id="{{ $key }}" class="page-item" onclick="change_page(this.id)">
                            <a class="page-link" href="{{ $element[1] }}">{{ $key }}</a>
                        </li>
                    @endforeach
                    <li class="page-item" id="next_page">
                        <a class="page-link" href="">
                            <i class="fas fa-angle-right"></i>
                        </a>
                    </li>
                @endif
            @endforeach 
        </ul>
    </nav>

</div>
@endsection
@section('js')
<script>
$(document).ready(function(){
    var link_id_array = [];
    var current_link_id; //當前頁面
    var lastest_link_id; //最後一頁
    var first_link_id; //第一頁
    var next_link_id; 
    var next_link_item;
    var last_link_id;
    var last_link_item;
    var page = []; //紀錄active

    //取得所有頁面array
    $('li[name=news_link]').each(function(){
        link_id_array[$(this).attr('id')] = $(this);
    });

    //取得最後一頁及第一頁
    first_link_id = link_id_array[1]; //第一頁
    var reverse_link_array = link_id_array.reverse();
    lastest_link_id = reverse_link_array[1]; //最後一頁
    // console.log()
    if(window.location.search.indexOf('page') == -1){
        //第一頁當做當前頁面
        $('li[id='+first_link_id.attr('id')+']').addClass('active');
        current_link_id = first_link_id.attr('id');
    }else{
        //取得當前頁面
        page = window.location.search.split('=');
        console.log(page)
        if(page[0] == "?select_option"){
            $('li[id='+page[3]+']').addClass('active');
            current_link_id = $('li[id='+page[3]+']').attr('id')
        }
        else{
            $('li[id='+page[2]+']').addClass('active');
            current_link_id = $('li[id='+page[2]+']').attr('id')
        }
    }
    
    //第一頁時上一頁禁用
    if(current_link_id == 1){
        $('li[id=last_page]').addClass('disabled');
    }
    else{
        $('li[id=last_page]').removeClass('disabled');
    }
    //最後一頁時下一頁禁用
    if(current_link_id == link_id_array.length-1){
        $('li[id=next_page]').addClass('disabled');
    }
    else{
        $('li[id=next_page]').removeClass('disabled');
    }
    
    //設定到最後一頁之後改用減的
    next_link_id = Number(current_link_id)+1;
    next_link_item = $('li[id='+next_link_id+'] a').attr('href');
    $('li[id=next_page] a').attr('href',next_link_item);

    last_link_id = Number(current_link_id)-1;
    last_link_item = $('li[id='+last_link_id+'] a').attr('href');
    $('li[id=last_page] a').attr('href',last_link_item);
    
});

function change_page(id){
    var link_id_array = [];
    var current_link_id;
    
    $('li[id='+id+']').addClass('active');

    //取得所有頁面array
    $('li[name=news_link]').each(function(){
        link_id_array[$(this).attr('id')] = $(this);
    });
     
    //當前頁面
    $('li[name=news_link].active').each(function(){
        current_link_id = $(this).attr('id');
        $('li[id='+current_link_id+']').removeClass('active');
    });
}

//標註關鍵字
var keyword = document.getElementById('keyword');
var titles = document.getElementsByClassName('title');
var content = document.getElementsByClassName('text');
Array.prototype.forEach.call(titles, function(el) {
    var index = el.innerHTML.indexOf(keyword.value);
    if (index >= 0) { 
        var innerHTML = el.innerHTML.substring(0,index) + "<span class='search-keyword'>" + el.innerHTML.substring(index,index+keyword.value.length) + "</span>" + el.innerHTML.substring(index + keyword.value.length);
        el.innerHTML = innerHTML;
    }
})
Array.prototype.forEach.call(content, function(el) {
    var index = el.innerHTML.indexOf(keyword.value);
    if (index >= 0) { 
        var innerHTML = el.innerHTML.substring(0,index) + "<span class='search-keyword'>" + el.innerHTML.substring(index,index+keyword.value.length) + "</span>" + el.innerHTML.substring(index + keyword.value.length);
        el.innerHTML = innerHTML;
    }
});
</script>
@endsection