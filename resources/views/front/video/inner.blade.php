@extends('front.video.default')
@section('fb-og')
{{-- <meta property="og:image" content="https://www.youtube.com/embed/{{ $Video->youtube_id }}"> --}}
{{-- <meta property="og:url" content="{{ route('front.special',['video'=>$Video->id]) }}"> --}}
{{-- <meta property="og:title" content="{{ $News->title }}"> --}}
{{-- <meta property="og:description" content="{{ $Video->title }}"> --}}
@endsection
@section('banner')
@if(isset($Subject->banner))
<div class="banner">
    <img src="{{ str_replace( " ", "%20",$Subject->banner->img) }}" class="d-block w-100">
</div>
@endif
@endsection
@section('content')
<div class="content">
    <div class="video-player">
        <!-- 16:9 aspect ratio -->
        <div class="embed-responsive embed-responsive-16by9">
            {{-- <iframe class="embed-responsive-item" src="//www.youtube.com/embed/"></iframe> --}}
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $Video->youtube_id }}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>

    <div class="video-content">
        <div class="date">
            {{ $Video->date }}
        </div>
        <div class="title">
            {{ $Video->title }}
        </div>
        <div class="text">
            <!-- ↓圖文編輯器↓ -->
            {!! $Video->content !!}
            <!-- ↑圖文編輯器↑ -->
        </div>
    </div>

    <!-- 分享按鈕 -->
    <div class="shareBTN-group tr">
        <!--facebook-->
        <a href="https://www.facebook.com/sharer/sharer.php?u={{ $Video->share_comment_url }}" target="_blank">
            <img src="/images/icon-fb.png">
        </a>
        <!--line-->
        <a href="https://social-plugins.line.me/lineit/share?url={{ $Video->share_comment_url }}" target="_blank">
            <img src="/images/icon-line.png">
        </a>
        <!--twitter-->
        <a href="https://twitter.com/share?url={{ $Video->share_comment_url }}" target="_blank">
            <img src="/images/icon-twitter.png">
        </a>
        <!--copylink-->
        <a id="copylink">
            <img src="/images/icon-link.png" alt="">
        </a>
        {{-- <div class="fb-share-button" data-href="{{ $News->share_comment_url }}" data-layout="button" data-size="large" style="margin-right:5px;">
            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore"></a>
        </div> --}}
        {{-- <div class="line-it-button" data-lang="zh_Hant" data-type="share-b" data-ver="3" data-url="{{ $News->share_comment_url }}" data-color="default" data-size="small" data-count="false" style="display: none;"></div> --}}
        {{-- <a class="twitter-share-button" data-size="large" data-url="{{ $News->share_comment_url }}" href="https://twitter.com/intent/tweet?text=東匯玩具"></a> --}}
    </div>

    <!-- FB留言外掛 -->
    @if(!is_null($Video->share_comment_url))
        <div class="fb-function">
            <div class="fb-comments" data-href="{{ $Video->share_comment_url }}" data-width="100%" data-numposts="5"></div>
        </div>
    @endif
</div>
@endsection