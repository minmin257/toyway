@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
@endif

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script type="text/javascript">
	$(function() {
		setTimeout(function() {
			$('.alert-success').fadeOut()
		}, 5000);
	});
</script>
<script type="text/javascript">
	@if(session()->has('message'))
		Swal.fire({
			html: '<span class="f-family" style="color: #707070;font-size: 14px;font-weight:600;">'+$('#message').html()+'</span>',
			icon: 'success',
			showConfirmButton: false,
			width: '300px',
			heightAuto: false,
			timer: 2000
		});

		$(function() {
			$('div[class="nice-select swal2-select"]').removeClass('nice-select').css("display","none");
			// 去除吃到css的nice-select
		});
	@endif
	
	@if(session()->has('update_message'))
		Swal.fire({
			title: '儲存成功',
			icon: 'success',
			showConfirmButton: false,
			width: '300px',
			timer: 1000,
			heightAuto: false
		})
	@endif

	@if(session()->has('create_message'))
		Swal.fire({
			title: '新增成功',
			icon: 'success',
			showConfirmButton: false,
			width: '300px',
			timer: 1000,
			heightAuto: false
		})
	@endif

	@if(session()->has('send_message'))
		Swal.fire({
			title: '發送成功',
			icon: 'success',
			showConfirmButton: false,
			width: '300px',
			timer: 1000,
			heightAuto: false
		})
	@endif
</script>