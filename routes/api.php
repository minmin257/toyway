<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/execution/storage-link',function(){   
    Artisan::call('storage:link');        
    return response()->json([
        'result' => true  
    ]);;    
 });

 Route::get('/clear/cache',function(){ 
    Artisan::call('cache:clear');   
	Artisan::call('config:clear');   
	Artisan::call('config:cache');   
    return response()->json([
        'result' => true  
    ]);;    
 });

 Route::group(['namespace'=>'Api'],function(){
    Route::get('/set/url','SetUrlController@setUrl');
    Route::get('/set/emoji','SetEmojiController@export');
    Route::get('/import/emoji','SetEmojiController@import');
 });
