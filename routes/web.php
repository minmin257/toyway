<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//維護中介
Route::group(['middleware'=>['maintenance']], function(){
    Route::get('/', 'FrontController@index')->name('front.homepage');
    Route::get('/page/{url}', 'FrontController@page')->name('front.page');
    Route::get('/news/{url}', 'FrontController@news')->name('front.news');
    Route::get('/product/inner/{product}/{brand?}', 'FrontController@product_inner')->name('front.product.inner');
    Route::get('/product/{category?}/{brand?}', 'FrontController@product')->name('front.product');
    Route::post('/product/fliter', 'FrontController@product_fliter')->name('front.product.fliter');
    Route::get('/special/{video?}', 'FrontController@special')->name('front.special');
    Route::get('/link', 'FrontController@link')->name('front.link');
    Route::get('/search', 'FrontController@search')->name('front.search');
});

//filemanager
Route::group(['prefix'=>'laravel-filemanager','middleware'=> ['web','auth']],function(){
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::group(['prefix'=>'webmin','namespace'=>'backend'], function(){
    // 登入登出
    Route::get('/','LoginController@index')->name('backend');
    Route::post('/','LoginController@login')->name('login');
    Route::get('/logout','LoginController@logout')->name('logout');

    Route::group(['middleware'=>'auth:web'], function(){
        Route::get('/index', 'ModuleController@index')->name('backend.index');
        //Content
        Route::group(['prefix'=>'content'], function(){
            //homebanner
            Route::group(['prefix'=>'homebanner'], function(){
                Route::get('/', 'ContentController@homebanner')->name('homebanner');
                Route::post('/update', 'ContentController@homebanner_update')->name('homebanner.update');
                Route::post('/create', 'ContentController@homebanner_create')->name('homebanner.create');
                Route::post('/delete', 'ContentController@homebanner_delete')->name('homebanner.delete');
            });
            //innerbanner
            Route::group(['prefix'=>'innerbanner'], function(){
                Route::get('/', 'ContentController@innerbanner')->name('innerbanner');
                Route::post('/update', 'ContentController@innerbanner_update')->name('innerbanner.update');
            });
            //page
            Route::group(['prefix'=>'page'], function(){
                Route::get('/subject', 'ContentController@subject')->name('subject');
                Route::post('/subject/update', 'ContentController@subject_update')->name('subject.update');
                Route::get('/pages/{name}', 'ContentController@pages')->name('pages');
                Route::post('/update', 'ContentController@pages_update')->name('pages.update');
            });
            //products
            Route::group(['prefix'=>'products'], function(){
                Route::get('/categories', 'ContentController@product_categories')->name('product.categories');
                Route::get('/categories/edit/{id}', 'ContentController@product_categories_edit')->name('product.categories.edit');
                Route::post('/categories/update', 'ContentController@product_categories_update')->name('product.categories.update');
                Route::get('/selects', 'ContentController@product_selects')->name('product.selects');
                Route::post('/selects/update', 'ContentController@product_selects_update')->name('product.selects.update');
                Route::post('/selects/create', 'ContentController@product_selects_create')->name('product.selects.create');
                Route::post('/selects/delete', 'ContentController@product_selects_delete')->name('product.selects.delete');
                Route::get('/selects/lists/{id}', 'ContentController@product_selects_lists')->name('product.selects.lists');
                Route::post('/selects/lists/update', 'ContentController@product_selects_lists_update')->name('product.selects.lists.update');
                Route::post('/selects/lists/delete', 'ContentController@product_selects_lists_delete')->name('product.selects.lists.delete');
                Route::get('/brands/{id}', 'ContentController@product_brands')->name('product.brands');
                Route::post('/brands/update', 'ContentController@product_brands_update')->name('product.brands.update');
                Route::post('/brands/create', 'ContentController@product_brands_create')->name('product.brands.create');
                Route::post('/brands/delete', 'ContentController@product_brands_delete')->name('product.brands.delete');
                Route::get('/lists/{id?}', 'ContentController@product_lists')->name('product.lists');
                Route::post('/productList/fliter', 'ContentController@product_fliter_product')->name('productList.fliter.product');
                Route::post('/create', 'ContentController@product_create')->name('product.create');
                Route::get('/edit/{id}', 'ContentController@product_edit')->name('product.edit');
                Route::get('/img/{id}', 'ContentController@product_img')->name('product.img');
                Route::post('/img/update', 'ContentController@product_img_update')->name('product.img.update');
                Route::post('/img/create', 'ContentController@product_img_create')->name('product.img.create');
                Route::post('/img/delete', 'ContentController@product_img_delete')->name('product.img.delete');
                Route::post('/update', 'ContentController@product_update')->name('product.update');
                Route::post('/delete', 'ContentController@product_delete')->name('product.delete');
                Route::post('/select/fliter', 'ContentController@select_fliter_product')->name('select.fliter.product');
            });
            //videos
            Route::group(['prefix'=>'videos'], function(){
                Route::get('/', 'ContentController@videos')->name('videos');
                Route::post('/create', 'ContentController@videos_create')->name('videos.create');
                Route::get('/edit/{id}', 'ContentController@videos_edit')->name('videos.edit');
                Route::post('/update', 'ContentController@videos_update')->name('videos.update');
                Route::post('/delete', 'ContentController@videos_delete')->name('videos.delete');
            });
            //links
            Route::group(['prefix'=>'links'], function(){
                Route::get('/', 'ContentController@links')->name('links');
                Route::post('/create', 'ContentController@links_create')->name('links.create');
                Route::get('/edit/{id}', 'ContentController@links_edit')->name('links.edit');
                Route::post('/update', 'ContentController@links_update')->name('links.update');
                Route::post('/delete', 'ContentController@links_delete')->name('links.delete');
            });
            //homeproduct
            Route::group(['prefix'=>'homeproduct'], function(){
                Route::get('/', 'ContentController@homeproduct')->name('homeproduct');
                Route::post('/fliter', 'ContentController@homeproduct_fliter')->name('homeproduct.fliter');
                Route::post('/fliter/product', 'ContentController@homeproduct_fliter_product')->name('homeproduct.fliter.product');
                Route::post('/update', 'ContentController@homeproduct_update')->name('homeproduct.update');
                Route::post('/delete', 'ContentController@homeproduct_delete')->name('homeproduct.delete');
            });
            //newsCategories
            Route::group(['prefix'=>'newsCategories'], function(){
                Route::get('/', 'ContentController@news_categories')->name('news.categories');
                Route::post('/update', 'ContentController@news_categories_update')->name('news.categories.update');
            });
            //news
            Route::group(['prefix'=>'news'], function(){
                Route::get('/lists/{id?}', 'ContentController@news_lists')->name('news.lists');
                Route::get('/edit/{id?}', 'ContentController@news_edit')->name('news.edit');
                Route::post('/delete', 'ContentController@news_delete')->name('news.delete');
                Route::post('/create', 'ContentController@news_create')->name('news.create');
                Route::post('/update', 'ContentController@news_update')->name('news.update');
                Route::get('/banner/{id}', 'ContentController@news_banner')->name('news.banner');
                Route::post('/banner/create', 'ContentController@news_banner_create')->name('news.banner.create');
                Route::post('/banner/update', 'ContentController@news_banner_update')->name('news.banner.update');
                Route::post('/banner/delete', 'ContentController@news_banner_delete')->name('news.banner.delete');
            });
            
        });
        //authority
        Route::group(['prefix'=>'authority'], function(){
            Route::get('/profile', 'AuthorityController@profile')->name('profile');
            Route::post('/profile/update', 'AuthorityController@profile_update')->name('profile.update');
            Route::group(['middleware'=>'HasPremission:權限管理'], function() {
                //帳號管理
                Route::get('/management', 'AuthorityController@management')->name('management');
                Route::post('/management/update', 'AuthorityController@management_update')->name('management.update');
                Route::get('/management/edit/{id}', 'AuthorityController@management_edit')->name('management.edit');
                Route::post('/management/user/delete', 'AuthorityController@user_delete')->name('user.delete');
                Route::post('/management/user/create', 'AuthorityController@user_create')->name('user.create');
                //權限設定
                Route::get('/premission/{id}', 'AuthorityController@premission')->name('premission');
                Route::post('/premission/update', 'AuthorityController@premission_update')->name('premission.update');
            });
        });
        Route::group(['prefix'=>'system'], function(){
            Route::get('/', 'SystemController@system')->name('system');
            Route::post('/', 'SystemController@system_update')->name('system.update');
            Route::get('/footer', 'SystemController@footer')->name('footer');
            Route::post('/footer/update', 'SystemController@footer_update')->name('footer.update');
            
        });
    });
});